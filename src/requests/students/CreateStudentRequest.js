import api from '../../api';

class CreateStudentRequest {
  constructor(data) {
    this.endpoint = '/students/';
    this.method = 'post';
    this.data = data;
  }

  fire() {
    return api[this.method](this.endpoint, this.data);
  }
}

export default CreateStudentRequest;
