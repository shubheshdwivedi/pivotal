import api from '../../api';


class FetchStudentRequest {
  constructor(page, params = {}) {
    this.offset = (page - 1) * 10;
    this.endpoint = '/students/?offset=' + this.offset;
    this.method = 'get';
    this.params = params;
  }

  fire() {
    return api[this.method](this.endpoint, this.params);
  }
}

export default FetchStudentRequest;
