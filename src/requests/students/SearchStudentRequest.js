import api from '../../api';
import {searchLength} from '../../_helpers/exportConsts';


class SearchStudentRequest {
  constructor() {
    this.endpoint = `/students/?limit=${searchLength}`;
    this.method = 'get';
  }

  fire(name) {
    return api[this.method](this.endpoint, {name});
  }
}

export default SearchStudentRequest;
