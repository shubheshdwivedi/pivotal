import api from '../../api';

class EditStudentRequest {
  constructor(data) {
    this.endpoint = '/students/';
    this.method = 'put';
    this.data = data;
  }

  fire() {
    return api[this.method](this.endpoint, this.data);
  }
}

export default EditStudentRequest;
