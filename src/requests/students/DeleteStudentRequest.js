import api from '../../api';

class DeleteStudentRequest {
  constructor(id) {
    this.endpoint = '/students/' + id + '/';
    this.method = 'delete';
  }

  fire() {
    return api[this.method](this.endpoint);
  }
}

export default DeleteStudentRequest;
