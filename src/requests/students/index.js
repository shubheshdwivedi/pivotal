import FetchStudentRequest from './FetchStudentRequest';
import CreateStudentRequest from './CreateStudentRequest';
import EditStudentRequest from './EditStudentRequest';
import DeleteStudentRequest from './DeleteStudentRequest';
import SearchStudentRequest from './SearchStudentRequest';

export {
  FetchStudentRequest,
  CreateStudentRequest,
  EditStudentRequest,
  DeleteStudentRequest,
  SearchStudentRequest
};
