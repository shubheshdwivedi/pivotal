import api from '../../api';

class EditGroupRequest {
  constructor(data) {
    this.endpoint = '/groups/';
    this.method = 'put';
    this.data = data;
  }

  fire() {
    return api[this.method](this.endpoint, this.data);
  }
}

export default EditGroupRequest;
