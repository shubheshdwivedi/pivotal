import api from '../../api';

class CreateGroupRequest {
  constructor(data) {
    this.endpoint = '/groups/';
    this.method = 'post';
    this.data = data;
  }

  fire() {
    return api[this.method](this.endpoint, this.data);
  }
}

export default CreateGroupRequest;
