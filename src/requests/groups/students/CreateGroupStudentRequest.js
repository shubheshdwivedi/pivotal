import api from '../../../api';

class CreateGroupStudentRequest {
  constructor(id, studentId) {
    this.endpoint = `/groups/${id}/students/`;
    this.method = 'post';
    this.data = {studentIds: [studentId]};
  }

  fire() {
    return api[this.method](this.endpoint, this.data);
  }
}

export default CreateGroupStudentRequest;
