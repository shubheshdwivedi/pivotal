import api from '../../../api';


class FetchGroupStudentRequest {
  constructor(id, page, params = {}) {
    this.offset = (page - 1) * 10;
    this.endpoint = `/groups/${id}/students/?offset=${this.offset}`;
    this.method = 'get';
    this.params = params;
  }

  fire() {
    return api[this.method](this.endpoint, this.params);
  }
}

export default FetchGroupStudentRequest;
