import api from '../../../api';

class DeleteGroupStudentRequest {
  constructor(id, studentID) {
    this.endpoint = `/groups/${id}/students/`;
    this.method = 'delete';
    this.data = {studentIds: [studentID]};
  }

  fire() {
    return api[this.method](this.endpoint, this.data);
  }
}

export default DeleteGroupStudentRequest;
