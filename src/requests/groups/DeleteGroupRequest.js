import api from '../../api';

class DeleteGroupRequest {
  constructor(id) {
    this.endpoint = '/groups/' + id + '/';
    this.method = 'delete';
  }

  fire() {
    return api[this.method](this.endpoint);
  }
}

export default DeleteGroupRequest;
