import api from '../../api';

class EditCourseRequest {
  constructor(data) {
    this.endpoint = '/courses/';
    this.method = 'put';
    this.data = data;
  }

  fire() {
    return api[this.method](this.endpoint, this.data);
  }
}

export default EditCourseRequest;
