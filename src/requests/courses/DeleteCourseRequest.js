import api from '../../api';

class DeleteCourseRequest {
  constructor(id) {
    this.endpoint = '/courses/' + id + '/';
    this.method = 'delete';
  }

  fire() {
    return api[this.method](this.endpoint);
  }
}

export default DeleteCourseRequest;
