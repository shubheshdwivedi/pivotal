import api from '../../../api';

class CreateCourseModuleRequest {
  constructor(id, data) {
    this.endpoint = `/courses/${id}/modules/`;
    this.method = 'post';
    this.data = data;
  }

  fire() {
    return api[this.method](this.endpoint, this.data);
  }
}

export default CreateCourseModuleRequest;
