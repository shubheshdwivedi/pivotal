import api from '../../../api';


class FetchCourseModuleRequest {
  constructor(id, page, params = {}) {
    this.offset = (page - 1) * 10;
    this.endpoint = `/courses/${id}/modules/?offset=${this.offset}`;
    this.method = 'get';
    this.params = params;
  }

  fire() {
    return api[this.method](this.endpoint, this.params);
  }
}

export default FetchCourseModuleRequest;
