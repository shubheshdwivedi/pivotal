import api from '../../../../api';

class CreateModuleTopicRequest {
  constructor(courseId, moduleId, data) {
    this.endpoint = `/courses/${courseId}/modules/${moduleId}/topics/`;
    this.method = 'post';
    this.data = data;
  }

  fire() {
    return api[this.method](this.endpoint, this.data);
  }
}

export default CreateModuleTopicRequest;
