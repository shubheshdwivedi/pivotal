import api from '../../../../api';

class DeleteModuleTopicRequest {
  constructor(courseId, moduleId, id) {
    this.endpoint = `/courses/${courseId}/modules/${moduleId}/topics/${id}/`;
    this.method = 'delete';
  }

  fire() {
    return api[this.method](this.endpoint);
  }
}

export default DeleteModuleTopicRequest;
