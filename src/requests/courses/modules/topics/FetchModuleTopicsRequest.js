import api from '../../../../api';


class FetchModuleTopicsRequest {
  constructor(courseId, moduleId, page, params = {}) {
    this.offset = (page - 1) * 10;
    this.endpoint =
      `/courses/${courseId}/modules/${moduleId}/topics/?offset=${this.offset}`;
    this.method = 'get';
    this.params = params;
  }

  fire() {
    return api[this.method](this.endpoint, this.params);
  }
}

export default FetchModuleTopicsRequest;
