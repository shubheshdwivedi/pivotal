import api from '../../../../api';

class EditModuleTopicRequest {
  constructor(courseId, moduleId, id, data) {
    this.endpoint = `/courses/${courseId}/modules/${moduleId}/topics/${id}/`;
    this.method = 'put';
    this.data = data;
  }

  fire() {
    return api[this.method](this.endpoint, this.data);
  }
}

export default EditModuleTopicRequest;
