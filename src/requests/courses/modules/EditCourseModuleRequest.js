import api from '../../../api';

class EditCourseModuleRequest {
  constructor(id, moduleId, data) {
    this.endpoint = `/courses/${id}/modules/${moduleId}/`;
    this.method = 'put';
    this.data = data;
  }

  fire() {
    return api[this.method](this.endpoint, this.data);
  }
}

export default EditCourseModuleRequest;
