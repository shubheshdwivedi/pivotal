import api from '../../../api';

class DeleteCourseModuleRequest {
  constructor(id, moduleId) {
    this.endpoint = `/courses/${id}/modules/${moduleId}/`;
    this.method = 'delete';
  }

  fire() {
    return api[this.method](this.endpoint);
  }
}

export default DeleteCourseModuleRequest;
