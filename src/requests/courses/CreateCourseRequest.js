import api from '../../api';

class CreateCourseRequest {
  constructor(data) {
    this.endpoint = '/courses/';
    this.method = 'post';
    this.data = data;
  }

  fire() {
    return api[this.method](this.endpoint, this.data);
  }
}

export default CreateCourseRequest;
