import api from '../../api';
import {searchLength} from '../../_helpers/exportConsts';


class SearchMentorsRequest {
  constructor() {
    this.endpoint = `/employees/?limit=${searchLength}&role=Mentor`;
    this.method = 'get';
  }

  fire(name) {
    return api[this.method](this.endpoint, {name});
  }
}

export default SearchMentorsRequest;
