import api from '../../api';

class EditEmployeeRequest {
  constructor(data) {
    this.endpoint = '/employees/';
    this.method = 'put';
    this.data = data;
  }

  fire() {
    return api[this.method](this.endpoint, this.data);
  }
}

export default EditEmployeeRequest;
