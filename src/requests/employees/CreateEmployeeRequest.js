import api from '../../api';


class CreateEmployeeRequest {
  constructor(data) {
    this.endpoint = '/employees/';
    this.method = 'post';
    this.data = data;
  }

  fire() {
    return api[this.method](this.endpoint, this.data);
  }
}

export default CreateEmployeeRequest;
