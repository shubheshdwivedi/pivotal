import api from '../../api';

class DeleteEmployeeRequest {
  constructor(id) {
    this.endpoint = '/employees/' + id + '/';
    this.method = 'delete';
  }

  fire() {
    return api[this.method](this.endpoint);
  }
}

export default DeleteEmployeeRequest;
