import DeleteModuleRequest from './DeleteModuleRequest';
import EditModuleRequest from './EditModuleRequest';
import FetchModuleRequest from './FetchModuleRequest';

export {
  DeleteModuleRequest,
  EditModuleRequest,
  FetchModuleRequest
};
