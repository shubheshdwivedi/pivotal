import api from '../../api';

class EditModuleRequest {
  constructor(data) {
    this.endpoint = '/modules/';
    this.method = 'put';
    this.data = data;
  }

  fire() {
    return api[this.method](this.endpoint, this.data);
  }
}

export default EditModuleRequest;
