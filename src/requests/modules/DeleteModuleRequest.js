import api from '../../api';

class DeleteModuleRequest {
  constructor(id) {
    this.endpoint = '/modules/' + id + '/';
    this.method = 'delete';
  }

  fire() {
    return api[this.method](this.endpoint);
  }
}

export default DeleteModuleRequest;
