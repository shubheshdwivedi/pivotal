import api from './../api';

class LoginRequest {
  constructor(data) {
    this.data = {
      'email'   : data['email'],
      'password': data['password']
    };
    this.endpoint = '/auth/';
    this.method = 'post';
  }

  fire() {
    return api[this.method](this.endpoint, this.data);
  }
}

export default LoginRequest;
