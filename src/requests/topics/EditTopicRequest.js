import api from '../../api';

class EditTopicRequest {
  constructor(data) {
    this.endpoint = '/topics/';
    this.method = 'put';
    this.data = data;
  }

  fire() {
    return api[this.method](this.endpoint, this.data);
  }
}

export default EditTopicRequest;
