import api from '../../api';

class DeleteTopicRequest {
  constructor(id) {
    this.endpoint = '/topics/' + id + '/';
    this.method = 'delete';
  }

  fire() {
    return api[this.method](this.endpoint);
  }
}

export default DeleteTopicRequest;
