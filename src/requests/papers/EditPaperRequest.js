import api from '../../api';

class EditPaperRequest {
  constructor(data, id) {
    this.endpoint = `/papers/${id}`;
    this.method = 'put';
    this.data = data;
  }

  fire() {
    return api[this.method](this.endpoint, this.data);
  }
}

export default EditPaperRequest;
