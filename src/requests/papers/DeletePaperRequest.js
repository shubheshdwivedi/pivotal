import api from '../../api';

class DeletePaperRequest {
  constructor(id) {
    this.endpoint = '/papers/' + id + '/';
    this.method = 'delete';
  }

  fire() {
    return api[this.method](this.endpoint);
  }
}

export default DeletePaperRequest;
