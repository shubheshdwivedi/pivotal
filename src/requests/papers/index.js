import FetchPapersRequest from './FetchPapersRequest';
import DeletePaperRequest from './DeletePaperRequest';
import EditPaperRequest from './EditPaperRequest';
import CreatePaperRequest from './CreatePaperRequest';

export {
  FetchPapersRequest,
  CreatePaperRequest,
  DeletePaperRequest,
  EditPaperRequest
};
