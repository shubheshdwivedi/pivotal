import api from '../../api';

class CreatePaperRequest {
  constructor(data) {
    this.endpoint = '/papers/';
    this.method = 'post';
    this.data = data;
  }

  fire() {
    return api[this.method](this.endpoint, this.data);
  }
}

export default CreatePaperRequest;
