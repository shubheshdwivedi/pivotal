import api from '../../api';

class MenteeRequest {
  constructor(data) {
    this.endpoint = '/mentees/';
    this.method = 'post';
    this.data = data;
  }

  fire() {
    return api[this.method](this.endpoint, this.data);
  }
}

export default MenteeRequest;
