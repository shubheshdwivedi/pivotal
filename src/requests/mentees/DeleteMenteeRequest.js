import api from '../../api';

class DeleteMenteeRequest {
  constructor(id) {
    this.endpoint = '/mentees/';
    this.method = 'put';
    this.data = {
      'studentId' : id,
      'employeeId': 0
    };
  }

  fire() {
    return api[this.method](this.endpoint, this.data);
  }
}

export default DeleteMenteeRequest;
