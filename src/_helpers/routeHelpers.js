const routes = {
  auth         : '/',
  dashboard    : '/dashboard',
  employeeList : '/dashboard/employees',
  studentList  : '/dashboard/students',
  menteeList   : '/dashboard/mentees',
  groupList    : '/dashboard/groups',
  courseList   : '/dashboard/courses',
  eventCreator : '/dashboard/event-create',
  paperUploader: '/dashboard/paper-uploader',

  groupStudentList: '/dashboard/groups/student',
  courseModuleList: '/dashboard/course/modules',
  moduleTopicList : '/dashboard/course/modules/topics',

  paperList    : '/dashboard/papers',
  paperCrafting: '/dashboard/paper-craft'
};

export default routes;
