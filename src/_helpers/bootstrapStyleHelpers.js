const styleFixes = {
  noMarginPadding: {
    padding: 0,
    margin : 0
  },
  formInputs: {
    border      : 'none',
    borderBottom: '2px solid #eff0f1',
    borderRadius: 0
  },
  customPadding: (pT, pR, pB, pL) => {
    return {
      paddingTop   : pT + 'rem',
      paddingRight : pR + 'rem',
      paddingBottom: pB + 'rem',
      paddingLeft  : pL + 'rem'
    };
  },
  customWidth: (width) => {
    return {
      width: width + '%'
    };
  }
};

export default styleFixes;
