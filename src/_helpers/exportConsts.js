import routes from './routeHelpers';
import AwesomeDebouncePromise from 'awesome-debounce-promise';

export const APP_NAME = 'Progress Tracker';

export const PRIMARY_COLOR = '#43425c';

export const paginatorLength = 10;

export const searchLength = 4;

export const isLoggedIn = () => (localStorage.getItem('token') !== null);

export const getSearchDebounced = (request) => {
  return AwesomeDebouncePromise((text) => request.fire(text), 500);
};

export const validateFormFields = (data) => {
  Object.entries(data).forEach(([i, v]) => {
    if (!v) {
      delete data[i];
    }
  });
  return data;
};

export const eRoles = {
  mentor       : 'Mentor',
  teacher      : 'Teacher',
  paperUploader: 'Paper Uploader',
  eventCreator : 'Event Creator'
};

export const eRolesArray = Object.keys(eRoles).map(function(key) {
  return eRoles[key];
});

export const uRoles = {
  tenant  : 'Tenant',
  employee: 'Employee',
  student : 'Student'
};

export const uRolesArray = Object.keys(uRoles).map(function(key) {
  return [uRoles[key]];
});

export const tenantDashboardMenuHeadings = ['employees', 'students',
  'mentees', 'groups', 'courses'];
export const tenantDashboardObjects = {
  'employees': {
    'key'  : 'emp',
    'name' : 'Employees',
    'id'   : 'employeeCard',
    'route': routes.employeeList
  },
  'students': {
    'key'  : 'stu',
    'name' : 'Students',
    'id'   : 'studentCard',
    'route': routes.studentList
  },
  'mentees': {
    'key'  : 'ment',
    'name' : 'Mentees',
    'id'   : 'menteesCard',
    'route': routes.menteeList
  },
  'groups': {
    'key'  : 'grp',
    'name' : 'Groups',
    'id'   : 'groupCard',
    'route': routes.groupList
  },
  'courses': {
    'key'  : 'crs',
    'name' : 'Courses',
    'id'   : 'courseCard',
    'route': routes.courseList
  }
};

export const employeeDashboardObjects = {
  'Event Creator': {
    'key'  : 'ec',
    'name' : 'Event Creator',
    'id'   : 'eventCreator',
    'route': routes.eventCreator
  },
  'Paper Uploader': {
    'key'  : 'pu',
    'name' : 'Paper Uploader',
    'id'   : 'paperUploader',
    'route': routes.paperList
  }
};

export const modalTitles = {
  employee: {
    delete: 'Delete Employee',
    create: 'Create an Employee',
    edit  : 'Edit Employee'
  },
  student: {
    delete: 'Delete Student',
    create: 'Create a Student',
    edit  : 'Edit Student'
  },
  mentees: {
    delete: 'Remove Mentee',
    create: 'Assign a Mentor',
    edit  : 'Assign a new Mentor'
  },
  groups: {
    delete  : 'Delete Group',
    create  : 'Create Group',
    edit    : 'Edit Group',
    students: {
      delete: 'Delete group\'s students ?',
      create: 'Add students ?'
    }
  },
  courses: {
    delete: 'Delete Course',
    create: 'Create Course',
    edit  : 'Edit Course'
  },
  modules: {
    delete: 'Delete Module',
    create: 'Create Module',
    edit  : 'Edit Module'
  },
  topics: {
    delete: 'Delete Topic',
    create: 'Create Topic',
    edit  : 'Edit Topic'
  },
  papers: {
    create    : 'Paper Uploader',
    delete    : 'Delete Paper',
    addSection: 'New section'
  }
};
