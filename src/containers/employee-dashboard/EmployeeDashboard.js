import React from 'react';
import {connect} from 'react-redux';
import {Container, Row} from 'react-bootstrap';
import {
  employeeDashboardObjects
} from '../../_helpers/exportConsts';
import styleFixes from '../../_helpers/bootstrapStyleHelpers';
import {Columns} from '../../components/cols/Columns';

class EmployeeDashboard extends React.Component {
  render() {
    // const cols = [];
    const headings = this.props.user.roles;
    return (
      <Container>
        <Row style={{...styleFixes.customPadding(4, 0, 0, 0)}}>
          <Container>
            <h1><strong>Dashboard</strong></h1>
            <small className="text-muted">
              Welcome Back, {this.props.user.name}
            </small>
          </Container>
        </Row>
        <br/>
        <Row style={styleFixes.noMarginPadding}>
          <Columns headings={headings}
            reference={employeeDashboardObjects}/>
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.DataReducers.AuthReducer.user
  };
};

export default connect(
  mapStateToProps,
  null
)(EmployeeDashboard);
