import React from 'react';
import {connect} from 'react-redux';
import {
  fetchGroupStudents,
  deleteGroupStudent,
  setGroSearchParams,
  createGroupStudent
} from '../../store/actions/groups';
import styleFixes from '../../_helpers/bootstrapStyleHelpers';
import List from '../../components/list/List';
import {Row, Container, Breadcrumb} from 'react-bootstrap';
import Searchbar from '../../components/searchbar/Searchbar';
import Preloader from '../../components/preloader/Preloader';
import Paginator from '../../components/paginator';
import {
  GroupStudentCreateModal,
  GroupStudentDeleteModal
} from '../../components/modals';
import routes from '../../_helpers/routeHelpers';


class GroupStudent extends React.Component {
  state = {
    createModalShow: false,
    deleteModalShow: false,
    deleteModalStu : null
  };

  toggleDeleteModalShow = (bool, stu = null) => {
    this.setState({
      ...this.state,
      deleteModalShow: bool,
      deleteModalStu : stu
    });
  };

  toggleCreateModalShow = (bool) => {
    this.setState({
      ...this.state,
      createModalShow: bool
    });
  };

  create = (e) => {
    e.preventDefault();
    this.toggleCreateModalShow(true);
  };

  searchParams = (e) => {
    e.preventDefault();
    const form = e.target.form;
    const name = form.name.value;

    const params = {name};
    this.props.setGroSearchParams(params);
  };

  getStudents = () => {
    const {ids, currentPage, groupStudents, grpId} = this.props;
    const currentIds = ids[currentPage];
    const students = groupStudents[grpId];
    const stu = {};

    if (currentIds && grpId && students) {
      currentIds.forEach((id) => stu[id] = students[id]);
    }
    return stu;
  };

  fetchStu = (page) => {
    const {grpId, searchParams} = this.props;
    this.props.fetchStudents(grpId, page, searchParams);
  };

  getCrumbs = () => {
    const crubms = [];
    crubms.push(<Breadcrumb.Item href={routes.dashboard} key={0}>
      Dashboard
    </Breadcrumb.Item>);

    crubms.push(<Breadcrumb.Item href={routes.groupList} key={1}>
      Groups
    </Breadcrumb.Item>);

    crubms.push(<Breadcrumb.Item href={routes.groupStudentList} key={2} active>
      Students
    </Breadcrumb.Item>);
    return crubms;
  };

  isLoading() {
    return (
      this.props.fetching ||
      this.props.editing ||
      this.props.deleting ||
      this.props.creating
    );
  }

  componentDidMount() {
    this.fetchStu(this.props.currentPage);
  }

  render() {
    if (this.isLoading()) return (<Preloader/>);
    if (!this.props.grpId) return (<h1>HELLO</h1>);

    return (
      <Container className="pt-5">

        {/* DELETE MODAL*/}
        {this.state.deleteModalShow && <GroupStudentDeleteModal
          show={this.state.deleteModalShow}
          onHide={this.toggleDeleteModalShow}
          grpId={this.props.grpId}
          stu={this.state.deleteModalStu}
          del={this.props.deleteStudent}
        />}

        {/* CREATE MODAL*/}
        {this.state.createModalShow && <GroupStudentCreateModal
          show={this.state.createModalShow}
          onHide={this.toggleCreateModalShow}
          grpId={this.props.grpId}
          create={this.props.createStudent}
        />}

        <Searchbar
          data={this.props.searchParams}
          submit={this.searchParams}
          toggleCreate={this.create}
        /><br/>

        <Row className={'justify-content-left'}
          style={styleFixes.noMarginPadding}>
          <Breadcrumb>
            {this.getCrumbs()}
          </Breadcrumb>
        </Row>
        <List
          main="name"
          under="groups"
          toggleEdit={this.toggleEditModalShow}
          toggleDel={this.toggleDeleteModalShow}
          objects={this.getStudents()}
        />
        <br/>

        <Row className={'justify-content-center'}>
          <Paginator
            currentPage={this.props.currentPage}
            totalPages={this.props.totalPages}
            fetch={this.fetchStu}
          />
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    searchParams: state.UiReducers.GroupReducer.searchParams,
    currentPage : state.UiReducers.GroupReducer.grpStuCurrentPage,
    totalPages  : state.UiReducers.GroupReducer.totalPages,
    ids         : state.UiReducers.GroupReducer.grpStuIds,
    grpId       : state.UiReducers.GroupReducer.grpId,

    groupStudents: state.DataReducers.DataReducer.groupStudents,
    fetching     : state.DataReducers.DataReducer.fetching,
    creating     : state.DataReducers.DataReducer.creating,
    editing      : state.DataReducers.DataReducer.editing,
    deleting     : state.DataReducers.DataReducer.deleting,
    error        : state.DataReducers.DataReducer.error
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchStudents: (id, page, params) => dispatch(
      fetchGroupStudents(id, page, params)
    ),
    createStudent     : (id, stuId) => dispatch(createGroupStudent(id, stuId)),
    deleteStudent     : (id, stuId) => dispatch(deleteGroupStudent(id, stuId)),
    setGroSearchParams: (params) => dispatch(setGroSearchParams(params))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GroupStudent);
