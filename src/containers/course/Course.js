import React from 'react';
import {connect} from 'react-redux';
import {
  fetchCourse,
  editCourse,
  createCourse,
  deleteCourse,
  setCourseId,
  setCourseSearchParams
} from '../../store/actions/courses/';
import List from '../../components/list/List';
import {Row, Container, Breadcrumb} from 'react-bootstrap';
import Searchbar from '../../components/searchbar/Searchbar';
import {
  CoursesDeleteModal,
  CoursesEditModal,
  CoursesCreateModal
} from '../../components/modals';
import Preloader from '../../components/preloader/Preloader';
import Paginator from '../../components/paginator';
import routes from '../../_helpers/routeHelpers';

class Course extends React.Component {
  state = {
    editModalShow    : false,
    createModalShow  : false,
    deleteModalShow  : false,
    editModalCourse  : null,
    deleteModalCourse: null
  };

  toggleDeleteModalShow = (bool, course = null) => {
    this.setState({
      ...this.state,
      deleteModalShow  : bool,
      deleteModalCourse: course
    });
  };

  toggleEditModalShow = (bool, course = null) => {
    this.setState({
      ...this.state,
      editModalShow  : bool,
      editModalCourse: course
    });
  };

  toggleCreateModalShow = (bool) => {
    this.setState({
      ...this.state,
      createModalShow: bool
    });
  };

  create = (e) => {
    e.preventDefault();
    this.toggleCreateModalShow(true);
  };

  searchParams = (e) => {
    e.preventDefault();
    const form = e.target.form;
    const name = form.name.value;

    const params = {name};
    this.props.setCourseSearchParams(params);
  };

  getCourses = () => {
    const {ids, currentPage, courses} = this.props;
    const currentIds = ids[currentPage];

    const course = {};
    if (currentIds) {
      currentIds.forEach((id) => course[id] = courses[id]);
    }
    return course;
  };

  getModules = (course) => {
    const courseId = course.id;
    this.props.setCourseId(courseId);
    return this.props.history.push(routes.courseModuleList);
  };

  fetchCourse = (page) => {
    this.props.fetchCourse(page, this.props.searchParams);
  };

  getCrumbs = () => {
    const crumbs = [];
    crumbs.push(<Breadcrumb.Item href={routes.dashboard} key={0}>
      Dashboard
    </Breadcrumb.Item>);

    crumbs.push(<Breadcrumb.Item href={routes.courseList} key={1} active>
      Courses
    </Breadcrumb.Item>);
    return crumbs;
  };

  isLoading() {
    return (
      this.props.fetching ||
      this.props.editing ||
      this.props.deleting ||
      this.props.creating
    );
  }

  componentDidMount() {
    this.fetchCourse(this.props.currentPage);
  }

  render() {
    if (this.isLoading()) return (<Preloader/>);

    return (
      <Container className="pt-5">

        {/* DELETE MODAL*/}
        {this.state.deleteModalShow && <CoursesDeleteModal
          show={this.state.deleteModalShow}
          onHide={this.toggleDeleteModalShow}
          course={this.state.deleteModalCourse}
          del={this.props.deleteCourse}
        />}

        {/* EDIT MODAL*/}
        {this.state.editModalShow && <CoursesEditModal
          show={this.state.editModalShow}
          onHide={this.toggleEditModalShow}
          course={this.state.editModalCourse}
          edit={this.props.editCourse}
        />}

        {/* CREATE MODAL*/}
        {this.state.createModalShow && <CoursesCreateModal
          show={this.state.createModalShow}
          onHide={this.toggleCreateModalShow}
          create={this.props.createCourse}
        />}

        <Searchbar
          data={this.props.searchParams}
          submit={this.searchParams}
          toggleCreate={this.create}
        /><br/>

        <Row className={'justify-content-left ml-5'}>
          <Breadcrumb>
            {this.getCrumbs()}
          </Breadcrumb>
        </Row>
        <List
          main="name"
          under="description"
          toggleEdit={this.toggleEditModalShow}
          others={[
            {
              label : 'Modules',
              action: this.getModules
            }
          ]}
          toggleDel={this.toggleDeleteModalShow}
          objects={this.getCourses()}
        />
        <br/>

        <Row className={'justify-content-center'}>
          <Paginator
            currentPage={this.props.currentPage}
            totalPages={this.props.totalPages}
            fetch={this.fetchCourse}
          />
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    searchParams: state.UiReducers.CourseReducer.searchParams,
    currentPage : state.UiReducers.CourseReducer.currentPage,
    totalPages  : state.UiReducers.CourseReducer.totalPages,
    ids         : state.UiReducers.CourseReducer.ids,

    courses : state.DataReducers.DataReducer.courses,
    fetching: state.DataReducers.DataReducer.fetching,
    editing : state.DataReducers.DataReducer.editing,
    deleting: state.DataReducers.DataReducer.deleting,
    creating: state.DataReducers.DataReducer.creating,
    error   : state.DataReducers.DataReducer.error
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchCourse: (page, params) => dispatch(
      fetchCourse(page, params)
    ),
    setCourseId          : (id) => dispatch(setCourseId(id)),
    editCourse           : (data) => dispatch(editCourse(data)),
    createCourse         : (data) => dispatch(createCourse(data)),
    deleteCourse         : (id) => dispatch(deleteCourse(id)),
    setCourseSearchParams: (params) => dispatch(setCourseSearchParams(params))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Course);
