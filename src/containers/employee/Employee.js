import React from 'react';
import {connect} from 'react-redux';
import {
  fetchEmployees,
  editEmployee,
  deleteEmployee,
  createEmployee,
  setEmpSearchParams
} from '../../store/actions/employee/';
import List from '../../components/list/List';
import {Row, Container, Breadcrumb} from 'react-bootstrap';
import Searchbar from '../../components/searchbar/Searchbar';
import {eRolesArray} from '../../_helpers/exportConsts';
import {
  EmployeeDeleteModal,
  EmployeeEditModal,
  EmployeeCreateModal
} from '../../components/modals';
import Preloader from '../../components/preloader/Preloader';
import Paginator from '../../components/paginator';
import routes from '../../_helpers/routeHelpers';

class Employee extends React.Component {
  state = {
    editModalShow  : false,
    createModalShow: false,
    deleteModalShow: false,
    editModalEmp   : null,
    deleteModalEmp : null
  };

  toggleDeleteModalShow = (bool, emp = null) => {
    this.setState({
      ...this.state,
      deleteModalShow: bool,
      deleteModalEmp : emp
    });
  };

  toggleEditModalShow = (bool, emp = null) => {
    this.setState({
      ...this.state,
      editModalShow: bool,
      editModalEmp : emp
    });
  };

  toggleCreateModalShow = (bool) => {
    this.setState({
      ...this.state,
      createModalShow: bool
    });
  };

  isSearch = () => {
    return this.props.searchParams !== null;
  };

  create = (e) => {
    e.preventDefault();
    this.toggleCreateModalShow(true);
  };

  searchParams = (e) => {
    e.preventDefault();
    const form = e.target.form;
    const name = form.name.value;
    const role = form.role.value;

    const params = {name, role};
    // console.log(params);
    this.props.setEmpSearchParams(params);
  };

  getEmployees = () => {
    const {ids, currentPage, employees} = this.props;
    const currentIds = ids[currentPage];

    const emp = {};
    if (currentIds) {
      currentIds.forEach((id) => emp[id] = employees[id]);
    }
    return emp;
  };

  fetchEmp = (page) => {
    this.props.fetchEmployees(page, this.props.searchParams);
  };

  getCrumbs = () => {
    const crubms = [];
    crubms.push(<Breadcrumb.Item href={routes.dashboard} key={0}>
      Dashboard
    </Breadcrumb.Item>);

    crubms.push(<Breadcrumb.Item href={routes.employeeList} key={1} active>
      Employee
    </Breadcrumb.Item>);
    return crubms;
  };

  isLoading() {
    return (
      this.props.fetching ||
      this.props.editing ||
      this.props.deleting ||
      this.props.creating
    );
  }

  componentDidMount() {
    this.fetchEmp(this.props.currentPage);
  }

  render() {
    if (this.isLoading()) return (<Preloader/>);

    return (
      <Container className="pt-5">

        {/* DELETE MODAL*/}
        {this.state.deleteModalShow && <EmployeeDeleteModal
          show={this.state.deleteModalShow}
          onHide={this.toggleDeleteModalShow}
          emp={this.state.deleteModalEmp}
          del={this.props.deleteEmployee}
        />}

        {/* EDIT MODAL*/}
        {this.state.editModalShow && <EmployeeEditModal
          show={this.state.editModalShow}
          onHide={this.toggleEditModalShow}
          emp={this.state.editModalEmp}
          edit={this.props.editEmployee}
        />}

        {/* CREATE MODAL*/}
        {this.state.createModalShow && <EmployeeCreateModal
          show={this.state.createModalShow}
          onHide={this.toggleCreateModalShow}
          create={this.props.createEmployee}
        />}

        <Searchbar
          roles={eRolesArray}
          data={this.props.searchParams}
          submit={this.searchParams}
          toggleCreate={this.create}
        /><br/>

        <Row className={'justify-content-left ml-5'}>
          <Breadcrumb>
            {this.getCrumbs()}
          </Breadcrumb>
        </Row>
        <List
          main="name"
          under="roles"
          toggleEdit={this.toggleEditModalShow}
          toggleDel={this.toggleDeleteModalShow}
          objects={this.getEmployees()}
        />
        <br/>

        <Row className={'justify-content-center'}>
          <Paginator
            currentPage={this.props.currentPage}
            totalPages={this.props.totalPages}
            fetch={this.fetchEmp}
          />
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    searchParams: state.UiReducers.EmployeeReducer.searchParams,
    currentPage : state.UiReducers.EmployeeReducer.currentPage,
    totalPages  : state.UiReducers.EmployeeReducer.totalPages,
    ids         : state.UiReducers.EmployeeReducer.ids,

    employees: state.DataReducers.DataReducer.employees,
    fetching : state.DataReducers.DataReducer.fetching,
    editing  : state.DataReducers.DataReducer.editing,
    deleting : state.DataReducers.DataReducer.deleting,
    creating : state.DataReducers.DataReducer.creating,
    error    : state.DataReducers.DataReducer.error
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchEmployees: (page, params) => dispatch(
      fetchEmployees(page, params)
    ),
    editEmployee      : (data) => dispatch(editEmployee(data)),
    createEmployee    : (data) => dispatch(createEmployee(data)),
    deleteEmployee    : (id) => dispatch(deleteEmployee(id)),
    setEmpSearchParams: (params) => dispatch(setEmpSearchParams(params))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Employee);
