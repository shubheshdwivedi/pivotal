import React from 'react';
import {Button, ButtonToolbar, Container, Row} from 'react-bootstrap';

const SectionList = (props) => {
  const sectionList = [];
  props.sections.forEach((section, i) => {
    sectionList.push(
      <Container key={i}>
        <Row className={'grid-card add-shadow mb-4 px-5 py-5'}
          style={{margin: 'auto'}}>
          <div style={{width: '100%'}}>
            <h4 style={{display: 'block'}}>Section Name: {section.name}</h4>
            <hr/>
            <h5 className={'text-muted'}>{section.details}</h5>
            <ButtonToolbar className={'justify-content-center'}>
              <Button
                variant="primary"
                className={'add-bg action-btn add-shadow mt-2 mr-3'}>
                Add Question
              </Button>
              <Button
                variant="primary"
                className={'add-bg action-btn add-shadow mt-2'}>
                Add Cluster
              </Button>
            </ButtonToolbar>
          </div>
        </Row>
      </Container>
    );
  });
  return sectionList;
};

class Sections extends React.Component {
  render() {
    return (
      <SectionList sections={this.props.sections}/>
    );
  }
}

export default Sections;
