import React from 'react';
import {
  setPaperCrafting
} from '../../store/actions/papers';
import {connect} from 'react-redux';
import {Button, Container, Form} from 'react-bootstrap';
import Row from 'react-bootstrap/Row';
import styleFixes from '../../_helpers/bootstrapStyleHelpers';
import AddSectionModal from '../../components/modals/sections/AddSectionModal';
import Section from './Sections';
import routes from '../../_helpers/routeHelpers';

class PaperCraft extends React.Component {
  state = {
    toggleAddSectionShow: false,
    sections            : []
  };

  toggleAddSectionShow = (bool) => {
    this.setState({
      ...this.state,
      toggleAddSectionShow: bool
    });
  };

  addSection = (newSection) => {
    const sections = this.state.sections;
    sections.push(newSection);
    this.setState({...this.state, sections: sections});
  };

  render() {
    return (
      <Container className={'px-4 py-4'}>

        {/* EDIT MODAL*/}
        {this.state.toggleAddSectionShow && <AddSectionModal
          show={this.state.toggleAddSectionShow}
          onHide={this.toggleAddSectionShow}
          /*
           * abhi main bas add kar de raha, tu dispatch
           * ke through state mein bhej dena
           */
          addSection={this.addSection}
        />}
        <h3>Paper Title</h3>
        <hr/>
        <h5>Test Duration: 90 minutes</h5>
        <Row className={'px-4 py-4 justify-content-center'}>
          <Form
            style={styleFixes.customWidth(100)}>
            <Form.Control className={'px-4 py-4'}
              as="textarea" rows="7" placeholder="Description" />
          </Form>
        </Row>
        <Container>
          <Section sections={this.state.sections}/>
        </Container>
        <Row className={'justify-content-center'}>
          <Button
            onClick={() => this.toggleAddSectionShow(true)}
            variant="primary"
            className={'add-bg action-btn add-shadow mt-2'}>
            Add a New Section
          </Button>
        </Row>

        <div>
          <button onClick={() => {
            this.props.history.push(routes.paperList);
            this.props.setPaperCrafting(false);
          }}>
            DONE CRAFTING
          </button>
        </div>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    craftingModePaper: state.UiReducers.PaperReducer.craftingModePaper,
    craftingMode     : state.UiReducers.PaperReducer.craftingMode
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setPaperCrafting:
      (bool, paper = undefined) => dispatch(setPaperCrafting(bool, paper))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PaperCraft);
