import React from 'react';
import {connect} from 'react-redux';
import {
  deleteStudent,
  editStudent,
  fetchStudents,
  createStudent,
  setStuSearchParams
} from '../../store/actions/students';
import List from '../../components/list/List';
import {Row, Container, Breadcrumb} from 'react-bootstrap';
import Searchbar from '../../components/searchbar/Searchbar';
import Preloader from '../../components/preloader/Preloader';
import Paginator from '../../components/paginator';
import {
  StudentDeleteModal,
  StudentEditModal,
  StudentCreateModal
} from '../../components/modals';
import routes from '../../_helpers/routeHelpers';
import styleFixes from '../../_helpers/bootstrapStyleHelpers';

class Student extends React.Component {
  state = {
    editModalShow  : false,
    createModalShow: false,
    deleteModalShow: false,
    editModalStu   : null,
    deleteModalStu : null
  };

  toggleDeleteModalShow = (bool, stu = null) => {
    this.setState({
      ...this.state,
      deleteModalShow: bool,
      deleteModalStu : stu
    });
  };

  toggleEditModalShow = (bool, stu = null) => {
    this.setState({
      ...this.state,
      editModalShow: bool,
      editModalStu : stu
    });
  };

  toggleCreateModalShow = (bool) => {
    this.setState({
      ...this.state,
      createModalShow: bool
    });
  };

  isSearch = () => {
    return this.props.searchParams !== null;
  };

  create = (e) => {
    e.preventDefault();
    this.toggleCreateModalShow(true);
  };

  searchParams = (e) => {
    e.preventDefault();
    const form = e.target.form;
    const name = form.name.value;

    const params = {name};
    this.props.setStuSearchParams(params);
  };

  getStudents = () => {
    const {ids, currentPage, students} = this.props;
    const currentIds = ids[currentPage];
    const stu = {};

    if (currentIds) {
      currentIds.forEach((id) => stu[id] = students[id]);
    }
    return stu;
  };

  fetchStu = (page) => {
    this.props.fetchStudents(page, this.props.searchParams);
  };

  getCrumbs = () => {
    const crubms = [];
    crubms.push(<Breadcrumb.Item href={routes.dashboard} key={0}>
      Dashboard
    </Breadcrumb.Item>);

    crubms.push(<Breadcrumb.Item href={routes.studentList} key={1} active>
      Student
    </Breadcrumb.Item>);
    return crubms;
  };

  isLoading() {
    return (
      this.props.fetching ||
      this.props.editing ||
      this.props.deleting ||
      this.props.creating
    );
  }

  componentDidMount() {
    this.fetchStu(this.props.currentPage);
  }

  render() {
    if (this.isLoading()) return (<Preloader/>);

    return (
      <Container className="pt-5">

        {/* DELETE MODAL*/}
        {this.state.deleteModalShow && <StudentDeleteModal
          show={this.state.deleteModalShow}
          onHide={this.toggleDeleteModalShow}
          stu={this.state.deleteModalStu}
          del={this.props.deleteStudent}
        />}

        {/* EDIT MODAL*/}
        {this.state.editModalShow && <StudentEditModal
          show={this.state.editModalShow}
          onHide={this.toggleEditModalShow}
          stu={this.state.editModalStu}
          edit={this.props.editStudent}
        />}

        {/* CREATE MODAL*/}
        {this.state.createModalShow && <StudentCreateModal
          show={this.state.createModalShow}
          onHide={this.toggleCreateModalShow}
          create={this.props.createStudent}
        />}

        <Searchbar
          data={this.props.searchParams}
          submit={this.searchParams}
          toggleCreate={this.create}
        /><br/>

        <Row className={'justify-content-left'}
          style={styleFixes.noMarginPadding}>
          <Breadcrumb>
            {this.getCrumbs()}
          </Breadcrumb>
        </Row>
        <List
          main="name"
          under="groups"
          toggleEdit={this.toggleEditModalShow}
          toggleDel={this.toggleDeleteModalShow}
          objects={this.getStudents()}
        />
        <br/>

        <Row className={'justify-content-center'}>
          <Paginator
            currentPage={this.props.currentPage}
            totalPages={this.props.totalPages}
            fetch={this.fetchStu}
          />
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    searchParams: state.UiReducers.StudentReducer.searchParams,
    currentPage : state.UiReducers.StudentReducer.currentPage,
    totalPages  : state.UiReducers.StudentReducer.totalPages,
    ids         : state.UiReducers.StudentReducer.ids,

    students: state.DataReducers.DataReducer.students,
    fetching: state.DataReducers.DataReducer.fetching,
    creating: state.DataReducers.DataReducer.creating,
    editing : state.DataReducers.DataReducer.editing,
    deleting: state.DataReducers.DataReducer.deleting,
    error   : state.DataReducers.DataReducer.error
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchStudents: (page, params) => dispatch(
      fetchStudents(page, params)
    ),
    editStudent       : (data) => dispatch(editStudent(data)),
    createStudent     : (data) => dispatch(createStudent(data)),
    deleteStudent     : (id) => dispatch(deleteStudent(id)),
    setStuSearchParams: (params) => dispatch(setStuSearchParams(params))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Student);
