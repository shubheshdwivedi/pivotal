import React from 'react';
import {connect} from 'react-redux';
import {
  fetchCourseModules,
  deleteCourseModule,
  setCourseSearchParams,
  createCourseModule,
  editCourseModule,
  setModuleId
} from '../../store/actions/courses';
import styleFixes from '../../_helpers/bootstrapStyleHelpers';
import List from '../../components/list/List';
import {Row, Container, Breadcrumb} from 'react-bootstrap';
import Searchbar from '../../components/searchbar/Searchbar';
import Preloader from '../../components/preloader/Preloader';
import Paginator from '../../components/paginator';
import {
  ModuleCreateModal,
  ModuleDeleteModal,
  ModuleEditModal
} from '../../components/modals';
import routes from '../../_helpers/routeHelpers';


class Module extends React.Component {
  state = {
    createModalShow  : false,
    deleteModalShow  : false,
    editModalShow    : false,
    editModalModule  : null,
    deleteModalModule: null
  };

  toggleCreateModalShow = (bool) => {
    this.setState({
      ...this.state,
      createModalShow: bool
    });
  };

  toggleDeleteModalShow = (bool, module = null) => {
    this.setState({
      ...this.state,
      deleteModalShow  : bool,
      deleteModalModule: module
    });
  };

  toggleEditModalShow = (bool, module = null) => {
    this.setState({
      ...this.state,
      editModalShow  : bool,
      editModalModule: module
    });
  };

  create = (e) => {
    e.preventDefault();
    this.toggleCreateModalShow(true);
  };

  searchParams = (e) => {
    e.preventDefault();
    const form = e.target.form;
    const name = form.name.value;

    const params = {name};
    this.props.setCourseSearchParams(params);
  };

  getModules = () => {
    const {ids, currentPage, courseModules, courseId} = this.props;
    const currentIds = ids[currentPage];
    const modules = courseModules[courseId];
    const mods = {};

    if (currentIds && courseId && modules) {
      currentIds.forEach((id) => mods[id] = modules[id]);
    }
    return mods;
  };

  getTopics = (module) => {
    const moduleId = module.id;
    this.props.setModuleId(moduleId);
    return this.props.history.push(routes.moduleTopicList);
  };

  fetchModules = (page) => {
    const {courseId, searchParams} = this.props;
    this.props.fetchModules(courseId, page, searchParams);
  };

  getCrumbs = () => {
    const crubms = [];
    crubms.push(<Breadcrumb.Item href={routes.dashboard} key={0}>
      Dashboard
    </Breadcrumb.Item>);

    crubms.push(<Breadcrumb.Item href={routes.courseList} key={1}>
      Courses
    </Breadcrumb.Item>);

    crubms.push(<Breadcrumb.Item href={routes.courseModuleList} key={2} active>
      Modules
    </Breadcrumb.Item>);
    return crubms;
  };

  isLoading() {
    return (
      this.props.fetching ||
      this.props.editing ||
      this.props.deleting ||
      this.props.creating
    );
  }

  componentDidMount() {
    this.fetchModules(this.props.currentPage);
  }

  render() {
    if (this.isLoading()) return (<Preloader/>);
    if (!this.props.courseId) return (<h1>HELLO</h1>);

    return (
      <Container className="pt-5">

        {/* DELETE MODAL*/}
        {this.state.deleteModalShow && <ModuleDeleteModal
          show={this.state.deleteModalShow}
          onHide={this.toggleDeleteModalShow}
          courseId={this.props.courseId}
          module={this.state.deleteModalModule}
          del={this.props.deleteModule}
        />}

        {/* CREATE MODAL*/}
        {this.state.createModalShow && <ModuleCreateModal
          show={this.state.createModalShow}
          onHide={this.toggleCreateModalShow}
          courseId={this.props.courseId}
          create={this.props.createModule}
        />}

        {/* EDIT MODAL*/}
        {this.state.editModalShow && <ModuleEditModal
          show={this.state.editModalShow}
          onHide={this.toggleEditModalShow}
          courseId={this.props.courseId}
          module={this.state.editModalModule}
          edit={this.props.editModule}
        />}

        <Searchbar
          data={this.props.searchParams}
          submit={this.searchParams}
          toggleCreate={this.create}
        /><br/>

        <Row className={'justify-content-left'}
          style={styleFixes.noMarginPadding}>
          <Breadcrumb>
            {this.getCrumbs()}
          </Breadcrumb>
        </Row>

        <List
          main="name"
          under="id"
          toggleEdit={this.toggleEditModalShow}
          toggleDel={this.toggleDeleteModalShow}
          objects={this.getModules()}
          others={[
            {
              label : 'Topics',
              action: this.getTopics
            }
          ]}
        /><br/>

        <Row className={'justify-content-center'}>
          <Paginator
            currentPage={this.props.currentPage}
            totalPages={this.props.totalPages}
            fetch={this.fetchModules}
          />
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    searchParams: state.UiReducers.CourseReducer.searchParams,
    currentPage : state.UiReducers.CourseReducer.courseModCurrentPage,
    totalPages  : state.UiReducers.CourseReducer.totalPages,
    ids         : state.UiReducers.CourseReducer.courseModIds,
    courseId    : state.UiReducers.CourseReducer.courseId,

    courseModules: state.DataReducers.DataReducer.courseModules,
    fetching     : state.DataReducers.DataReducer.fetching,
    creating     : state.DataReducers.DataReducer.creating,
    editing      : state.DataReducers.DataReducer.editing,
    deleting     : state.DataReducers.DataReducer.deleting,
    error        : state.DataReducers.DataReducer.error
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    createModule: (id, data) => dispatch(createCourseModule(id, data)),
    fetchModules: (id, page, params) => dispatch(
      fetchCourseModules(id, page, params)
    ),
    editModule: (id, moduleId, data) => dispatch(
      editCourseModule(id, moduleId, data)
    ),
    deleteModule: (id, moduleId) => dispatch(
      deleteCourseModule(id, moduleId)
    ),

    setModuleId          : (id) => dispatch(setModuleId(id)),
    setCourseSearchParams: (params) => dispatch(setCourseSearchParams(params))};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Module);
