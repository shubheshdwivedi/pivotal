import React from 'react';
import {connect} from 'react-redux';
import {
  createMentee,
  deleteMentee,
  editMentee,
  fetchMentees
} from '../../store/actions/mentees';
import List from '../../components/list/List';
import {Row, Container, Breadcrumb} from 'react-bootstrap';
import Searchbar from '../../components/searchbar/Searchbar';
import Preloader from '../../components/preloader/Preloader';
import Paginator from '../../components/paginator';
import {
  MenteeDeleteModal,
  MenteeCreateModal,
  MenteeEditModal
} from '../../components/modals';
import routes from '../../_helpers/routeHelpers';
import styleFixes from '../../_helpers/bootstrapStyleHelpers';


class Mentee extends React.Component {
  state = {
    editModalShow  : false,
    createModalShow: false,
    deleteModalShow: false,
    editModalMen   : null,
    deleteModalMen : null
  };

  toggleDeleteModalShow = (bool, stu = null) => {
    this.setState({
      ...this.state,
      deleteModalShow: bool,
      deleteModalMen : stu
    });
  };

  toggleEditModalShow = (bool, stu = null) => {
    this.setState({
      ...this.state,
      editModalShow: bool,
      editModalMen : stu
    });
  };

  toggleCreateModalShow = (bool) => {
    this.setState({
      ...this.state,
      createModalShow: bool
    });
  };

  isSearch = () => {
    return this.props.searchParams !== null;
  };

  create = (e) => {
    e.preventDefault();
    this.toggleCreateModalShow(true);
  };

  searchParams = (e) => {
    e.preventDefault();
    const form = e.target.form;
    const name = form.name.value;

    const params = {name};
    this.props.setStuSearchParams(params);
  };

  getMentees = () => {
    const {ids, currentPage, mentees} = this.props;
    const currentIds = ids[currentPage];
    const men = {};

    if (currentIds) {
      currentIds.forEach((id) => men[id] = mentees[id]);
    }
    return men;
  };

  fetchMen = (page) => {
    this.props.fetchMentees(page, this.props.searchParams);
  };

  getCrumbs = () => {
    const crubms = [];
    crubms.push(<Breadcrumb.Item href={routes.dashboard} key={0}>
      Dashboard
    </Breadcrumb.Item>);

    crubms.push(<Breadcrumb.Item href={routes.menteeList} key={1} active>
      Mentee
    </Breadcrumb.Item>);
    return crubms;
  };

  isLoading() {
    return (
      this.props.fetching ||
      this.props.editing ||
      this.props.deleting ||
      this.props.creating ||
      this.props.fetchingMentors
    );
  }

  componentDidMount() {
    this.fetchMen(this.props.currentPage);
  }

  render() {
    if (this.isLoading()) return (<Preloader/>);

    return (
      <Container className="pt-5">

        {/* DELETE MODAL*/}
        {this.state.deleteModalShow && <MenteeDeleteModal
          show={this.state.deleteModalShow}
          onHide={this.toggleDeleteModalShow}
          mentee={this.state.deleteModalMen}
          del={this.props.deleteMentee}
        />}

        {/* EDIT MODAL*/}
        {this.state.editModalShow && <MenteeEditModal
          show={this.state.editModalShow}
          onHide={this.toggleEditModalShow}
          mentee={this.state.editModalMen}
          edit={this.props.editMentee}
        />}

        {/* CREATE MODAL*/}
        {this.state.createModalShow && <MenteeCreateModal
          show={this.state.createModalShow}
          onHide={this.toggleCreateModalShow}
          create={this.props.createMentee}
        />}

        <Searchbar
          data={this.props.searchParams}
          submit={this.searchParams}
          toggleCreate={this.create}
        /><br/>

        <Row className={'justify-content-left'}
          style={styleFixes.noMarginPadding}>
          <Breadcrumb>
            {this.getCrumbs()}
          </Breadcrumb>
        </Row>
        <List
          main="name"
          under="mentorName"
          toggleEdit={this.toggleEditModalShow}
          toggleDel={this.toggleDeleteModalShow}
          objects={this.getMentees()}
        />
        <br/>

        <Row className={'justify-content-center'}>
          <Paginator
            currentPage={this.props.currentPage}
            totalPages={this.props.totalPages}
            fetch={this.fetchMen}
          />
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    searchParams: state.UiReducers.MenteeReducer.searchParams,
    currentPage : state.UiReducers.MenteeReducer.currentPage,
    totalPages  : state.UiReducers.MenteeReducer.totalPages,
    ids         : state.UiReducers.MenteeReducer.ids,

    mentees        : state.DataReducers.DataReducer.mentees,
    fetchingMentors: state.DataReducers.DataReducer.fetchingMentors,
    fetching       : state.DataReducers.DataReducer.fetching,
    creating       : state.DataReducers.DataReducer.creating,
    editing        : state.DataReducers.DataReducer.editing,
    deleting       : state.DataReducers.DataReducer.deleting,
    error          : state.DataReducers.DataReducer.error
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchMentees: (page, params) => dispatch(
      fetchMentees(page, params)
    ),
    createMentee: (data) => dispatch(createMentee(data)),
    editMentee  : (data) => dispatch(editMentee(data)),
    deleteMentee: (id) => dispatch(deleteMentee(id))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Mentee);
