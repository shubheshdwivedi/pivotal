import React from 'react';
import {connect} from 'react-redux';
import {
  fetchGroup,
  editGroup,
  deleteGroup,
  setGroSearchParams, setGroupId
} from '../../store/actions/groups/';
import List from '../../components/list/List';
import {Row, Container, Breadcrumb} from 'react-bootstrap';
import Searchbar from '../../components/searchbar/Searchbar';
import {
  GroupDeleteModal,
  GroupEditModal,
  GroupCreateModal
} from '../../components/modals';
import Preloader from '../../components/preloader/Preloader';
import Paginator from '../../components/paginator';
import routes from '../../_helpers/routeHelpers';
import createGroup from '../../store/actions/groups/createGroup';

class Group extends React.Component {
  state = {
    editModalShow  : false,
    createModalShow: false,
    deleteModalShow: false,
    editModalGro   : null,
    deleteModalGro : null
  };

  toggleDeleteModalShow = (bool, gro = null) => {
    this.setState({
      ...this.state,
      deleteModalShow: bool,
      deleteModalGro : gro
    });
  };

  toggleEditModalShow = (bool, gro = null) => {
    this.setState({
      ...this.state,
      editModalShow: bool,
      editModalGro : gro
    });
  };

  toggleCreateModalShow = (bool) => {
    this.setState({
      ...this.state,
      createModalShow: bool
    });
  };

  create = (e) => {
    e.preventDefault();
    this.toggleCreateModalShow(true);
  };

  searchParams = (e) => {
    e.preventDefault();
    const form = e.target.form;
    const name = form.name.value;

    const params = {name};
    this.props.setGroSearchParams(params);
  };

  getGroups = () => {
    const {ids, currentPage, groups} = this.props;
    const currentIds = ids[currentPage];

    const gro = {};
    if (currentIds) {
      currentIds.forEach((id) => gro[id] = groups[id]);
    }
    return gro;
  };

  getStudents = (grp) => {
    const grpId = grp.id;
    this.props.setGroupId(grpId);
    return this.props.history.push(routes.groupStudentList);
  };

  fetchGro = (page) => {
    this.props.fetchGroup(page, this.props.searchParams);
  };

  getCrumbs = () => {
    const crumbs = [];
    crumbs.push(<Breadcrumb.Item href={routes.dashboard} key={0}>
      Dashboard
    </Breadcrumb.Item>);

    crumbs.push(<Breadcrumb.Item href={routes.groupList} key={1} active>
      Groups
    </Breadcrumb.Item>);
    return crumbs;
  };

  isLoading() {
    return (
      this.props.fetching ||
      this.props.editing ||
      this.props.deleting ||
      this.props.creating
    );
  }

  componentDidMount() {
    this.fetchGro(this.props.currentPage);
  }

  render() {
    if (this.isLoading()) return (<Preloader/>);

    return (
      <Container className="pt-5">

        {/* DELETE MODAL*/}
        {this.state.deleteModalShow && <GroupDeleteModal
          show={this.state.deleteModalShow}
          onHide={this.toggleDeleteModalShow}
          gro={this.state.deleteModalGro}
          del={this.props.deleteGroup}
        />}

        {/* EDIT MODAL*/}
        {this.state.editModalShow && <GroupEditModal
          show={this.state.editModalShow}
          onHide={this.toggleEditModalShow}
          gro={this.state.editModalGro}
          edit={this.props.editGroup}
        />}

        {/* CREATE MODAL*/}
        {this.state.createModalShow && <GroupCreateModal
          show={this.state.createModalShow}
          onHide={this.toggleCreateModalShow}
          create={this.props.createGroup}
        />}

        <Searchbar
          data={this.props.searchParams}
          submit={this.searchParams}
          toggleCreate={this.create}
        /><br/>

        <Row className={'justify-content-left ml-5'}>
          <Breadcrumb>
            {this.getCrumbs()}
          </Breadcrumb>
        </Row>
        <List
          main="name"
          under="id"
          toggleEdit={this.toggleEditModalShow}
          others={[
            {
              label : 'Students',
              action: this.getStudents
            }
          ]}
          toggleDel={this.toggleDeleteModalShow}
          objects={this.getGroups()}
        />
        <br/>

        <Row className={'justify-content-center'}>
          <Paginator
            currentPage={this.props.currentPage}
            totalPages={this.props.totalPages}
            fetch={this.fetchGro}
          />
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    searchParams: state.UiReducers.GroupReducer.searchParams,
    currentPage : state.UiReducers.GroupReducer.currentPage,
    totalPages  : state.UiReducers.GroupReducer.totalPages,
    ids         : state.UiReducers.GroupReducer.ids,

    groups  : state.DataReducers.DataReducer.groups,
    fetching: state.DataReducers.DataReducer.fetching,
    editing : state.DataReducers.DataReducer.editing,
    deleting: state.DataReducers.DataReducer.deleting,
    creating: state.DataReducers.DataReducer.creating,
    error   : state.DataReducers.DataReducer.error
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchGroup: (page, params) => dispatch(
      fetchGroup(page, params)
    ),
    setGroupId        : (id) => dispatch(setGroupId(id)),
    editGroup         : (data) => dispatch(editGroup(data)),
    createGroup       : (data) => dispatch(createGroup(data)),
    deleteGroup       : (id) => dispatch(deleteGroup(id)),
    setGroSearchParams: (params) => dispatch(setGroSearchParams(params))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Group);
