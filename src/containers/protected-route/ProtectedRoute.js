import React from 'react';
import {Redirect} from 'react-router';
import {connect} from 'react-redux';
import logoutAction from '../../store/actions/auth/logoutAction';
import {isLoggedIn} from '../../_helpers/exportConsts';
import routes from '../../_helpers/routeHelpers';

class ProtectedRoute extends React.Component {
  redirect = () => {
    this.props.logout();
    return <Redirect to={routes.auth}/>;
  };

  render() {
    if (isLoggedIn() &&
      this.props.user !== null &&
      this.props.token !== null
    ) return <this.props.next {...this.props} user={this.props.user}/>;

    return this.redirect();
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.DataReducers.AuthReducer.token,
    user : state.DataReducers.AuthReducer.user
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    logout: () => {
      dispatch(logoutAction());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProtectedRoute);
