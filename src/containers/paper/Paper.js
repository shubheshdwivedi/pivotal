import React from 'react';
import {connect} from 'react-redux';
import {
  deletePaper,
  fetchPapers,
  setPaperCrafting,
  setPaperSearchParams
} from '../../store/actions/papers/';
import List from '../../components/list/List';
import {Row, Container, Breadcrumb} from 'react-bootstrap';
import Searchbar from '../../components/searchbar/Searchbar';
import Preloader from '../../components/preloader/Preloader';
import Paginator from '../../components/paginator';
import routes from '../../_helpers/routeHelpers';
import {PaperDeleteModal} from '../../components/modals/';
import {Redirect} from 'react-router';
import PaperCreateModal from '../../components/modals/papers/PaperCreateModal';

// Jo bhi kariyo samjha diyo, aur kuch gadbad ho toh bata diyo

class Paper extends React.Component {
  state = {
    deleteModalShow : false,
    deleteModalPaper: null
  };

  toggleDeleteModalShow = (bool, paper = null) => {
    this.setState({
      ...this.state,
      deleteModalShow : bool,
      deleteModalPaper: paper
    });
  };

  toggleCreateModalShow = (bool) => {
    this.setState({
      ...this.state,
      createModalShow: bool
    });
  };

  toggleEditingMode = (bool, paper = null) => {
    this.props.setPaperCrafting(bool, paper);
  };

  create = (e) => {
    e.preventDefault();
    this.toggleCreateModalShow(true);
  };

  searchParams = (e) => {
    e.preventDefault();
    const form = e.target.form;
    const title = form.name.value;

    const params = {title};
    this.props.setPaperSearchParams(params);
  };

  getPapers = () => {
    const {ids, currentPage, papers} = this.props;
    const currentIds = ids[currentPage];

    const paps = {};
    if (currentIds) {
      currentIds.forEach((id) => paps[id] = papers[id]);
    }
    return paps;
  };

  fetchPapers = (page) => {
    this.props.fetchPapers(page, this.props.searchParams);
  };

  getCrumbs = () => {
    const crumbs = [];
    crumbs.push(<Breadcrumb.Item href={routes.dashboard} key={0}>
      Dashboard
    </Breadcrumb.Item>);

    crumbs.push(<Breadcrumb.Item href={routes.paperList} key={1} active>
      Papers
    </Breadcrumb.Item>);
    return crumbs;
  };

  isLoading() {
    return (
      this.props.fetching ||
      this.props.editing ||
      this.props.deleting ||
      this.props.creating
    );
  }

  isCrafting = () => {
    return this.props.craftingMode;
  };

  createPaper = () => {
  };

  componentDidMount() {
    this.fetchPapers(this.props.currentPage);
  }

  render() {
    if (this.isCrafting()) {
      return <Redirect to={routes.paperCrafting}/>;
    } if (this.isLoading()) return (<Preloader/>);

    return (
      <Container className="pt-5">
        {this.state.deleteModalShow && <PaperDeleteModal
          show={this.state.deleteModalShow}
          onHide={this.toggleDeleteModalShow}
          paper={this.state.deleteModalPaper}
          del={this.props.deletePaper}
        />}

        {/* /!* EDIT MODAL*!/*/}
        {/* {this.state.editModalShow && <EmployeeEditModal*/}
        {/*  show={this.state.editModalShow}*/}
        {/*  onHide={this.toggleEditingMode}*/}
        {/*  emp={this.state.editModalEmp}*/}
        {/*  edit={this.props.editEmployee}*/}
        {/* />}*/}

        {/* CREATE MODAL*/}
        {this.state.createModalShow && <PaperCreateModal
          show={this.state.createModalShow}
          onHide={this.toggleCreateModalShow}
          create={this.createPaper}
        />}

        <Searchbar
          data={this.props.searchParams}
          submit={this.searchParams}
          toggleCreate={this.create}
        /><br/>

        <Row className={'justify-content-left ml-5'}>
          <Breadcrumb>
            {this.getCrumbs()}
          </Breadcrumb>
        </Row>
        <List
          main="title"
          under="id"
          toggleEdit={this.toggleEditingMode}
          toggleDel={this.toggleDeleteModalShow}
          objects={this.getPapers()}
        />
        <br/>

        <Row className={'justify-content-center'}>
          <Paginator
            currentPage={this.props.currentPage}
            totalPages={this.props.totalPages}
            fetch={this.fetchPapers}
          />
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    searchParams     : state.UiReducers.PaperReducer.searchParams,
    currentPage      : state.UiReducers.PaperReducer.currentPage,
    totalPages       : state.UiReducers.PaperReducer.totalPages,
    ids              : state.UiReducers.PaperReducer.ids,
    craftingModePaper: state.UiReducers.PaperReducer.craftingModePaper,
    craftingMode     : state.UiReducers.PaperReducer.craftingMode,

    papers  : state.DataReducers.DataReducer.papers,
    fetching: state.DataReducers.DataReducer.fetching,
    editing : state.DataReducers.DataReducer.editing,
    deleting: state.DataReducers.DataReducer.deleting,
    creating: state.DataReducers.DataReducer.creating,
    error   : state.DataReducers.DataReducer.error
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchPapers     : (page, params) => dispatch(fetchPapers(page, params)),
    deletePaper     : (id) => dispatch(deletePaper(id)),
    setPaperCrafting:
      (bool, paper) => dispatch(setPaperCrafting(bool, paper)),
    setPaperSearchParams: (params) => dispatch(setPaperSearchParams(params))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Paper);
