import React from 'react';
import {connect} from 'react-redux';
import {
  fetchModuleTopics,
  deleteModuleTopic,
  setCourseSearchParams,
  createModuleTopic,
  editModuleTopic
} from '../../store/actions/courses';
import styleFixes from '../../_helpers/bootstrapStyleHelpers';
import List from '../../components/list/List';
import {Row, Container, Breadcrumb} from 'react-bootstrap';
import Searchbar from '../../components/searchbar/Searchbar';
import Preloader from '../../components/preloader/Preloader';
import Paginator from '../../components/paginator';
import {
  TopicCreateModal,
  TopicDeleteModal,
  TopicEditModal
} from '../../components/modals';
import routes from '../../_helpers/routeHelpers';


class Topic extends React.Component {
  state = {
    createModalShow : false,
    deleteModalShow : false,
    editModalShow   : false,
    editModalTopic  : null,
    deleteModalTopic: null
  };

  toggleCreateModalShow = (bool) => {
    this.setState({
      ...this.state,
      createModalShow: bool
    });
  };

  toggleDeleteModalShow = (bool, topic = null) => {
    this.setState({
      ...this.state,
      deleteModalShow : bool,
      deleteModalTopic: topic
    });
  };

  toggleEditModalShow = (bool, topic = null) => {
    this.setState({
      ...this.state,
      editModalShow : bool,
      editModalTopic: topic
    });
  };

  create = (e) => {
    e.preventDefault();
    this.toggleCreateModalShow(true);
  };

  searchParams = (e) => {
    e.preventDefault();
    const form = e.target.form;
    const name = form.name.value;

    const params = {name};
    this.props.setCourseSearchParams(params);
  };

  getTopics = () => {
    const {ids, currentPage, moduleTopics, moduleId} = this.props;
    const currentIds = ids[currentPage];
    const topics = moduleTopics[moduleId];
    const tops = {};

    if (currentIds && moduleId && topics) {
      currentIds.forEach((id) => tops[id] = topics[id]);
    }
    return tops;
  };

  fetchTopics = (page) => {
    const {courseId, moduleId, searchParams} = this.props;
    this.props.fetchTopics(courseId, moduleId, page, searchParams);
  };

  getCrumbs = () => {
    const crubms = [];
    crubms.push(<Breadcrumb.Item href={routes.dashboard} key={0}>
      Dashboard
    </Breadcrumb.Item>);

    crubms.push(<Breadcrumb.Item href={routes.courseList} key={1}>
      Courses
    </Breadcrumb.Item>);

    crubms.push(<Breadcrumb.Item href={routes.courseModuleList} key={2}>
      Modules
    </Breadcrumb.Item>);

    crubms.push(<Breadcrumb.Item href={routes.moduleTopicList} key={3} active>
      Topics
    </Breadcrumb.Item>);

    return crubms;
  };

  isLoading() {
    return (
      this.props.fetching ||
      this.props.editing ||
      this.props.deleting ||
      this.props.creating
    );
  }

  componentDidMount() {
    this.fetchTopics(this.props.currentPage);
  }

  render() {
    if (this.isLoading()) return (<Preloader/>);
    if (!this.props.moduleId) return (<h1>HELLO</h1>);

    return (
      <Container className="pt-5">

        {/* DELETE MODAL*/}
        {this.state.deleteModalShow && <TopicDeleteModal
          show={this.state.deleteModalShow}
          onHide={this.toggleDeleteModalShow}
          courseId={this.props.courseId}
          moduleId={this.props.moduleId}
          topic={this.state.deleteModalTopic}
          del={this.props.deleteTopic}
        />}

        {/* CREATE MODAL*/}
        {this.state.createModalShow && <TopicCreateModal
          show={this.state.createModalShow}
          onHide={this.toggleCreateModalShow}
          courseId={this.props.courseId}
          moduleId={this.props.moduleId}
          create={this.props.createTopic}
        />}

        {/* EDIT MODAL*/}
        {this.state.editModalShow && <TopicEditModal
          show={this.state.editModalShow}
          onHide={this.toggleEditModalShow}
          courseId={this.props.courseId}
          moduleId={this.props.moduleId}
          topic={this.state.editModalTopic}
          edit={this.props.editTopic}
        />}

        <Searchbar
          data={this.props.searchParams}
          submit={this.searchParams}
          toggleCreate={this.create}
        /><br/>

        <Row className={'justify-content-left'}
          style={styleFixes.noMarginPadding}>
          <Breadcrumb>
            {this.getCrumbs()}
          </Breadcrumb>
        </Row>

        <List
          main="name"
          under="description"
          toggleEdit={this.toggleEditModalShow}
          toggleDel={this.toggleDeleteModalShow}
          objects={this.getTopics()}
        /><br/>

        <Row className={'justify-content-center'}>
          <Paginator
            currentPage={this.props.currentPage}
            totalPages={this.props.totalPages}
            fetch={this.fetchTopics}
          />
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    searchParams: state.UiReducers.CourseReducer.searchParams,
    currentPage : state.UiReducers.CourseReducer.courseModCurrentPage,
    totalPages  : state.UiReducers.CourseReducer.totalPages,
    ids         : state.UiReducers.CourseReducer.modulesTopIds,
    courseId    : state.UiReducers.CourseReducer.courseId,
    moduleId    : state.UiReducers.CourseReducer.moduleId,

    moduleTopics: state.DataReducers.DataReducer.moduleTopics,
    fetching    : state.DataReducers.DataReducer.fetching,
    creating    : state.DataReducers.DataReducer.creating,
    editing     : state.DataReducers.DataReducer.editing,
    deleting    : state.DataReducers.DataReducer.deleting,
    error       : state.DataReducers.DataReducer.error
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    createTopic: (courseId, moduleId, data) => dispatch(
      createModuleTopic(courseId, moduleId, data)
    ),
    fetchTopics: (courseId, moduleId, page, params) => dispatch(
      fetchModuleTopics(courseId, moduleId, page, params)
    ),
    editTopic: (courseId, moduleId, id, data) => dispatch(
      editModuleTopic(courseId, moduleId, id, data)
    ),
    deleteTopic: (courseId, moduleId, id) => dispatch(
      deleteModuleTopic(courseId, moduleId, id)
    ),

    setCourseSearchParams: (params) => dispatch(setCourseSearchParams(params))};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Topic);
