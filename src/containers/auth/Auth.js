import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Redirect} from 'react-router';
import {isLoggedIn} from '../../_helpers/exportConsts';
import loginAction from '../../store/actions/auth/loginAction';
import routes from '../../_helpers/routeHelpers';
import './Auth.scss';
import {Button, Col, Form, Row} from 'react-bootstrap';
import styleFixes from '../../_helpers/bootstrapStyleHelpers';
import {APP_NAME} from '../../_helpers/exportConsts';
import Error from '../../components/errors/Error';

class Auth extends Component {
  fire = (e) => {
    e.preventDefault();
    const email = e.target.form.email.value;
    const password = e.target.form.password.value;

    this.props.login({
      email,
      password
    });
  };

  render() {
    if (isLoggedIn()) return <Redirect to={routes.dashboard}/>;
    const error = this.props.error;
    return (
      <Row className="landing" style={styleFixes.noMarginPadding}>
        <Col md="6" className="app-landing-aside">
          <h2 align="center" className={'app-name__' +
          'landing-page'}
          style={{...styleFixes.customPadding(4, 0, 0, 0), ...{color: '#fff'}}}>
            WELCOME TO ABES ENGINEERING COLLEGE</h2>
        </Col>
        <Col md="6" className="app-landing-loginForm">
          <h2 align="center" className={'app-name__' +
          'landing-page'}>{APP_NAME.toUpperCase()}</h2>
          <h5 align="center" className="subheading">Welcome back,
            please login to your account.</h5>
          <Form style={styleFixes.customPadding(5, 5, 5, 5)}>
            <Form.Group controlId="formBasicEmail">
              <Form.Control
                type="email"
                name="email"
                placeholder="Enter email"
                style={styleFixes.formInputs}/>
            </Form.Group>
            <Form.Group controlId="formBasicPassword">
              <Form.Control
                type="password"
                name="password"
                placeholder="Password"
                style={styleFixes.formInputs}
              />
            </Form.Group>
            <br/>
            <Row style={{...styleFixes.noMarginPadding,
              ...{'justifyContent': 'space-between'}}}>
              <Form.Group controlId="formBasicCheckbox">
                <div className="pretty p-curve p-default">
                  <input type="checkbox" id={'remember'} name={'remember'} />
                  <div className="state p-primary">
                    <label htmlFor={'remember'} style={{lineHeight: '1.1'}}>
                      Remember me</label>
                  </div>
                </div>
              </Form.Group>
              <a href={'/'}>Forgot Password?</a>
            </Row>
            <br/><br/>
            <Row style={{...styleFixes.noMarginPadding,
              ...{'justifyContent': 'center'}}}>
              <Button variant="primary"
                className="action-btn add-bg" type="submit"
                onClick={this.fire}>
                Login
              </Button>
            </Row>
            <br/><br/>
            {!error || <Error e={error}/>}
          </Form>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user : state.DataReducers.AuthReducer.user,
    error: state.DataReducers.AuthReducer.errors
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    login: (data) => dispatch(loginAction(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Auth);
