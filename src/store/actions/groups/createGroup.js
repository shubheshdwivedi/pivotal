import {CreateGroupRequest} from '../../../requests/groups/';
import {
  CREATE_BEGINS,
  CREATE_GROUPS_SUCCESS,
  CREATE_FAILED
} from '../index';

const createGroup = (data) => {
  return (dispatch) => {
    dispatch(createGroupBegins());
    (new CreateGroupRequest(data))
      .fire()
      .then((res) => dispatch(createGroupSuccess(res.data.data)))
      .catch((err) => dispatch(createGroupFailed(err.response.data.error)));
  };
};

const createGroupBegins = () => {
  return {
    type: CREATE_BEGINS
  };
};

const createGroupFailed = (message) => {
  return {
    type   : CREATE_FAILED,
    payload: {message}
  };
};

const createGroupSuccess = (group) => {
  return {
    type   : CREATE_GROUPS_SUCCESS,
    payload: {
      group
    }
  };
};

export default createGroup;
