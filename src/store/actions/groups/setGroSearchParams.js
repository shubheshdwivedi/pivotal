import {SET_GRO_SEARCH_PARAMS} from '../index';


const setGroSearchParams = (params) => {
  return {
    type   : SET_GRO_SEARCH_PARAMS,
    payload: {params}
  };
};

export default setGroSearchParams;
