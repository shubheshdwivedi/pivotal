import {FetchGroupStudentRequest} from '../../../../requests/groups/';
import {
  FETCH_BEGINS,
  FETCH_GROUPS_STUDENTS_SUCCESS,
  FETCH_FAILED
} from '../../index';

const fetchGroupStudents = (id, page, params) => {
  return (dispatch) => {
    dispatch(fetchGroupStudentsBegins());
    (new FetchGroupStudentRequest(id, page, params))
      .fire()
      .then((res) => dispatch(fetchGroupStudentsSuccess(
        id,
        res.data.data,
        page,
        res.data.metaData
      )))
      .catch((err) => dispatch(
        fetchGroupStudentsFailed(err.response.data.error
        ))
      );
  };
};

const fetchGroupStudentsBegins = () => {
  return {
    type: FETCH_BEGINS
  };
};

const fetchGroupStudentsFailed = (message) => {
  return {
    type   : FETCH_FAILED,
    payload: {message}
  };
};

const fetchGroupStudentsSuccess = (id, students, page, meta) => {
  return {
    type   : FETCH_GROUPS_STUDENTS_SUCCESS,
    payload: {
      id,
      students,
      page,
      meta
    }
  };
};

export default fetchGroupStudents;
