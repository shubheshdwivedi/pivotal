import {
  DELETE_BEGINS,
  DELETE_GROUPS_STUDENTS_SUCCESS,
  DELETE_FAILED
} from '../../index';
import {DeleteGroupStudentRequest} from '../../../../requests/groups/';


const deleteGroupStudent = (id, stuId) => {
  return (dispatch) => {
    dispatch(deleteGroupStudentBegins());
    (new DeleteGroupStudentRequest(id, stuId))
      .fire()
      .then((res) => dispatch(deleteGroupStudentSuccess(
        res.data.info,
        id,
        stuId
      )))
      .catch((err) => dispatch(deleteGroupStudentFailed(
        err.response.data.error
      )));
  };
};

const deleteGroupStudentBegins = () => {
  return {
    type: DELETE_BEGINS
  };
};

const deleteGroupStudentFailed = (message) => {
  return {
    type   : DELETE_FAILED,
    payload: {message}
  };
};

const deleteGroupStudentSuccess = (message, id, stuId) => {
  return {
    type   : DELETE_GROUPS_STUDENTS_SUCCESS,
    payload: {
      message,
      id,
      stuId
    }
  };
};

export default deleteGroupStudent;
