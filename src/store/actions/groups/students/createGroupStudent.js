import {
  CREATE_BEGINS,
  CREATE_GROUPS_STUDENTS_SUCCESS,
  CREATE_FAILED
} from '../../index';
import {CreateGroupStudentRequest} from '../../../../requests/groups/';


const createGroupStudent = (id, stuId) => {
  return (dispatch) => {
    dispatch(createGroupStudentBegins());
    (new CreateGroupStudentRequest(id, stuId))
      .fire()
      .then((res) => dispatch(createGroupStudentSuccess(
        res.data.info,
        id,
        stuId
      )))
      .catch((err) => dispatch(
        createGroupStudentFailed(err.response.data.error)
      ));
  };
};

const createGroupStudentBegins = () => {
  return {
    type: CREATE_BEGINS
  };
};

const createGroupStudentFailed = (message) => {
  return {
    type   : CREATE_FAILED,
    payload: {message}
  };
};

const createGroupStudentSuccess = (message, id, stuId) => {
  return {
    type   : CREATE_GROUPS_STUDENTS_SUCCESS,
    payload: {
      message,
      id,
      stuId
    }
  };
};

export default createGroupStudent;
