import {
  EDIT_BEGINS,
  EDIT_FAILED,
  EDIT_GROUPS_SUCCESS
} from '../index';
import {EditGroupRequest} from '../../../requests/groups/';


const editGroup = (data) => {
  return (dispatch) => {
    dispatch(editGroupBegins());
    (new EditGroupRequest(data))
      .fire()
      .then((res) => dispatch(editGroupSuccess(
        res.data.info,
        res.data.data
      )))
      .catch((err) => dispatch(editGroupFailed(err.response.data.error)));
  };
};

const editGroupBegins = () => {
  return {
    type: EDIT_BEGINS
  };
};

const editGroupFailed = (message) => {
  return {
    type   : EDIT_FAILED,
    payload: {message}
  };
};

const editGroupSuccess = (message, data) => {
  return {
    type   : EDIT_GROUPS_SUCCESS,
    payload: {
      message,
      data
    }
  };
};

export default editGroup;
