import {SET_GROUP_ID} from '../index';

const setGroupId = (id) => {
  return {
    type   : SET_GROUP_ID,
    payload: {id}
  };
};

export default setGroupId;
