import {FetchGroupRequest} from '../../../requests/groups/';
import {
  FETCH_BEGINS,
  FETCH_GROUPS_SUCCESS,
  FETCH_FAILED
} from '../index';

const fetchGroup = (page, params) => {
  return (dispatch) => {
    dispatch(fetchGroupsBegins());
    (new FetchGroupRequest(page, params))
      .fire()
      .then((res) => dispatch(fetchGroupsSuccess(
        res.data.data,
        page,
        res.data.metaData
      )))
      .catch((err) => dispatch(fetchGroupsFailed(err.response.data.error)));
  };
};

const fetchGroupsBegins = () => {
  return {
    type: FETCH_BEGINS
  };
};

const fetchGroupsFailed = (message) => {
  return {
    type   : FETCH_FAILED,
    payload: {message}
  };
};

const fetchGroupsSuccess = (groups, page, meta) => {
  return {
    type   : FETCH_GROUPS_SUCCESS,
    payload: {
      groups,
      page,
      meta
    }
  };
};

export default fetchGroup;
