import {DeleteGroupRequest} from '../../../requests/groups/';
import {
  DELETE_BEGINS,
  DELETE_GROUPS_SUCCESS,
  DELETE_FAILED
} from '../index';


const deleteGroup = (id) => {
  return (dispatch) => {
    dispatch(deleteGroupBegins());
    (new DeleteGroupRequest(id))
      .fire()
      .then((res) => dispatch(deleteGroupSuccess(res.data.info, id)))
      .catch((err) => dispatch(deleteGroupFailed(err.response.data.error)));
  };
};

const deleteGroupBegins = () => {
  return {
    type: DELETE_BEGINS
  };
};

const deleteGroupFailed = (message) => {
  return {
    type   : DELETE_FAILED,
    payload: {message}
  };
};

const deleteGroupSuccess = (message, id) => {
  return {
    type   : DELETE_GROUPS_SUCCESS,
    payload: {
      message,
      id
    }
  };
};

export default deleteGroup;
