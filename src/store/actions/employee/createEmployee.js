import {CreateEmployeeRequest} from '../../../requests/employees/';
import {
  CREATE_EMPLOYEES_SUCCESS,
  CREATE_BEGINS,
  CREATE_FAILED
} from '../index';

const createEmployee = (data) => {
  return (dispatch) => {
    dispatch(createEmployeeBegins());
    (new CreateEmployeeRequest(data))
      .fire()
      .then((res) => dispatch(createEmployeeSuccess(
        res.data.data
      )))
      .catch((err) => dispatch(createEmployeeFailed(err.response.data.error)));
  };
};

const createEmployeeBegins = () => {
  return {
    type: CREATE_BEGINS
  };
};

const createEmployeeFailed = (message) => {
  return {
    type   : CREATE_FAILED,
    payload: {message}
  };
};

const createEmployeeSuccess = (employee) => {
  return {
    type   : CREATE_EMPLOYEES_SUCCESS,
    payload: {
      employee
    }
  };
};

export default createEmployee;
