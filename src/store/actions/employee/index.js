import deleteEmployee from './deleteEmployee';
import editEmployee from './editEmployee';
import fetchEmployees from './fetchEmployees';
import createEmployee from './createEmployee';
import setEmpSearchParams from './setEmpSearchParams';

export {
  deleteEmployee,
  editEmployee,
  createEmployee,
  fetchEmployees,
  setEmpSearchParams
};
