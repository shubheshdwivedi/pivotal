import {DeleteEmployeeRequest} from '../../../requests/employees/';
import {
  DELETE_BEGINS,
  DELETE_EMPLOYEES_SUCCESS, DELETE_FAILED
} from '../index';


const deleteEmployee = (id) => {
  return (dispatch) => {
    dispatch(deleteEmployeeBegins());
    (new DeleteEmployeeRequest(id))
      .fire()
      .then((res) => dispatch(deleteEmployeeSuccess(res.data.info, id)))
      .catch((err) => dispatch(deleteEmployeeFailed(err.response.data.error)));
  };
};

const deleteEmployeeBegins = () => {
  return {
    type: DELETE_BEGINS
  };
};

const deleteEmployeeFailed = (message) => {
  return {
    type   : DELETE_FAILED,
    payload: {message}
  };
};

const deleteEmployeeSuccess = (message, id) => {
  return {
    type   : DELETE_EMPLOYEES_SUCCESS,
    payload: {
      message,
      id
    }
  };
};

export default deleteEmployee;
