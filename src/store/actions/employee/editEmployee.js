import {
  EDIT_BEGINS,
  EDIT_FAILED,
  EDIT_EMPLOYEES_SUCCESS
} from '../index';
import {EditEmployeeRequest} from '../../../requests/employees/';


const editEmployee = (data) => {
  return (dispatch) => {
    dispatch(editEmployeeBegins());
    (new EditEmployeeRequest(data))
      .fire()
      .then((res) => dispatch(editEmployeeSuccess(
        res.data.info,
        res.data.data
      )))
      .catch((err) => dispatch(editEmployeeFailed(err.response.data.error)));
  };
};

const editEmployeeBegins = () => {
  return {
    type: EDIT_BEGINS
  };
};

const editEmployeeFailed = (message) => {
  return {
    type   : EDIT_FAILED,
    payload: {message}
  };
};

const editEmployeeSuccess = (message, data) => {
  return {
    type   : EDIT_EMPLOYEES_SUCCESS,
    payload: {
      message,
      data
    }
  };
};

export default editEmployee;
