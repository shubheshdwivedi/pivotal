import {SET_EMP_SEARCH_PARAMS} from '../index';


const setEmpSearchParams = (params) => {
  return {
    type   : SET_EMP_SEARCH_PARAMS,
    payload: {params}
  };
};

export default setEmpSearchParams;
