import {FetchEmployeesRequest} from '../../../requests/employees/';
import {
  FETCH_BEGINS,
  FETCH_EMPLOYEES_SUCCESS,
  FETCH_FAILED
} from '../index';

const fetchEmployees = (page, params) => {
  return (dispatch) => {
    dispatch(fetchEmployeesBegins());
    (new FetchEmployeesRequest(page, params))
      .fire()
      .then((res) => dispatch(fetchEmployeesSuccess(
        res.data.data,
        page,
        res.data.metaData
      )))
      .catch((err) => dispatch(fetchEmployeesFailed(err.response.data.error)));
  };
};

const fetchEmployeesBegins = () => {
  return {
    type: FETCH_BEGINS
  };
};

const fetchEmployeesFailed = (message) => {
  return {
    type   : FETCH_FAILED,
    payload: {message}
  };
};

const fetchEmployeesSuccess = (employees, page, meta) => {
  return {
    type   : FETCH_EMPLOYEES_SUCCESS,
    payload: {
      employees,
      page,
      meta
    }
  };
};

export default fetchEmployees;
