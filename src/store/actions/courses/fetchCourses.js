import {
  FETCH_BEGINS,
  FETCH_COURSES_SUCCESS,
  FETCH_FAILED
} from '../index';
import {FetchCourseRequest} from '../../../requests/courses';

const fetchCourse = (page, params) => {
  return (dispatch) => {
    dispatch(fetchCoursesBegins());
    (new FetchCourseRequest(page, params))
      .fire()
      .then((res) => dispatch(fetchCoursesSuccess(
        res.data.data,
        page,
        res.data.metaData
      )))
      .catch((err) => dispatch(fetchCoursesFailed(err.response.data.error)));
  };
};

const fetchCoursesBegins = () => {
  return {
    type: FETCH_BEGINS
  };
};

const fetchCoursesFailed = (message) => {
  return {
    type   : FETCH_FAILED,
    payload: {message}
  };
};

const fetchCoursesSuccess = (courses, page, meta) => {
  return {
    type   : FETCH_COURSES_SUCCESS,
    payload: {
      courses,
      page,
      meta
    }
  };
};

export default fetchCourse;
