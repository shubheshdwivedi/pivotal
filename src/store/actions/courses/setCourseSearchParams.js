import {SET_COURSE_SEARCH_PARAMS} from '../index';


const setCourseSearchParams = (params) => {
  return {
    type   : SET_COURSE_SEARCH_PARAMS,
    payload: {params}
  };
};

export default setCourseSearchParams;
