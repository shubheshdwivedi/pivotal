import {
  DELETE_BEGINS,
  DELETE_COURSES_SUCCESS,
  DELETE_FAILED
} from '../index';
import {DeleteCourseRequest} from '../../../requests/courses';


const deleteCourse = (id) => {
  return (dispatch) => {
    dispatch(deleteCourseBegins());
    (new DeleteCourseRequest(id))
      .fire()
      .then((res) => dispatch(deleteCourseSuccess(res.data.info, id)))
      .catch((err) => dispatch(deleteCourseFailed(err.response.data.error)));
  };
};

const deleteCourseBegins = () => {
  return {
    type: DELETE_BEGINS
  };
};

const deleteCourseFailed = (message) => {
  return {
    type   : DELETE_FAILED,
    payload: {message}
  };
};

const deleteCourseSuccess = (message, id) => {
  return {
    type   : DELETE_COURSES_SUCCESS,
    payload: {
      message,
      id
    }
  };
};

export default deleteCourse;
