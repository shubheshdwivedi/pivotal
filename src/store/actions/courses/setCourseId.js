import {SET_COURSE_ID} from '../index';

const setCourseId = (id) => {
  return {
    type   : SET_COURSE_ID,
    payload: {id}
  };
};

export default setCourseId;
