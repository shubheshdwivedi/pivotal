import {
  DELETE_BEGINS,
  DELETE_COURSE_MODULES_SUCCESS,
  DELETE_FAILED
} from '../../index';
import {DeleteCourseModuleRequest} from '../../../../requests/courses/';


const deleteCourseModule = (id, moduleId) => {
  return (dispatch) => {
    dispatch(deleteCourseModuleBegins());
    (new DeleteCourseModuleRequest(id, moduleId))
      .fire()
      .then((res) => dispatch(deleteCourseModuleSuccess(
        res.data.info,
        id,
        moduleId
      )))
      .catch((err) => dispatch(deleteCourseModuleFailed(
        err.response.data.error
      )));
  };
};

const deleteCourseModuleBegins = () => {
  return {
    type: DELETE_BEGINS
  };
};

const deleteCourseModuleFailed = (message) => {
  return {
    type   : DELETE_FAILED,
    payload: {message}
  };
};

const deleteCourseModuleSuccess = (message, id, moduleId) => {
  return {
    type   : DELETE_COURSE_MODULES_SUCCESS,
    payload: {
      message,
      id,
      moduleId
    }
  };
};

export default deleteCourseModule;
