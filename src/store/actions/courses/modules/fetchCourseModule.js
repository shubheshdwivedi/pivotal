import {FetchCourseModuleRequest} from '../../../../requests/courses/';
import {
  FETCH_BEGINS,
  FETCH_COURSE_MODULES_SUCCESS,
  FETCH_FAILED
} from '../../index';

const fetchCourseModules = (id, page, params) => {
  return (dispatch) => {
    dispatch(fetchCourseModulesBegins());
    (new FetchCourseModuleRequest(id, page, params))
      .fire()
      .then((res) => dispatch(fetchCourseModulesSuccess(
        id,
        res.data.data,
        page,
        res.data.metaData
      )))
      .catch((err) => dispatch(
        fetchCourseModulesFailed(err.response.data.error
        ))
      );
  };
};

const fetchCourseModulesBegins = () => {
  return {
    type: FETCH_BEGINS
  };
};

const fetchCourseModulesFailed = (message) => {
  return {
    type   : FETCH_FAILED,
    payload: {message}
  };
};

const fetchCourseModulesSuccess = (id, modules, page, meta) => {
  return {
    type   : FETCH_COURSE_MODULES_SUCCESS,
    payload: {
      id,
      modules,
      page,
      meta
    }
  };
};

export default fetchCourseModules;
