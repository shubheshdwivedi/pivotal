import {
  EDIT_BEGINS,
  EDIT_MODULE_TOPIC_SUCCESS,
  EDIT_FAILED
} from '../../../index';
import {EditModuleTopicRequest} from '../../../../../requests/courses/';


const editModuleTopic = (courseId, moduleId, id, data) => {
  return (dispatch) => {
    dispatch(editModuleTopicBegins());
    (new EditModuleTopicRequest(courseId, moduleId, id, data))
      .fire()
      .then((res) => dispatch(editModuleTopicSuccess(
        res.data.info,
        res.data.data
      )))
      .catch((err) => dispatch(
        editModuleTopicFailed(err.response.data.error)
      ));
  };
};

const editModuleTopicBegins = () => {
  return {
    type: EDIT_BEGINS
  };
};

const editModuleTopicFailed = (message) => {
  return {
    type   : EDIT_FAILED,
    payload: {message}
  };
};

const editModuleTopicSuccess = (message, topic) => {
  return {
    type   : EDIT_MODULE_TOPIC_SUCCESS,
    payload: {
      message,
      topic
    }
  };
};

export default editModuleTopic;
