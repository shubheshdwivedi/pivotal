import {FetchModuleTopicsRequest} from '../../../../../requests/courses/';
import {
  FETCH_BEGINS,
  FETCH_MODULE_TOPIC_SUCCESS,
  FETCH_FAILED
} from '../../../index';

const fetchModuleTopics = (courseId, moduleId, page, params) => {
  return (dispatch) => {
    dispatch(fetchModuleTopicsBegins());
    (new FetchModuleTopicsRequest(courseId, moduleId, page, params))
      .fire()
      .then((res) => dispatch(fetchModuleTopicsSuccess(
        courseId,
        moduleId,
        res.data.data,
        page,
        res.data.metaData
      )))
      .catch((err) => dispatch(
        fetchModuleTopicsFailed(
          err.response.data.error
        ))
      );
  };
};

const fetchModuleTopicsBegins = () => {
  return {
    type: FETCH_BEGINS
  };
};

const fetchModuleTopicsFailed = (message) => {
  return {
    type   : FETCH_FAILED,
    payload: {message}
  };
};

const fetchModuleTopicsSuccess = (courseId, moduleId, topics, page, meta) => {
  return {
    type   : FETCH_MODULE_TOPIC_SUCCESS,
    payload: {
      courseId,
      moduleId,
      topics,
      page,
      meta
    }
  };
};

export default fetchModuleTopics;
