import {
  CREATE_BEGINS,
  CREATE_FAILED,
  CREATE_MODULE_TOPIC_SUCCESS
} from '../../../index';
import {CreateModuleTopicRequest} from '../../../../../requests/courses/';


const createModuleTopic = (courseId, moduleId, data) => {
  return (dispatch) => {
    dispatch(createModuleTopicBegins());
    (new CreateModuleTopicRequest(courseId, moduleId, data))
      .fire()
      .then((res) => dispatch(createModuleTopicSuccess(
        res.data.info,
        courseId,
        moduleId,
        res.data.data
      )))
      .catch((err) => dispatch(
        createModuleTopicFailed(err.response.data.error)
      ));
  };
};

const createModuleTopicBegins = () => {
  return {
    type: CREATE_BEGINS
  };
};

const createModuleTopicFailed = (message) => {
  return {
    type   : CREATE_FAILED,
    payload: {message}
  };
};

const createModuleTopicSuccess = (message, courseId, moduleId, data) => {
  return {
    type   : CREATE_MODULE_TOPIC_SUCCESS,
    payload: {
      message,
      courseId,
      moduleId,
      data
    }
  };
};

export default createModuleTopic;
