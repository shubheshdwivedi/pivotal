import {
  DELETE_BEGINS,
  DELETE_MODULE_TOPIC_SUCCESS,
  DELETE_FAILED
} from '../../../index';
import {DeleteModuleTopicRequest} from '../../../../../requests/courses/';


const deleteModuleTopic = (courseId, moduleId, id) => {
  return (dispatch) => {
    dispatch(deleteModuleTopicBegins());
    (new DeleteModuleTopicRequest(courseId, moduleId, id))
      .fire()
      .then((res) => dispatch(deleteModuleTopicSuccess(
        res.data.info,
        courseId,
        moduleId,
        id
      )))
      .catch((err) => dispatch(deleteModuleTopicFailed(
        err.response.data.error
      )));
  };
};

const deleteModuleTopicBegins = () => {
  return {
    type: DELETE_BEGINS
  };
};

const deleteModuleTopicFailed = (message) => {
  return {
    type   : DELETE_FAILED,
    payload: {message}
  };
};

const deleteModuleTopicSuccess = (message, courseId, moduleId, id) => {
  return {
    type   : DELETE_MODULE_TOPIC_SUCCESS,
    payload: {
      message,
      courseId,
      moduleId,
      id
    }
  };
};

export default deleteModuleTopic;
