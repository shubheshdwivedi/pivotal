import {
  EDIT_BEGINS,
  EDIT_COURSE_MODULE_SUCCESS,
  EDIT_FAILED
} from '../../index';
import {EditCourseModuleRequest} from '../../../../requests/courses/';


const editCourseModule = (id, moduleId, data) => {
  return (dispatch) => {
    dispatch(editCourseModuleBegins());
    (new EditCourseModuleRequest(id, moduleId, data))
      .fire()
      .then((res) => dispatch(editCourseModuleSuccess(
        res.data.info,
        res.data.data
      )))
      .catch((err) => dispatch(
        editCourseModuleFailed(err.response.data.error)
      ));
  };
};

const editCourseModuleBegins = () => {
  return {
    type: EDIT_BEGINS
  };
};

const editCourseModuleFailed = (message) => {
  return {
    type   : EDIT_FAILED,
    payload: {message}
  };
};

const editCourseModuleSuccess = (message, data) => {
  return {
    type   : EDIT_COURSE_MODULE_SUCCESS,
    payload: {
      message,
      data
    }
  };
};

export default editCourseModule;
