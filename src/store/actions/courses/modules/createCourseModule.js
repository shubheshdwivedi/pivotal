import {
  CREATE_BEGINS,
  CREATE_COURSE_MODULES_SUCCESS,
  CREATE_FAILED
} from '../../index';
import {CreateCourseModuleRequest} from '../../../../requests/courses/';


const createCourseModule = (id, data) => {
  return (dispatch) => {
    dispatch(createCourseModuleBegins());
    (new CreateCourseModuleRequest(id, data))
      .fire()
      .then((res) => dispatch(createCourseModuleSuccess(
        res.data.info,
        id,
        res.data.data
      )))
      .catch((err) => dispatch(
        createCourseModuleFailed(err.response.data.error)
      ));
  };
};

const createCourseModuleBegins = () => {
  return {
    type: CREATE_BEGINS
  };
};

const createCourseModuleFailed = (message) => {
  return {
    type   : CREATE_FAILED,
    payload: {message}
  };
};

const createCourseModuleSuccess = (message, id, data) => {
  return {
    type   : CREATE_COURSE_MODULES_SUCCESS,
    payload: {
      message,
      id,
      data
    }
  };
};

export default createCourseModule;
