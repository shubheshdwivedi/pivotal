import {SET_MODULE_ID} from '../../index';

const setModuleId = (id) => {
  return {
    type   : SET_MODULE_ID,
    payload: {id}
  };
};

export default setModuleId;
