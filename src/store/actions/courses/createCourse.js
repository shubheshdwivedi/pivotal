import {
  CREATE_BEGINS,
  CREATE_COURSES_SUCCESS,
  CREATE_FAILED
} from '../index';
import {CreateCourseRequest} from '../../../requests/courses';

const createCourse = (data) => {
  return (dispatch) => {
    dispatch(createCourseBegins());
    (new CreateCourseRequest(data))
      .fire()
      .then((res) => dispatch(createCourseSuccess(res.data.data)))
      .catch((err) => dispatch(createCourseFailed(err.response.data.error)));
  };
};

const createCourseBegins = () => {
  return {
    type: CREATE_BEGINS
  };
};

const createCourseFailed = (message) => {
  return {
    type   : CREATE_FAILED,
    payload: {message}
  };
};

const createCourseSuccess = (course) => {
  return {
    type   : CREATE_COURSES_SUCCESS,
    payload: {
      course
    }
  };
};

export default createCourse;
