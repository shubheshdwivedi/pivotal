import {
  EDIT_BEGINS,
  EDIT_FAILED,
  EDIT_COURSES_SUCCESS
} from '../index';
import {EditCourseRequest} from '../../../requests/courses';

const editCourse = (data) => {
  return (dispatch) => {
    dispatch(editCourseBegins());
    (new EditCourseRequest(data))
      .fire()
      .then((res) => dispatch(editCourseSuccess(
        res.data.info,
        res.data.data
      )))
      .catch((err) => dispatch(editCourseFailed(err.response.data.error)));
  };
};

const editCourseBegins = () => {
  return {
    type: EDIT_BEGINS
  };
};

const editCourseFailed = (message) => {
  return {
    type   : EDIT_FAILED,
    payload: {message}
  };
};

const editCourseSuccess = (message, data) => {
  return {
    type   : EDIT_COURSES_SUCCESS,
    payload: {
      message,
      data
    }
  };
};

export default editCourse;
