import {SET_PAPER_CRAFTING} from '../index';

const setPaperCrafting = (crafting, paper = undefined) => {
  return {
    type   : SET_PAPER_CRAFTING,
    payload: {crafting, paper}
  };
};

export default setPaperCrafting;
