import {
  EDIT_BEGINS,
  EDIT_FAILED,
  EDIT_PAPER_SUCCESS
} from '../index';
import {EditPaperRequest} from '../../../requests/papers';

const editPaper = (data, id) => {
  return (dispatch) => {
    dispatch(editPaperBegins());
    (new EditPaperRequest(data, id))
      .fire()
      .then((res) => dispatch(editPaperSuccess(
        res.data.info,
        res.data.data
      )))
      .catch((err) => dispatch(editPaperFailed(err.response.data.error)));
  };
};

const editPaperBegins = () => {
  return {
    type: EDIT_BEGINS
  };
};

const editPaperFailed = (message) => {
  return {
    type   : EDIT_FAILED,
    payload: {message}
  };
};

const editPaperSuccess = (message, paper) => {
  return {
    type   : EDIT_PAPER_SUCCESS,
    payload: {
      message,
      paper
    }
  };
};

export default editPaper;
