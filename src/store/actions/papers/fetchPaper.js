import {FetchPapersRequest} from '../../../requests/papers/';
import {
  FETCH_BEGINS,
  FETCH_PAPERS_SUCCESS,
  FETCH_FAILED
} from '../index';

const fetchPapers = (page, params) => {
  return (dispatch) => {
    dispatch(fetchPapersBegins());
    (new FetchPapersRequest(page, params))
      .fire()
      .then((res) => dispatch(fetchPapersSuccess(
        res.data.data,
        page,
        res.data.metaData
      )))
      .catch((err) => dispatch(fetchPapersFailed(err.response.data.error)));
  };
};

const fetchPapersBegins = () => {
  return {
    type: FETCH_BEGINS
  };
};

const fetchPapersFailed = (message) => {
  return {
    type   : FETCH_FAILED,
    payload: {message}
  };
};

const fetchPapersSuccess = (papers, page, meta) => {
  return {
    type   : FETCH_PAPERS_SUCCESS,
    payload: {
      papers,
      page,
      meta
    }
  };
};

export default fetchPapers;
