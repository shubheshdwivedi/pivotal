import fetchPapers from './fetchPaper';
import createPaper from './createPaper';
import editPaper from './editPaper';
import deletePaper from './deletePaper';
import setPaperCrafting from './setPaperCrafting';
import setPaperSearchParams from './setPaperSearchParams';

export {
  fetchPapers,
  createPaper,
  deletePaper,
  editPaper,
  setPaperCrafting,
  setPaperSearchParams
};
