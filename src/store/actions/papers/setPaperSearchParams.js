import {SET_PAPER_SEARCH_PARAMS} from '../index';


const setPaperSearchParams = (params) => {
  return {
    type   : SET_PAPER_SEARCH_PARAMS,
    payload: {params}
  };
};

export default setPaperSearchParams;
