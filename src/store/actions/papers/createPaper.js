import {
  CREATE_BEGINS,
  CREATE_PAPER_SUCCESS,
  CREATE_FAILED
} from '../index';
import {CreatePaperRequest} from '../../../requests/papers';

const createPaper = (data) => {
  return (dispatch) => {
    dispatch(createPaperBegins());
    (new CreatePaperRequest(data))
      .fire()
      .then((res) => dispatch(createPaperSuccess(res.data.data)))
      .catch((err) => dispatch(createPaperFailed(err.response.data.error)));
  };
};

const createPaperBegins = () => {
  return {
    type: CREATE_BEGINS
  };
};

const createPaperFailed = (message) => {
  return {
    type   : CREATE_FAILED,
    payload: {message}
  };
};

const createPaperSuccess = (paper) => {
  return {
    type   : CREATE_PAPER_SUCCESS,
    payload: {
      paper
    }
  };
};

export default createPaper;
