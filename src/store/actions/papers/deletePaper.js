import {
  DELETE_BEGINS,
  DELETE_PAPER_SUCCESS,
  DELETE_FAILED
} from '../index';
import {DeletePaperRequest} from '../../../requests/papers';


const deletePaper = (id) => {
  return (dispatch) => {
    dispatch(deletePaperBegins());
    (new DeletePaperRequest(id))
      .fire()
      .then((res) => dispatch(deletePaperSuccess(res.data.info, id)))
      .catch((err) => dispatch(deletePaperFailed(err.response.data.error)));
  };
};

const deletePaperBegins = () => {
  return {
    type: DELETE_BEGINS
  };
};

const deletePaperFailed = (message) => {
  return {
    type   : DELETE_FAILED,
    payload: {message}
  };
};

const deletePaperSuccess = (message, id) => {
  return {
    type   : DELETE_PAPER_SUCCESS,
    payload: {
      message,
      id
    }
  };
};

export default deletePaper;
