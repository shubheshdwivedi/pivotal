import {LOGOUT} from '../index';

const logoutAction = () => {
  return {
    type: LOGOUT
  };
};

export default logoutAction;
