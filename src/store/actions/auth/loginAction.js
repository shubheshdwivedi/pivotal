import {LOGIN_RESET, LOGIN_SUCCESS, LOGIN_FAILED} from '../index';
import LoginRequest from '../../../requests/LoginRequest';
import api from '../../../api';

const loginAction = (data) => {
  return (dispatch) => {
    dispatch(loginReset());
    const request = new LoginRequest(data);
    request.fire()
      .then((res) => {
        api.setHeader('x-auth-token', res.data.data.token);
        dispatch(loginSuccess(res.data.data));
      })
      .catch((err) => dispatch(loginFailed(err.response.data.error)));
  };
};

const loginReset = () => ({
  type: LOGIN_RESET
});

const loginSuccess = (data) => ({
  type   : LOGIN_SUCCESS,
  payload: {
    token: data.token,
    user : data
  }
});

const loginFailed = (message) => ({
  type   : LOGIN_FAILED,
  payload: {message}
});

export default loginAction;
