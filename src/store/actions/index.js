export const LOGIN_RESET = 'loginReset';
export const LOGIN_SUCCESS = 'loginSuccess';
export const LOGIN_FAILED = 'loginFailed';

export const LOGOUT = 'logout';

export const FETCH_BEGINS = 'fetch_begins';
export const FETCH_FAILED = 'fetch_failed';

export const CREATE_BEGINS = 'create_begins';
export const CREATE_FAILED = 'create_failed';

export const DELETE_BEGINS = 'delete_begins';
export const DELETE_FAILED = 'delete_failed';

export const EDIT_BEGINS = 'edit_begins';
export const EDIT_FAILED = 'edit_failed';

export const FETCH_EMPLOYEES_SUCCESS = 'fetch_employees_success';
export const CREATE_EMPLOYEES_SUCCESS = 'create_employees_success';
export const DELETE_EMPLOYEES_SUCCESS = 'delete_employees_success';
export const EDIT_EMPLOYEES_SUCCESS = 'edit_employees_success';

export const SET_EMP_SEARCH_PARAMS = 'set_emp_search_params';

export const FETCH_STUDENTS_SUCCESS = 'fetch_students_success';
export const CREATE_STUDENTS_SUCCESS = 'create_students_success';
export const DELETE_STUDENTS_SUCCESS = 'delete_students_success';
export const EDIT_STUDENTS_SUCCESS = 'edit_students_success';

export const SET_STU_SEARCH_PARAMS = 'set_stu_search_params';

export const FETCH_MENTEES_SUCCESS = 'fetch_mentees_success';
export const DELETE_MENTEES_SUCCESS = 'delete_mentees_success';
export const EDIT_MENTEES_SUCCESS = 'edit_mentees_success';
export const CREATE_MENTEES_SUCCESS = 'create_mentees_success';

export const FETCH_GROUPS_SUCCESS = 'fetch_groups_success';
export const CREATE_GROUPS_SUCCESS = 'create_groups_success';
export const DELETE_GROUPS_SUCCESS = 'delete_groups_success';
export const EDIT_GROUPS_SUCCESS = 'edit_groups_success';

export const SET_GRO_SEARCH_PARAMS = 'set_gro_search_params';
export const SET_GROUP_ID = 'set_group_id';

export const FETCH_GROUPS_STUDENTS_SUCCESS = 'fetch_group_students_success';
export const CREATE_GROUPS_STUDENTS_SUCCESS = 'create_groups_students_success';
export const DELETE_GROUPS_STUDENTS_SUCCESS = 'delete_groups_students_success';

export const FETCH_COURSES_SUCCESS = 'fetch_courses_success';
export const DELETE_COURSES_SUCCESS = 'delete_courses_success';
export const EDIT_COURSES_SUCCESS = 'edit_courses_success';
export const CREATE_COURSES_SUCCESS = 'create_courses_success';

export const SET_COURSE_SEARCH_PARAMS = 'set_course_search_params';
export const SET_COURSE_ID = 'set_course_id';
export const SET_MODULE_ID = 'set_module_id';

export const FETCH_COURSE_MODULES_SUCCESS = 'fetch_course_modules_success';
export const DELETE_COURSE_MODULES_SUCCESS = 'delete_course_modules_success';
export const CREATE_COURSE_MODULES_SUCCESS = 'create_course_modules_success';
export const EDIT_COURSE_MODULE_SUCCESS = 'edit_course_module_success';

export const FETCH_MODULE_TOPIC_SUCCESS = 'fetch_module_topic_success';
export const DELETE_MODULE_TOPIC_SUCCESS = 'delete_module_topic_success';
export const CREATE_MODULE_TOPIC_SUCCESS = 'create_module_topic_success';
export const EDIT_MODULE_TOPIC_SUCCESS = 'edit_module_topic_success';

export const FETCH_PAPERS_SUCCESS = 'fetch_papers_success';
export const DELETE_PAPER_SUCCESS = 'delete_paper_success';
export const CREATE_PAPER_SUCCESS = 'create_paper_success';
export const EDIT_PAPER_SUCCESS = 'edit_paper_success';

export const SET_PAPER_CRAFTING = 'set_paper_crafting';
export const SET_PAPER_SEARCH_PARAMS = 'set_paper_search_params';
