import fetchMentees from './fetchMentees';
import editMentee from './editMentees';
import deleteMentee from './deleteMentees';
import createMentee from './createMentee';

export {
  fetchMentees,
  editMentee,
  deleteMentee,
  createMentee
};
