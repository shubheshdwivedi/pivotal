import {FetchMenteesRequest} from '../../../requests/mentees/';
import {
  FETCH_BEGINS,
  FETCH_MENTEES_SUCCESS,
  FETCH_FAILED
} from '../index';

const fetchMentees = (page, params) => {
  return (dispatch) => {
    dispatch(fetchMenteesBegins());
    (new FetchMenteesRequest(page, params))
      .fire()
      .then((res) => dispatch(fetchMenteesSuccess(
        res.data.data,
        page,
        res.data.metaData
      )))
      .catch((err) => dispatch(fetchMenteesFailed(err.response.data.error)));
  };
};

const fetchMenteesBegins = () => {
  return {
    type: FETCH_BEGINS
  };
};

const fetchMenteesFailed = (message) => {
  return {
    type   : FETCH_FAILED,
    payload: {message}
  };
};

const fetchMenteesSuccess = (mentees, page, meta) => {
  return {
    type   : FETCH_MENTEES_SUCCESS,
    payload: {
      mentees,
      page,
      meta
    }
  };
};

export default fetchMentees;
