import {
  DELETE_BEGINS,
  DELETE_MENTEES_SUCCESS,
  DELETE_FAILED
} from '../index';
import {DeleteMenteeRequest} from '../../../requests/mentees';


const deleteMentee = (id) => {
  return (dispatch) => {
    dispatch(deleteMenteeBegins());
    (new DeleteMenteeRequest(id))
      .fire()
      .then((res) => dispatch(deleteMenteeSuccess(res.data.info, id)))
      .catch((err) => dispatch(deleteMenteeFailed(err.response.data.error)));
  };
};

const deleteMenteeBegins = () => {
  return {
    type: DELETE_BEGINS
  };
};

const deleteMenteeFailed = (message) => {
  return {
    type   : DELETE_FAILED,
    payload: {message}
  };
};

const deleteMenteeSuccess = (message, id) => {
  return {
    type   : DELETE_MENTEES_SUCCESS,
    payload: {
      message,
      id
    }
  };
};

export default deleteMentee;
