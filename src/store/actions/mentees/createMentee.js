import {
  CREATE_BEGINS,
  CREATE_FAILED,
  CREATE_MENTEES_SUCCESS
} from '../index';
import {MenteeRequest} from '../../../requests/mentees';


const createMentee = (data) => {
  return (dispatch) => {
    dispatch(createMenteesBegins());
    (new MenteeRequest(data))
      .fire()
      .then((res) => dispatch(createMenteesSuccess(
        res.data.info,
        res.data.data
      )))
      .catch((err) => dispatch(createMenteesFailed(err.response.data.error)));
  };
};

const createMenteesBegins = () => {
  return {
    type: CREATE_BEGINS
  };
};

const createMenteesFailed = (message) => {
  return {
    type   : CREATE_FAILED,
    payload: {message}
  };
};

const createMenteesSuccess = (message, data) => {
  return {
    type   : CREATE_MENTEES_SUCCESS,
    payload: {
      message,
      data
    }
  };
};

export default createMentee;
