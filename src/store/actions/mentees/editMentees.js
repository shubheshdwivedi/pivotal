import {
  EDIT_BEGINS,
  EDIT_FAILED,
  EDIT_MENTEES_SUCCESS
} from '../index';
import {MenteeRequest} from '../../../requests/mentees';


const editMentee = (data) => {
  return (dispatch) => {
    dispatch(editMenteesBegins());
    (new MenteeRequest(data))
      .fire()
      .then((res) => dispatch(editMenteesSuccess(
        res.data.info,
        res.data.data
      )))
      .catch((err) => dispatch(editMenteesFailed(err.response.data.error)));
  };
};

const editMenteesBegins = () => {
  return {
    type: EDIT_BEGINS
  };
};

const editMenteesFailed = (message) => {
  return {
    type   : EDIT_FAILED,
    payload: {message}
  };
};

const editMenteesSuccess = (message, data) => {
  return {
    type   : EDIT_MENTEES_SUCCESS,
    payload: {
      message,
      data
    }
  };
};

export default editMentee;
