import {SET_STU_SEARCH_PARAMS} from '../index';


const setStuSearchParams = (params) => {
  return {
    type   : SET_STU_SEARCH_PARAMS,
    payload: {params}
  };
};

export default setStuSearchParams;
