import {
  DELETE_BEGINS,
  DELETE_STUDENTS_SUCCESS,
  DELETE_FAILED
} from '../index';
import {DeleteStudentRequest} from '../../../requests/students';


const deleteStudent = (id) => {
  return (dispatch) => {
    dispatch(deleteStudentBegins());
    (new DeleteStudentRequest(id))
      .fire()
      .then((res) => dispatch(deleteStudentSuccess(res.data.info, id)))
      .catch((err) => dispatch(deleteStudentFailed(err.response.data.error)));
  };
};

const deleteStudentBegins = () => {
  return {
    type: DELETE_BEGINS
  };
};

const deleteStudentFailed = (message) => {
  return {
    type   : DELETE_FAILED,
    payload: {message}
  };
};

const deleteStudentSuccess = (message, id) => {
  return {
    type   : DELETE_STUDENTS_SUCCESS,
    payload: {
      message,
      id
    }
  };
};

export default deleteStudent;
