import fetchStudents from './fetchStudent';
import deleteStudent from './deleteStudent';
import editStudent from './editStudent';
import createStudent from './createStudent';
import setStuSearchParams from './setStuSearchParams';

export {
  fetchStudents,
  deleteStudent,
  editStudent,
  createStudent,
  setStuSearchParams
};
