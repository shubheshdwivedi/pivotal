import {
  CREATE_BEGINS,
  CREATE_STUDENTS_SUCCESS,
  CREATE_FAILED
} from '../index';
import {CreateStudentRequest} from '../../../requests/students/';

const createStudent = (data) => {
  return (dispatch) => {
    dispatch(createStudentBegins());
    (new CreateStudentRequest(data))
      .fire()
      .then((res) => dispatch(createStudentSuccess(
        res.data.data
      )))
      .catch((err) => dispatch(createStudentFailed(err.response.data.error)));
  };
};

const createStudentBegins = () => {
  return {
    type: CREATE_BEGINS
  };
};

const createStudentFailed = (message) => {
  return {
    type   : CREATE_FAILED,
    payload: {message}
  };
};

const createStudentSuccess = (student) => {
  return {
    type   : CREATE_STUDENTS_SUCCESS,
    payload: {
      student
    }
  };
};

export default createStudent;
