import {
  FETCH_BEGINS,
  FETCH_STUDENTS_SUCCESS,
  FETCH_FAILED
} from '../index';
import {FetchStudentRequest} from '../../../requests/students';

const fetchStudents = (page, params) => {
  return (dispatch) => {
    dispatch(fetchStudentBegins());
    (new FetchStudentRequest(page, params))
      .fire()
      .then((res) => dispatch(fetchStudentSuccess(
        res.data.data,
        page,
        res.data.metaData
      )))
      .catch((err) => fetchStudentFailed(err.response.data.error));
  };
};

const fetchStudentBegins = () => {
  return {
    type: FETCH_BEGINS
  };
};

const fetchStudentFailed = (message) => {
  return {
    type   : FETCH_FAILED,
    payload: {message}
  };
};

const fetchStudentSuccess = (students, page, meta) => {
  return {
    type   : FETCH_STUDENTS_SUCCESS,
    payload: {
      students,
      page,
      meta
    }
  };
};

export default fetchStudents;
