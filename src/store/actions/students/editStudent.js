import {
  EDIT_BEGINS,
  EDIT_FAILED,
  EDIT_STUDENTS_SUCCESS
} from '../index';
import {EditStudentRequest} from '../../../requests/students';


const editStudent = (data) => {
  return (dispatch) => {
    dispatch(editStudentBegins());
    (new EditStudentRequest(data))
      .fire()
      .then((res) => dispatch(editStudentSuccess(
        res.data.info,
        res.data.data
      )))
      .catch((err) => dispatch(editStudentFailed(err.response.data.error)));
  };
};

const editStudentBegins = () => {
  return {
    type: EDIT_BEGINS
  };
};

const editStudentFailed = (message) => {
  return {
    type   : EDIT_FAILED,
    payload: {message}
  };
};

const editStudentSuccess = (message, data) => {
  return {
    type   : EDIT_STUDENTS_SUCCESS,
    payload: {
      message,
      data
    }
  };
};

export default editStudent;
