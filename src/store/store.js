import {createStore, applyMiddleware, compose} from 'redux';
import Reducers from './reducers';
import {persistStore, persistReducer} from 'redux-persist';
import ReduxThunk from 'redux-thunk';
import storage from 'redux-persist/lib/storage';

const persistConfig = {
  key: 'root',
  storage
};

const persistedReducer = persistReducer(persistConfig, Reducers);
const store = createStore(
  persistedReducer,
  {},
  compose(applyMiddleware(ReduxThunk),
    window.__REDUX_DEVTOOLS_EXTENSION__ &&
    window.__REDUX_DEVTOOLS_EXTENSION__())
);
const persistor = persistStore(store);

export {store, persistor};
