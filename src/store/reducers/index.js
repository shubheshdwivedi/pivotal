import {combineReducers} from 'redux';
import DataReducers from './data-reducers';
import UiReducers from './ui-reducers';

const Reducers = combineReducers({DataReducers, UiReducers});

export default Reducers;
