import {
  LOGIN_RESET,
  LOGIN_SUCCESS,
  LOGIN_FAILED,
  LOGOUT
} from '../../../actions';

const initState = {
  user    : null,
  token   : null,
  errors  : null,
  userType: null
};

const AuthReducer = (state = initState, action) => {
  switch (action.type) {
    case LOGIN_RESET:
      state = initState;
      break;

    case LOGIN_FAILED:
      state = {
        ...state,
        errors: action.payload.message
      };
      break;

    case LOGIN_SUCCESS:
      localStorage.setItem('token', action.payload.token);
      state = {
        ...state,
        errors  : null,
        token   : action.payload.token,
        user    : action.payload.user,
        userType: action.payload.user.userType
      };
      break;

    case LOGOUT:
      localStorage.clear();
      state = initState;
      break;

    default:
      break;
  }
  return state;
};

export default AuthReducer;
