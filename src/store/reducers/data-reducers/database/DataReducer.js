import {
  CREATE_BEGINS, CREATE_COURSE_MODULES_SUCCESS, CREATE_COURSES_SUCCESS,
  CREATE_EMPLOYEES_SUCCESS,
  CREATE_FAILED,
  CREATE_GROUPS_STUDENTS_SUCCESS,
  CREATE_GROUPS_SUCCESS,
  CREATE_MENTEES_SUCCESS, CREATE_MODULE_TOPIC_SUCCESS, CREATE_PAPER_SUCCESS,
  CREATE_STUDENTS_SUCCESS,
  DELETE_BEGINS, DELETE_COURSE_MODULES_SUCCESS, DELETE_COURSES_SUCCESS,
  DELETE_EMPLOYEES_SUCCESS,
  DELETE_FAILED,
  DELETE_GROUPS_STUDENTS_SUCCESS,
  DELETE_GROUPS_SUCCESS,
  DELETE_MENTEES_SUCCESS, DELETE_MODULE_TOPIC_SUCCESS, DELETE_PAPER_SUCCESS,
  DELETE_STUDENTS_SUCCESS,
  EDIT_BEGINS, EDIT_COURSE_MODULE_SUCCESS, EDIT_COURSES_SUCCESS,
  EDIT_EMPLOYEES_SUCCESS,
  EDIT_FAILED,
  EDIT_GROUPS_SUCCESS,
  EDIT_MENTEES_SUCCESS, EDIT_MODULE_TOPIC_SUCCESS, EDIT_PAPER_SUCCESS,
  EDIT_STUDENTS_SUCCESS,
  FETCH_BEGINS, FETCH_COURSE_MODULES_SUCCESS, FETCH_COURSES_SUCCESS,
  FETCH_EMPLOYEES_SUCCESS,
  FETCH_FAILED,
  FETCH_GROUPS_STUDENTS_SUCCESS,
  FETCH_GROUPS_SUCCESS,
  FETCH_MENTEES_SUCCESS, FETCH_MODULE_TOPIC_SUCCESS, FETCH_PAPERS_SUCCESS,
  FETCH_STUDENTS_SUCCESS,
  LOGIN_RESET
} from '../../../actions';
import Employee from '../../../../models/Employee';
import Student from '../../../../models/Student';
import Group from '../../../../models/Group';
import Course from '../../../../models/Course';
import Module from '../../../../models/Module';
import Topic from '../../../../models/Topic';
import Paper from '../../../../models/Paper';

const initState = {
  employees    : {},
  students     : {},
  mentees      : {},
  groups       : {},
  groupStudents: {},
  courses      : {},
  courseModules: {},
  moduleTopics : {},
  papers       : {},
  fetching     : false,
  deleting     : false,
  editing      : false,
  creating     : false,
  error        : null
};

const DataReducer = (state = initState, action) => {
  switch (action.type) {
    case LOGIN_RESET: {
      state = initState;
      return state;
    }

    case FETCH_BEGINS: {
      state = {
        ...state,
        fetching: true,
        error   : null
      };
      return state;
    }

    case FETCH_FAILED: {
      state = {
        ...state,
        fetching: false,
        error   : action.payload.message
      };
      return state;
    }

    case CREATE_BEGINS: {
      state = {
        ...state,
        creating: true,
        error   : null
      };

      return state;
    }

    case CREATE_FAILED: {
      state = {
        ...state,
        creating: false,
        error   : action.payload.message
      };

      return state;
    }

    case DELETE_BEGINS: {
      state = {
        ...state,
        deleting: true,
        error   : null
      };
      return state;
    }

    case DELETE_FAILED: {
      state = {
        ...state,
        error   : action.payload.message,
        deleting: false
      };
      return state;
    }

    case EDIT_BEGINS: {
      state = {
        ...state,
        editing: true,
        error  : null
      };
      return state;
    }

    case EDIT_FAILED: {
      state = {
        ...state,
        editing: null,
        error  : action.payload.message
      };

      return state;
    }

    case FETCH_EMPLOYEES_SUCCESS: {
      const fetchedEmployees = action.payload.employees;
      const employees = {};

      fetchedEmployees.forEach((emp) => {
        employees[emp.employeeId] = new Employee(emp);
      });

      state = {
        ...state,
        error    : null,
        fetching : false,
        employees: employees
      };
      return state;
    }

    case CREATE_EMPLOYEES_SUCCESS: {
      const emp = action.payload.employee;
      const id = emp.employeeId;

      state = {
        ...state,
        error    : null,
        creating : false,
        employees: Object.assign({}, state.employees, {[id]: new Employee(emp)})
      };

      return state;
    }

    case EDIT_EMPLOYEES_SUCCESS: {
      const data = action.payload.data;
      const id = data.employeeId;

      state = {
        ...state,
        editing  : false,
        error    : null,
        employees: Object.assign(
          {},
          state.employees,
          {[id]: new Employee(data)}
        )
      };
      return state;
    }

    case DELETE_EMPLOYEES_SUCCESS: {
      const id = action.payload.id;

      state = {
        ...state,
        error    : null,
        deleting : false,
        employees: Object.assign({}, state.employees, {[id]: undefined})
      };
      return state;
    }

    case FETCH_STUDENTS_SUCCESS: {
      const fetchedStudents = action.payload.students;
      const students = {};

      fetchedStudents.forEach((stu) => {
        students[stu.studentId] = new Student(stu);
      });

      state = {
        ...state,
        error   : null,
        fetching: false,
        students: students
      };
      return state;
    }

    case CREATE_STUDENTS_SUCCESS: {
      const stu = action.payload.student;
      const id = stu.studentId;

      state = {
        ...state,
        error   : null,
        creating: false,
        students: Object.assign({}, state.students, {[id]: new Student(stu)})
      };

      return state;
    }

    case EDIT_STUDENTS_SUCCESS: {
      const data = action.payload.data;
      const id = data.studentId;

      state = {
        ...state,
        editing : false,
        error   : null,
        students: Object.assign(
          {},
          state.students,
          {[id]: new Student(data)}
        )
      };
      return state;
    }

    case DELETE_STUDENTS_SUCCESS: {
      const id = action.payload.id;

      state = {
        ...state,
        error   : null,
        deleting: false,
        students: Object.assign({}, state.students, {[id]: undefined})
      };
      return state;
    }

    case FETCH_MENTEES_SUCCESS: {
      const fetchedMentees = action.payload.mentees;
      const mentees = {};

      fetchedMentees.forEach((men) => {
        mentees[men.studentId] = new Student(men);
      });

      state = {
        ...state,
        fetching: false,
        mentees : mentees,
        error   : null
      };

      return state;
    }

    case CREATE_MENTEES_SUCCESS: {
      state = {
        ...state,
        error   : null,
        creating: false
      };

      return state;
    }

    case EDIT_MENTEES_SUCCESS: {
      const {data} = action.payload;
      const id = data.studentId;
      const mentee = new Student(data);

      state = {
        ...state,
        editing: false,
        error  : null,
        mentees: Object.assign(
          {},
          state.mentees,
          {[id]: mentee}
        )
      };

      return state;
    }

    case DELETE_MENTEES_SUCCESS: {
      const id = action.payload.id;

      state = {
        ...state,
        error   : null,
        deleting: false,
        mentees : Object.assign({}, state.mentees, {[id]: undefined})
      };

      return state;
    }

    case FETCH_GROUPS_SUCCESS: {
      const fetchedGroups = action.payload.groups;
      const groups = {};

      fetchedGroups.forEach((gro) => {
        groups[gro.groupId] = new Group(gro);
      });

      state = {
        ...state,
        error   : null,
        fetching: false,
        groups  : groups
      };
      return state;
    }

    case CREATE_GROUPS_SUCCESS: {
      const gro = action.payload.group;
      const id = gro.groupId;

      state = {
        ...state,
        error   : null,
        creating: false,
        groups  : Object.assign({}, state.groups, {[id]: new Group(gro)})
      };

      return state;
    }

    case EDIT_GROUPS_SUCCESS: {
      const data = action.payload.data;
      const id = data.groupId;

      state = {
        ...state,
        editing: false,
        error  : null,
        groups : Object.assign(
          {},
          state.groups,
          {[id]: new Group(data)}
        )
      };
      return state;
    }

    case DELETE_GROUPS_SUCCESS: {
      const id = action.payload.id;

      state = {
        ...state,
        error   : null,
        deleting: false,
        groups  : Object.assign({}, state.groups, {[id]: undefined})
      };
      return state;
    }

    case FETCH_GROUPS_STUDENTS_SUCCESS: {
      const id = action.payload.id;
      const fetchedStudents = action.payload.students;
      const students = {};

      fetchedStudents.forEach((stu) => {
        students[stu.studentId] = new Student(stu);
      });

      state = {
        ...state,
        error        : null,
        fetching     : false,
        groupStudents: {
          ...state.groupStudents,
          [id]: students
        }
      };
      return state;
    }

    case CREATE_GROUPS_STUDENTS_SUCCESS: {
      state = {
        ...state,
        error   : null,
        creating: false
      };

      return state;
    }

    case DELETE_GROUPS_STUDENTS_SUCCESS: {
      const {id, stuId} = action.payload;

      state = {
        ...state,
        error        : null,
        deleting     : false,
        groupStudents: {
          ...state.groupStudents,
          [id]: Object.assign({}, state.groupStudents[id], {[stuId]: undefined})
        }
      };
      return state;
    }

    case FETCH_COURSES_SUCCESS: {
      const fetchedCourses = action.payload.courses;
      const courses = {};

      fetchedCourses.forEach((course) => {
        courses[course.courseId] = new Course(course);
      });

      state = {
        ...state,
        error   : null,
        fetching: false,
        courses : courses
      };
      return state;
    }

    case CREATE_COURSES_SUCCESS: {
      const course = action.payload.course;
      const id = course.courseId;

      state = {
        ...state,
        error   : null,
        creating: false,
        courses : Object.assign({}, state.courses, {[id]: new Course(course)})
      };

      return state;
    }

    case EDIT_COURSES_SUCCESS: {
      const data = action.payload.data;
      const id = data.courseId;

      state = {
        ...state,
        editing: false,
        error  : null,
        courses: Object.assign(
          {},
          state.courses,
          {[id]: new Course(data)}
        )
      };
      return state;
    }

    case DELETE_COURSES_SUCCESS: {
      const id = action.payload.id;

      state = {
        ...state,
        error   : null,
        deleting: false,
        courses : Object.assign({}, state.courses, {[id]: undefined})
      };
      return state;
    }

    case FETCH_COURSE_MODULES_SUCCESS: {
      const id = action.payload.id;
      const fetchedModules = action.payload.modules;
      const modules = {};

      fetchedModules.forEach((module) => {
        modules[module.moduleId] = new Module(module);
      });

      state = {
        ...state,
        error        : null,
        fetching     : false,
        courseModules: {
          ...state.courseModules,
          [id]: modules
        }
      };
      return state;
    }

    case CREATE_COURSE_MODULES_SUCCESS: {
      state = {
        ...state,
        error   : null,
        creating: false
      };

      return state;
    }

    case DELETE_COURSE_MODULES_SUCCESS: {
      const {id, moduleId} = action.payload;

      state = {
        ...state,
        error        : null,
        deleting     : false,
        courseModules: {
          ...state.courseModules,
          [id]: Object.assign({},
            state.courseModules[id],
            {[moduleId]: undefined}
          )
        }
      };
      return state;
    }

    case EDIT_COURSE_MODULE_SUCCESS: {
      const data = action.payload.data;
      const module = new Module(data);

      state = {
        ...state,
        editing      : false,
        error        : null,
        courseModules: Object.assign(
          {},
          state.courseModules,
          Object.assign({},
            state.courseModules[module.courseId],
            {[module.id]: module}
          )
        )
      };
      return state;
    }

    case FETCH_MODULE_TOPIC_SUCCESS: {
      const moduleId = action.payload.moduleId;
      const fetchedTopics = action.payload.topics;
      const topics = {};

      fetchedTopics.forEach((topic) => {
        topics[topic.topicId] = new Topic(topic);
      });

      state = {
        ...state,
        error       : null,
        fetching    : false,
        moduleTopics: {
          ...state.moduleTopics,
          [moduleId]: topics
        }
      };
      return state;
    }

    case CREATE_MODULE_TOPIC_SUCCESS: {
      state = {
        ...state,
        error   : null,
        creating: false
      };

      return state;
    }

    case DELETE_MODULE_TOPIC_SUCCESS: {
      const {id, moduleId} = action.payload;

      state = {
        ...state,
        error       : null,
        deleting    : false,
        moduleTopics: {
          ...state.moduleTopics,
          [id]: Object.assign({},
            state.moduleTopics[moduleId],
            {[id]: undefined}
          )
        }
      };
      return state;
    }

    case EDIT_MODULE_TOPIC_SUCCESS: {
      const topic = new Topic(action.payload.topic);

      state = {
        ...state,
        editing     : false,
        error       : null,
        moduleTopics: Object.assign(
          {},
          state.moduleTopics,
          Object.assign({},
            state.moduleTopics[topic.moduleId],
            {[topic.id]: topic}
          )
        )
      };
      return state;
    }

    case FETCH_PAPERS_SUCCESS: {
      const fetchedPapers = action.payload.papers;
      const papers = {};

      fetchedPapers.forEach((paper) => {
        papers[paper.paperId] = new Paper(paper);
      });

      state = {
        ...state,
        error   : null,
        fetching: false,
        papers  : papers
      };
      return state;
    }

    case CREATE_PAPER_SUCCESS: {
      const paper = action.payload.course;
      const id = paper.paperId;

      state = {
        ...state,
        error   : null,
        creating: false,
        papers  : Object.assign({}, state.papers, {[id]: new Paper(paper)})
      };

      return state;
    }

    case EDIT_PAPER_SUCCESS: {
      const paper = action.payload.paper;
      const id = paper.paperId;

      state = {
        ...state,
        editing: false,
        error  : null,
        papers : Object.assign(
          {},
          state.papers,
          {[id]: new Paper(paper)}
        )
      };
      return state;
    }

    case DELETE_PAPER_SUCCESS: {
      const id = action.payload.id;

      state = {
        ...state,
        error   : null,
        deleting: false,
        papers  : Object.assign({}, state.papers, {[id]: undefined})
      };
      return state;
    }

    default:
      return state;
  }
};

export default DataReducer;
