import {combineReducers} from 'redux';
import AuthReducer from './auth';
import DataReducer from './database';

const DataReducers = combineReducers({
  AuthReducer,
  DataReducer
});
export default DataReducers;
