import {
  LOGIN_RESET,
  FETCH_PAPERS_SUCCESS,
  SET_PAPER_SEARCH_PARAMS,
  DELETE_PAPER_SUCCESS,
  SET_PAPER_CRAFTING
} from '../../../actions';
import {paginatorLength} from '../../../../_helpers/exportConsts';


const initState = {
  searchParams     : null,
  error            : null,
  ids              : {},
  currentPage      : 1,
  totalPages       : 0,
  craftingMode     : false,
  craftingModePaper: null
};

const PaperReducer = (state = initState, action) => {
  switch (action.type) {
    case LOGIN_RESET:
      state = initState;
      return state;

    case FETCH_PAPERS_SUCCESS: {
      const {papers, page, meta} = action.payload;
      const ids = [];
      const newIds = state.ids;

      papers.forEach((paper) => {
        ids.push(paper.paperId);
      });
      newIds[page] = ids;

      state = {
        ...state,
        ids        : newIds,
        currentPage: page,
        totalPages : Math.ceil(meta.total / paginatorLength)
      };
      return state;
    }

    case DELETE_PAPER_SUCCESS: {
      const id = action.payload.id;
      state = {
        ...state,
        ids: {
          ...state.ids,
          [state.currentPage]: state.ids[state.currentPage].filter(
            (i) => i !== id
          )
        }
      };
      return state;
    }

    case SET_PAPER_CRAFTING: {
      const {crafting, paper} = action.payload;

      state = {
        ...state,
        craftingMode     : crafting,
        craftingModePaper: paper
      };

      return state;
    }

    case SET_PAPER_SEARCH_PARAMS: {
      state = {
        ...state,
        searchParams: action.payload.params,
        currentPage : 1
      };
      return state;
    }

    default:
      return state;
  }
};

export default PaperReducer;
