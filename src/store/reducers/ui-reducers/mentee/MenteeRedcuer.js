import {
  LOGIN_RESET,
  FETCH_MENTEES_SUCCESS,
  DELETE_MENTEES_SUCCESS
} from '../../../actions';
import {paginatorLength} from '../../../../_helpers/exportConsts';

const initState = {
  searchParams: null,
  error       : null,
  ids         : {},
  currentPage : 1,
  totalPages  : 0
};

const MenteeReducer = (state = initState, action) => {
  switch (action.type) {
    case LOGIN_RESET:
      state = initState;
      return state;

    case FETCH_MENTEES_SUCCESS: {
      const {mentees, page, meta} = action.payload;
      const ids = [];
      const newIds = state.ids;

      mentees.forEach((mentee) => {
        ids.push(mentee.studentId);
      });
      newIds[page] = ids;

      state = {
        ...state,
        ids        : newIds,
        currentPage: page,
        totalPages : Math.ceil(meta.total / paginatorLength)
      };
      return state;
    }

    case DELETE_MENTEES_SUCCESS: {
      const id = action.payload.id;
      state = {
        ...state,
        ids: {
          ...state.ids,
          [state.currentPage]: state.ids[state.currentPage].filter(
            (i) => i !== id
          )
        }
      };

      return state;
    }

    default:
      return state;
  }
};

export default MenteeReducer;
