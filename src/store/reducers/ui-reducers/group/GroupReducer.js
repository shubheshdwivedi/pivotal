import {
  FETCH_GROUPS_SUCCESS,
  DELETE_GROUPS_SUCCESS,
  SET_GRO_SEARCH_PARAMS,
  LOGIN_RESET,
  FETCH_GROUPS_STUDENTS_SUCCESS,
  SET_GROUP_ID,
  DELETE_GROUPS_STUDENTS_SUCCESS
} from '../../../actions';
import {paginatorLength} from '../../../../_helpers/exportConsts';

const initState = {
  searchParams     : null,
  error            : null,
  ids              : {},
  currentPage      : 1,
  grpStuIds        : {},
  grpStuCurrentPage: 1,
  grpId            : null,
  totalPages       : 0
};

const GroupReducer = (state = initState, action) => {
  switch (action.type) {
    case LOGIN_RESET:
      state = initState;
      return state;

    case FETCH_GROUPS_SUCCESS: {
      const {groups, page, meta} = action.payload;
      const ids = [];
      const newIds = state.ids;

      groups.forEach((group) => {
        ids.push(group.groupId);
      });
      newIds[page] = ids;

      state = {
        ...state,
        ids        : newIds,
        currentPage: page,
        totalPages : Math.ceil(meta.total / paginatorLength)
      };
      return state;
    }

    case DELETE_GROUPS_SUCCESS: {
      const id = action.payload.id;
      state = {
        ...state,
        ids: {
          ...state.ids,
          [state.currentPage]: state.ids[state.currentPage].filter(
            (i) => i !== id
          )
        }
      };
      return state;
    }

    case SET_GRO_SEARCH_PARAMS: {
      state = {
        ...state,
        searchParams: action.payload.params,
        currentPage : 1
      };
      return state;
    }

    case FETCH_GROUPS_STUDENTS_SUCCESS: {
      const {id, students, page, meta} = action.payload;
      const ids = [];
      const newIds = state.grpStuIds;

      students.forEach((student) => {
        ids.push(student.studentId);
      });
      newIds[page] = ids;

      state = {
        ...state,
        grpStuIds        : newIds,
        grpId            : id,
        grpStuCurrentPage: page,
        totalPages       : Math.ceil(meta.total / paginatorLength)
      };
      return state;
    }

    case SET_GROUP_ID: {
      const id = action.payload.id;
      state = {
        ...state,
        grpId            : id,
        grpStuCurrentPage: 1
      };
      return state;
    }

    case DELETE_GROUPS_STUDENTS_SUCCESS: {
      const {stuId} = action.payload;

      state = {
        ...state,
        grpStuIds: {
          ...state.grpStuIds,
          [state.grpStuCurrentPage]: state.grpStuIds[state.grpStuCurrentPage]
            .filter((i) => i !== stuId)
        }
      };
      return state;
    }

    default:
      return state;
  }
};

export default GroupReducer;
