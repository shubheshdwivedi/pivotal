import {
  SET_COURSE_SEARCH_PARAMS,
  LOGIN_RESET,
  SET_COURSE_ID,
  SET_MODULE_ID,
  FETCH_COURSES_SUCCESS,
  DELETE_COURSES_SUCCESS,
  FETCH_COURSE_MODULES_SUCCESS,
  DELETE_COURSE_MODULES_SUCCESS,
  FETCH_MODULE_TOPIC_SUCCESS,
  DELETE_MODULE_TOPIC_SUCCESS
} from '../../../actions';
import {paginatorLength} from '../../../../_helpers/exportConsts';

const initState = {
  searchParams         : null,
  error                : null,
  ids                  : {},
  currentPage          : 1,
  courseModIds         : {},
  courseModCurrentPage : 1,
  modulesTopIds        : {},
  modulesTopCurrentPage: 1,
  courseId             : null,
  moduleId             : null,
  totalPages           : 0
};

const CourseReducer = (state = initState, action) => {
  switch (action.type) {
    case LOGIN_RESET:
      state = initState;
      return state;

    case FETCH_COURSES_SUCCESS: {
      const {courses, page, meta} = action.payload;
      const ids = [];
      const newIds = state.ids;

      courses.forEach((course) => {
        ids.push(course.courseId);
      });
      newIds[page] = ids;

      state = {
        ...state,
        ids        : newIds,
        currentPage: page,
        totalPages : Math.ceil(meta.total / paginatorLength)
      };
      return state;
    }

    case DELETE_COURSES_SUCCESS: {
      const id = action.payload.id;
      state = {
        ...state,
        ids: {
          ...state.ids,
          [state.currentPage]: state.ids[state.currentPage].filter(
            (i) => i !== id
          )
        }
      };
      return state;
    }

    case SET_COURSE_SEARCH_PARAMS: {
      state = {
        ...state,
        searchParams: action.payload.params,
        currentPage : 1
      };
      return state;
    }

    case SET_COURSE_ID: {
      const id = action.payload.id;
      state = {
        ...state,
        courseId            : id,
        courseModCurrentPage: 1
      };
      return state;
    }

    case FETCH_COURSE_MODULES_SUCCESS: {
      const {id, modules, page, meta} = action.payload;
      const ids = [];
      const newIds = state.courseModIds;

      modules.forEach((module) => {
        ids.push(module.moduleId);
      });
      newIds[page] = ids;

      state = {
        ...state,
        courseModIds        : newIds,
        courseId            : id,
        courseModCurrentPage: page,
        totalPages          : Math.ceil(meta.total / paginatorLength)
      };
      return state;
    }

    case DELETE_COURSE_MODULES_SUCCESS: {
      const {moduleId} = action.payload;

      state = {
        ...state,
        courseModIds: {
          ...state.courseModIds,
          [state.courseModCurrentPage]: state
            .courseModIds[state.courseModCurrentPage]
            .filter((i) => i !== moduleId)
        }
      };
      return state;
    }

    case SET_MODULE_ID: {
      const id = action.payload.id;
      state = {
        ...state,
        moduleId             : id,
        modulesTopCurrentPage: 1
      };
      return state;
    }

    case FETCH_MODULE_TOPIC_SUCCESS: {
      const {moduleId, topics, page, meta} = action.payload;
      const ids = [];
      const newIds = state.modulesTopIds;

      topics.forEach((topic) => {
        ids.push(topic.topicId);
      });
      newIds[page] = ids;

      state = {
        ...state,
        modulesTopIds        : newIds,
        moduleId             : moduleId,
        modulesTopCurrentPage: page,
        totalPages           : Math.ceil(meta.total / paginatorLength)
      };
      return state;
    }

    case DELETE_MODULE_TOPIC_SUCCESS: {
      const {id} = action.payload;

      state = {
        ...state,
        modulesTopIds: {
          ...state.modulesTopIds,
          [state.modulesTopCurrentPage]: state
            .modulesTopIds[state.modulesTopCurrentPage]
            .filter((i) => i !== id)
        }
      };
      return state;
    }

    default:
      return state;
  }
};

export default CourseReducer;
