import {
  FETCH_EMPLOYEES_SUCCESS,
  DELETE_EMPLOYEES_SUCCESS,
  SET_EMP_SEARCH_PARAMS, LOGIN_RESET
} from '../../../actions';
import {paginatorLength} from '../../../../_helpers/exportConsts';

const initState = {
  searchParams: null,
  error       : null,
  ids         : {},
  currentPage : 1,
  totalPages  : 0
};

const EmployeeReducer = (state = initState, action) => {
  switch (action.type) {
    case LOGIN_RESET:
      state = initState;
      return state;

    case FETCH_EMPLOYEES_SUCCESS: {
      const {employees, page, meta} = action.payload;
      const ids = [];
      const newIds = state.ids;

      employees.forEach((employee) => {
        ids.push(employee.employeeId);
      });
      newIds[page] = ids;

      state = {
        ...state,
        ids        : newIds,
        currentPage: page,
        totalPages : Math.ceil(meta.total / paginatorLength)
      };
      return state;
    }

    case DELETE_EMPLOYEES_SUCCESS: {
      const id = action.payload.id;
      state = {
        ...state,
        ids: {
          ...state.ids,
          [state.currentPage]: state.ids[state.currentPage].filter(
            (i) => i !== id
          )
        }
      };
      return state;
    }

    case SET_EMP_SEARCH_PARAMS: {
      state = {
        ...state,
        searchParams: action.payload.params,
        currentPage : 1
      };
      return state;
    }

    default:
      return state;
  }
};

export default EmployeeReducer;
