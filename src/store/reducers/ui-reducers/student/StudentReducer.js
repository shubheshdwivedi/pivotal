import {
  FETCH_STUDENTS_SUCCESS,
  DELETE_STUDENTS_SUCCESS,
  SET_STU_SEARCH_PARAMS, LOGIN_RESET
} from '../../../actions';
import {paginatorLength} from '../../../../_helpers/exportConsts';

const initState = {
  searchParams: null,
  error       : null,
  ids         : {},
  currentPage : 1,
  totalPages  : 0
};

const StudentReducer = (state = initState, action) => {
  switch (action.type) {
    case LOGIN_RESET:
      state = initState;
      return state;

    case FETCH_STUDENTS_SUCCESS: {
      const {students, page, meta} = action.payload;
      const ids = [];
      const newIds = state.ids;

      students.forEach((student) => {
        ids.push(student.studentId);
      });
      newIds[page] = ids;

      state = {
        ...state,
        ids        : newIds,
        currentPage: page,
        totalPages : Math.ceil(meta.total / paginatorLength)
      };
      return state;
    }

    case DELETE_STUDENTS_SUCCESS: {
      const id = action.payload.id;
      state = {
        ...state,
        ids: {
          ...state.ids,
          [state.currentPage]: state.ids[state.currentPage].filter(
            (i) => i !== id
          )
        }
      };

      return state;
    }

    case SET_STU_SEARCH_PARAMS: {
      state = {
        ...state,
        searchParams: action.payload.params,
        currentPage : 1
      };
      return state;
    }

    default:
      return state;
  }
};

export default StudentReducer;
