import {combineReducers} from 'redux';
import EmployeeReducer from './employee';
import StudentReducer from './student';
import MenteeReducer from './mentee';
import GroupReducer from './group';
import CourseReducer from './course';
import PaperReducer from './paper';

const UiReducers = combineReducers({
  EmployeeReducer,
  StudentReducer,
  MenteeReducer,
  GroupReducer,
  CourseReducer,
  PaperReducer
});

export default UiReducers;
