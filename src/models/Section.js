export default class Section {
  constructor(title, description, duration) {
    this.title = title;
    this.description = description;
    this.duration = duration;
    this.clusters = {};
    this.questions = [];
  }

  addCluster(uuid, cluster) {
    this.clusters[uuid] = cluster;
  }

  addQuestion(question) {
    this.questions.push(question);
  }
}
