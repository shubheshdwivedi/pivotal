export default class Course {
  constructor(data) {
    this.id = data['courseId'];
    this.name = data['name'];
    this.description = data['description'];
  }
}
