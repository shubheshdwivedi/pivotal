import Section from './Section';

export default class PaperCraft {
  constructor(title, description) {
    this.title = title;
    this.description = description;
    this.sections = {};
  }

  addSection(uuid, section) {
    this.sections[uuid] = section;
  }
}
