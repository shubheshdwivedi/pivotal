export default class Group {
  constructor(data) {
    this.id = data['groupId'];
    this.name = data['name'];
  }
}
