export default class Cluster {
  constructor(title, description) {
    this.title = title;
    this.description = description;
    this.questions = [];
  }

  addQuestion(question) {
    this.questions.push(question);
  }
}
