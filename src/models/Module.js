export default class Module {
  constructor(data) {
    this.id = data['moduleId'];
    this.courseId = data['courseId'];
    this.name = data['name'];
    this.description = data['description'];
  }
}
