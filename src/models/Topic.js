export default class Topic {
  constructor(data) {
    this.id = data['topicId'];
    this.moduleId = data['moduleId'];
    this.courseId = data['courseId'];
    this.name = data['name'];
    this.description = data['description'];
  }
}
