export default class Student {
  constructor(data) {
    this.id = data['studentId'];
    this.name = data['name'];
    this.email = data['email'];
    this.groups = data['groups'];
    this.mentorId = data['mentorId'];
    this.mentorName = data['mentorName'];
  }

  setMentor = (mentors) => {
    this.mentor = mentors[this.mentorId].name;
  }
}
