export default class Mentor {
  constructor(data) {
    this.id = data.employeeId;
    this.name = data.name;
  }
}
