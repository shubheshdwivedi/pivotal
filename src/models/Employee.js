export default class Employee {
  constructor(data) {
    this.id = data['employeeId'];
    this.name = data['name'];
    this.email = data['email'];
    this.roles = data['roles'];
  }
}
