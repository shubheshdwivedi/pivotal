export default class Paper {
  constructor(data) {
    this.paperId = data['paperId'];
    this.title = data['title'];
    this.sections = data['sections'];
  }
}
