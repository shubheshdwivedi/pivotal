import API from './API';

let instance = null;

const getInstance = () => {
  if (!instance) {
    instance = new API();
  }
  return instance;
};

const api = getInstance();
export default api;
