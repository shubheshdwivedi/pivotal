import axios from 'axios';

class API {
  constructor() {
    this.BASE_URL = 'http://103.212.88.131:9000';
    this.api = axios.create({
      baseURL: this.BASE_URL
    });
    this.setHeader();
  }

  get(endpoint, params) {
    return this.api.get(endpoint, {params});
  }

  post(endpoint, data) {
    return this.api.post(endpoint, data);
  }

  delete(endpoint, data) {
    return this.api.delete(endpoint, {data});
  }

  put(endpoint, data) {
    return this.api.put(endpoint, data);
  }

  setHeader() {
    const token = localStorage.getItem('token');
    if (token !== null) {
      this.api.defaults.headers.common['x-auth-token'] = token;
    }
  }
}

export default API;
