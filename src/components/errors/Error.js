import {Alert, Container} from 'react-bootstrap';
import React from 'react';

const Error = (props) => <Container>
  <Alert variant={'danger'}>{props.e}</Alert>
</Container>;


export default Error;
