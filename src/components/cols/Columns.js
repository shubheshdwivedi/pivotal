import {Button, Col, Row} from 'react-bootstrap';
import styleFixes from '../../_helpers/bootstrapStyleHelpers';
import React from 'react';

export const Columns = (props) => {
  const cols = [];
  props.headings.forEach((heading) => {
    const ref = props.reference[heading];
    if (ref) {
      const {key, name, id, route} = ref;
      cols.push(
        <Col key={key} md="3" className="grid-card add-shadow mr-5 mb-5"
          id={id}>
          <h3>{name}</h3>
          <small className="text-muted">
            Show the list of all {heading}
          </small>
          <br/>
          <Row style={{
            ...styleFixes.noMarginPadding,
            justifyContent: 'center'
          }}>
            <a href={route}>
              <Button variant="primary" className="action-btn add-bg mt-4"
                type="submit"
                style={{...styleFixes.customPadding(0.5, 1, 0.5, 1)}}>
                Show
              </Button>
            </a>
          </Row>
        </Col>
      );
    }
  });
  return cols;
};
