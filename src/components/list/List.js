import React from 'react';
import {Button, Row} from 'react-bootstrap';
import styleFixes from '../../_helpers/bootstrapStyleHelpers';

class List extends React.Component {
  getOtherButtons = (obj) => {
    const buttons = [];
    const others = this.props.others;
    if (others instanceof Array && others.length > 0) {
      others.forEach((other, index) => {
        buttons.push(
          <Button
            key={index}
            variant="secondary"
            className={'action-btn add-shadow mr-3 mt-2'}
            onClick={() => other.action(obj)}>
            {other.label}
          </Button>
        );
      });
    }
    return buttons;
  };

  render() {
    const {main, under, objects} = this.props;
    const divs = [];
    Object.entries(objects).forEach(([k, obj]) => {
      if (obj) {
        let underText = 'No ' + under + ' assigned.';
        if (obj[under] instanceof Array) {
          if (obj[under].length) {
            underText = obj[under].join(', ');
          }
        } else underText = obj[under];
        divs.push(
          <div key={k} className={'grid-card add-shadow mb-4'}>
            <Row style={{...styleFixes.noMarginPadding,
              ...{'justifyContent': 'space-between'}}}>
              <div>
                <h4>{obj[main]}</h4>
                <small className="text-muted">
                  {underText}
                </small>
              </div>
              <div>
                {this.getOtherButtons(obj)}

                {this.props.toggleEdit !== undefined && (
                  <Button
                    className={'add-bg action-btn add-shadow mr-3 mt-2'}
                    variant="primary"
                    onClick={() => this.props.toggleEdit(true, obj)}>
                    Edit
                  </Button>
                )}

                {this.props.toggleDel !== undefined && (
                  <Button
                    variant="secondary"
                    className={'action-btn add-shadow mt-2'}
                    onClick={() => this.props.toggleDel(true, obj)}>
                    Delete
                  </Button>
                )}
              </div>
            </Row>
          </div>
        );
      }
    });

    return (
      <div>
        {divs}
      </div>
    );
  }
}

export default List;
