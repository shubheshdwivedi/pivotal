import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faPlus, faSearch} from '@fortawesome/free-solid-svg-icons';
import {Button, Col, Row} from 'react-bootstrap';
import './searchbar.scss';
import styleFixes from '../../_helpers/bootstrapStyleHelpers';

const roleSelect = (props) => {
  const role = props.roles.map(
    (k, r) => <option key={r} value={k}>{k}</option>
  );

  return (
    <Col md="2">
      <select
        name="role"
        defaultValue={props.role}
        className={'search search-dropdown clear-bg'}
      >
        <option label="Roles"> </option>
        {role}
      </select>
    </Col>
  );
};

/*
 * TODO: @shubhesh this component may need
 *  refactoring to adapt to responsive design
 */

class Searchbar extends React.Component {
  render() {
    const {submit, roles, data} = this.props;
    const colMdSearch = roles ? 9 : 11;
    return (
      <form>
        <Row>
          <Col md="11">
            <Row className={'searchbar clear-bg add-shadow'}>
              <Col md={colMdSearch}>
                <input
                  type="text"
                  name="name"
                  className={'search search-input ml-2'}
                  placeholder={'Search'}
                  defaultValue={data ? data.name : ''}/>
              </Col>
              {!roles || roleSelect({roles, role: data ? data.role : ''})}
              <Col md="1" className={'text-center'}>
                <button onClick={submit} className={'search-btn ml-2 clear-bg'}>
                  <FontAwesomeIcon icon={faSearch} />
                </button>
              </Col>
            </Row>
          </Col>
          <Col md="1" style={styleFixes.customPadding(0.6, 0.6, 0.6, 0.6)}>
            <Button
              style={{borderRadius: '1000px'}}
              onClick={this.props.toggleCreate}>
              <FontAwesomeIcon icon={faPlus}/>
            </Button>
          </Col>
        </Row>
      </form>
    );
  }
}

export default Searchbar;
