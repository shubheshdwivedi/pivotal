import FormModal from './FormModal';
import EmployeeDeleteModal from './employee/EmployeeDeleteModal';
import EmployeeEditModal from './employee/EmployeeEditModal';
import StudentDeleteModal from './student/StudentDeleteModal';
import StudentEditModal from './student/StudentEditModal';
import StudentCreateModal from './student/StudentCreateModal';
import EmployeeCreateModal from './employee/EmployeeCreateModal';
import MenteeCreateModal from './mentee/MenteeCreateModal';
import MenteeEditModal from './mentee/MenteeEditModal';
import MenteeDeleteModal from './mentee/MenteeDeleteModal';
import GroupCreateModal from './groups/GroupCreateModal';
import GroupDeleteModal from './groups/GroupDeleteModal';
import GroupEditModal from './groups/GroupEditModal';
import GroupStudentDeleteModal from './groups/students/GroupStudentDeleteModal';
import GroupStudentCreateModal from './groups/students/GroupStudentCreateModal';
import CoursesDeleteModal from './courses/CoursesDeleteModal';
import CoursesCreateModal from './courses/CoursesCreateModal';
import CoursesEditModal from './courses/CoursesEditModal';
import ModuleCreateModal from './courses/modules/ModuleCreateModal';
import ModuleDeleteModal from './courses/modules/ModuleDeleteModal';
import ModuleEditModal from './courses/modules/ModuleEditModal';
import TopicDeleteModal from './courses/modules/topics/TopicDeleteModal';
import TopicCreateModal from './courses/modules/topics/TopicCreateModal';
import TopicEditModal from './courses/modules/topics/TopicEditModal';
import PaperDeleteModal from './papers/PaperDeleteModal';


export default FormModal;

export {
  EmployeeDeleteModal,
  EmployeeCreateModal,
  EmployeeEditModal,
  StudentDeleteModal,
  StudentEditModal,
  StudentCreateModal,
  MenteeCreateModal,
  MenteeEditModal,
  MenteeDeleteModal,
  GroupCreateModal,
  GroupEditModal,
  GroupDeleteModal,
  GroupStudentDeleteModal,
  GroupStudentCreateModal,
  CoursesCreateModal,
  CoursesDeleteModal,
  CoursesEditModal,
  ModuleCreateModal,
  ModuleDeleteModal,
  ModuleEditModal,
  TopicDeleteModal,
  TopicCreateModal,
  TopicEditModal,
  PaperDeleteModal
};
