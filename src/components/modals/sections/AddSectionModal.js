import FormModal from '../index';
import React from 'react';
import {modalTitles, validateFormFields} from '../../../_helpers/exportConsts';
import AddSectionForm from '../../forms/section/AddSectionForm';

class AddSectionModal extends React.Component {
  create = (e) => {
    e.preventDefault();
    const formData = e.target.form;
    const data = {};
    data.name = formData.name.value;
    data.details = formData.details.value;
    this.props.addSection(validateFormFields(data));
    this.closeModal();
  };

  closeModal = () => {
    this.props.onHide(false);
  };

  render() {
    return (
      <FormModal
        show={this.props.show}
        onHide={this.props.onHide}
        title={modalTitles.papers.addSection}
        body={<AddSectionForm create={this.create} cancel={this.closeModal}/>}
      />
    );
  }
}

export default AddSectionModal;
