import React from 'react';
import Modal from 'react-bootstrap/Modal';

const FormModal = (props) => {
  return (
    <Modal{...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered>
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          <strong>{props.title}</strong>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {props.body}
      </Modal.Body>
    </Modal>
  );
};

export default FormModal;
