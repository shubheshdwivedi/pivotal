import FormModal from '../index';
import React from 'react';
import {StudentDeleteForm} from '../../forms/student/';
import {modalTitles} from '../../../_helpers/exportConsts';

class StudentDeleteModal extends React.Component {
  del = () => {
    this.closeModal();
    this.props.del(this.props.stu.id);
  };

  closeModal = () => {
    this.props.onHide(false);
  };

  render() {
    return (
      <FormModal
        show={this.props.show}
        onHide={this.props.onHide}
        title={modalTitles.student.delete}
        body={<StudentDeleteForm del={this.del} cancel={this.closeModal}/>}
      />
    );
  }
}

export default StudentDeleteModal;
