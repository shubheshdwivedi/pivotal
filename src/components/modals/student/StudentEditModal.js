import FormModal from '../index';
import React from 'react';
import {
  modalTitles,
  validateFormFields
} from '../../../_helpers/exportConsts';
import {StudentEditForm} from '../../forms/student/';

class StudentEditModal extends React.Component {
  edit = (e, id) => {
    e.preventDefault();

    const formData = e.target.form;
    const data = {};
    data.studentId = id;
    data.name = formData.name.value;
    data.password = formData.password.value;
    data.email = formData.email.value;
    data.enable = formData.enable.checked;

    this.props.edit(validateFormFields(data));
    this.closeModal();
  };

  closeModal = () => {
    this.props.onHide(false);
  };

  render() {
    return (
      <FormModal
        show={this.props.show}
        onHide={this.props.onHide}
        title={modalTitles.student.edit}
        body={<StudentEditForm
          stu={this.props.stu}
          edit={this.edit}
          cancel={this.closeModal}
        />}
      />
    );
  }
}

export default StudentEditModal;
