import FormModal from '../index';
import React from 'react';
import {
  modalTitles,
  validateFormFields
} from '../../../_helpers/exportConsts';
import {StudentCreateForm} from '../../forms/student/';

class StudentCreateModal extends React.Component {
  create = (e) => {
    e.preventDefault();

    const formData = e.target.form;

    const data = {};
    data.name = formData.name.value;
    data.password = formData.password.value;
    data.email = formData.email.value;
    data.enable = formData.enable.checked;

    this.props.create(validateFormFields(data));
    this.closeModal();
  };

  closeModal = () => {
    this.props.onHide(false);
  };

  render() {
    return (
      <FormModal
        show={this.props.show}
        onHide={this.props.onHide}
        title={modalTitles.student.create}
        body={<StudentCreateForm
          create={this.create}
          cancel={this.closeModal}
        />}
      />
    );
  }
}

export default StudentCreateModal;
