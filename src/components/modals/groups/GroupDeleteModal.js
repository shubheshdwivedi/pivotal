import FormModal from '../index';
import React from 'react';
import {modalTitles} from '../../../_helpers/exportConsts';
import {GroupDeleteForm} from '../../forms/groups';

class GroupDeleteModal extends React.Component {
  del = () => {
    this.closeModal();
    this.props.del(this.props.gro.id);
  };

  closeModal = () => {
    this.props.onHide(false);
  };

  render() {
    return (
      <FormModal
        show={this.props.show}
        onHide={this.props.onHide}
        title={modalTitles.groups.delete}
        body={<GroupDeleteForm del={this.del} cancel={this.closeModal}/>}
      />
    );
  }
}

export default GroupDeleteModal;
