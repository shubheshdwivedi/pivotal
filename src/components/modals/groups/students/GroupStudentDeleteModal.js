import FormModal from '../../index';
import React from 'react';
import {modalTitles} from '../../../../_helpers/exportConsts';
import {GroupStudentDeleteForm} from '../../../forms/groups';

class GroupStudentDeleteModal extends React.Component {
  del = () => {
    const {grpId, stu} = this.props;
    this.props.del(grpId, stu.id);
    this.closeModal();
  };

  closeModal = () => {
    this.props.onHide(false);
  };

  render() {
    return (
      <FormModal
        show={this.props.show}
        onHide={this.props.onHide}
        title={modalTitles.groups.students.delete}
        body={<GroupStudentDeleteForm del={this.del} cancel={this.closeModal}/>}
      />
    );
  }
}

export default GroupStudentDeleteModal;
