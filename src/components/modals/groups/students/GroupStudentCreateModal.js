import FormModal from '../../index';
import React from 'react';
import {
  modalTitles
} from '../../../../_helpers/exportConsts';
import {GroupStudentCreateForm} from '../../../forms/groups';

class GroupStudentCreateModal extends React.Component {
  create = (e) => {
    e.preventDefault();
    const formData = e.target.form;
    const {grpId} = this.props;

    this.props.create(grpId, formData.student.value);
    this.closeModal();
  };

  closeModal = () => {
    this.props.onHide(false);
  };

  render() {
    return (
      <FormModal
        show={this.props.show}
        onHide={this.props.onHide}
        title={modalTitles.groups.create}
        body={<GroupStudentCreateForm
          create={this.create}
          cancel={this.closeModal}
        />}
      />
    );
  }
}

export default GroupStudentCreateModal;
