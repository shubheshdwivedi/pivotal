import FormModal from '../index';
import React from 'react';
import {
  modalTitles,
  validateFormFields
} from '../../../_helpers/exportConsts';
import {GroupEditForm} from '../../forms/groups';

class GroupEditModal extends React.Component {
  edit = (e, id) => {
    e.preventDefault();

    const formData = e.target.form;
    const data = {};
    data.groupId = id;
    data.name = formData.name.value;
    data.details = formData.details.value;

    this.props.edit(validateFormFields(data));
    this.closeModal();
  };

  closeModal = () => {
    this.props.onHide(false);
  };

  render() {
    return (
      <FormModal
        show={this.props.show}
        onHide={this.props.onHide}
        title={modalTitles.groups.edit}
        body={<GroupEditForm
          gro={this.props.gro}
          edit={this.edit}
          cancel={this.closeModal}
        />}
      />
    );
  }
}

export default GroupEditModal;
