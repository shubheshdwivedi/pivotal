import FormModal from '../index';
import React from 'react';
import {modalTitles} from '../../../_helpers/exportConsts';
import {CoursesDeleteForm} from '../../forms/courses';

class CoursesDeleteModal extends React.Component {
  del = () => {
    this.closeModal();
    this.props.del(this.props.course.id);
  };

  closeModal = () => {
    this.props.onHide(false);
  };

  render() {
    return (
      <FormModal
        show={this.props.show}
        onHide={this.props.onHide}
        title={modalTitles.courses.delete}
        body={<CoursesDeleteForm del={this.del} cancel={this.closeModal}/>}
      />
    );
  }
}

export default CoursesDeleteModal;
