import FormModal from '../index';
import React from 'react';
import {
  modalTitles,
  validateFormFields
} from '../../../_helpers/exportConsts';
import {CoursesEditForm} from '../../forms/courses';

class CoursesEditModal extends React.Component {
  edit = (e, id) => {
    e.preventDefault();

    const formData = e.target.form;
    const data = {};
    data.courseId = id;
    data.name = formData.name.value;
    data.description = formData.description.value;

    this.props.edit(validateFormFields(data));
    this.closeModal();
  };

  closeModal = () => {
    this.props.onHide(false);
  };

  render() {
    return (
      <FormModal
        show={this.props.show}
        onHide={this.props.onHide}
        title={modalTitles.courses.edit}
        body={<CoursesEditForm
          course={this.props.course}
          edit={this.edit}
          cancel={this.closeModal}
        />}
      />
    );
  }
}

export default CoursesEditModal;
