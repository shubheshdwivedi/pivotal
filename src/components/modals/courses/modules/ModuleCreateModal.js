import FormModal from '../../index';
import React from 'react';
import {
  modalTitles,
  validateFormFields
} from '../../../../_helpers/exportConsts';
import {ModuleCreateForm} from '../../../forms/modules';


class ModuleCreateModal extends React.Component {
  create = (e) => {
    e.preventDefault();

    const formData = e.target.form;
    const data = {};
    data.name = formData.name.value;
    data.description = formData.description.value;
    this.props.create(this.props.courseId, validateFormFields(data));
    this.closeModal();
  };

  closeModal = () => {
    this.props.onHide(false);
  };

  render() {
    return (
      <FormModal
        show={this.props.show}
        onHide={this.props.onHide}
        title={modalTitles.modules.create}
        body={<ModuleCreateForm
          create={this.create}
          cancel={this.closeModal}
        />}
      />
    );
  }
}

export default ModuleCreateModal;
