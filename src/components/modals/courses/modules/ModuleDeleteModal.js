import FormModal from '../../index';
import React from 'react';
import {modalTitles} from '../../../../_helpers/exportConsts';
import {ModuleDeleteForm} from '../../../forms/modules';

class ModuleDeleteModal extends React.Component {
  del = () => {
    this.closeModal();
    this.props.del(this.props.courseId, this.props.module.id);
  };

  closeModal = () => {
    this.props.onHide(false);
  };

  render() {
    return (
      <FormModal
        show={this.props.show}
        onHide={this.props.onHide}
        title={modalTitles.modules.delete}
        body={<ModuleDeleteForm del={this.del} cancel={this.closeModal}/>}
      />
    );
  }
}

export default ModuleDeleteModal;
