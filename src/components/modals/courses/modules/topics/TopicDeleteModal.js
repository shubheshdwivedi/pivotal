import FormModal from '../../../index';
import React from 'react';
import {modalTitles} from '../../../../../_helpers/exportConsts';
import {TopicDeleteForm} from '../../../../forms/topics';

class TopicDeleteModal extends React.Component {
  del = () => {
    const {courseId, moduleId, topic} = this.props;
    this.props.del(courseId, moduleId, topic.id);
    this.closeModal();
  };

  closeModal = () => {
    this.props.onHide(false);
  };

  render() {
    return (
      <FormModal
        show={this.props.show}
        onHide={this.props.onHide}
        title={modalTitles.topics.delete}
        body={<TopicDeleteForm del={this.del} cancel={this.closeModal}/>}
      />
    );
  }
}

export default TopicDeleteModal;
