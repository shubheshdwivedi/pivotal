import FormModal from '../../../index';
import React from 'react';
import {
  modalTitles,
  validateFormFields
} from '../../../../../_helpers/exportConsts';
import {TopicEditForm} from '../../../../forms/topics';

class TopicEditModal extends React.Component {
  edit = (e, id) => {
    e.preventDefault();
    const {courseId, moduleId, topic} = this.props;

    const formData = e.target.form;
    const data = {};

    data.name = formData.name.value;
    data.description = formData.description.value;

    this.props.edit(
      courseId,
      moduleId,
      topic.id,
      validateFormFields(data)
    );
    this.closeModal();
  };

  closeModal = () => {
    this.props.onHide(false);
  };

  render() {
    return (
      <FormModal
        show={this.props.show}
        onHide={this.props.onHide}
        title={modalTitles.topics.edit}
        body={<TopicEditForm
          topic={this.props.topic}
          edit={this.edit}
          cancel={this.closeModal}
        />}
      />
    );
  }
}

export default TopicEditModal;
