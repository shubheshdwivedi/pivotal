import FormModal from '../../../index';
import React from 'react';
import {
  modalTitles,
  validateFormFields
} from '../../../../../_helpers/exportConsts';
import {TopicCreateForm} from '../../../../forms/topics';


class TopicCreateModal extends React.Component {
  create = (e) => {
    e.preventDefault();

    const {courseId, moduleId} = this.props;
    const formData = e.target.form;
    const data = {};
    data.name = formData.name.value;
    data.description = formData.description.value;

    this.props.create(courseId, moduleId, validateFormFields(data));
    this.closeModal();
  };

  closeModal = () => {
    this.props.onHide(false);
  };

  render() {
    return (
      <FormModal
        show={this.props.show}
        onHide={this.props.onHide}
        title={modalTitles.topics.create}
        body={<TopicCreateForm
          create={this.create}
          cancel={this.closeModal}
        />}
      />
    );
  }
}

export default TopicCreateModal;
