import FormModal from '../../index';
import React from 'react';
import {
  modalTitles,
  validateFormFields
} from '../../../../_helpers/exportConsts';
import {ModuleEditForm} from '../../../forms/modules';

class ModuleEditModal extends React.Component {
  edit = (e, id) => {
    e.preventDefault();

    const formData = e.target.form;
    const data = {};
    data.name = formData.name.value;
    data.description = formData.description.value;

    this.props.edit(this.props.courseId, id, validateFormFields(data));
    this.closeModal();
  };

  closeModal = () => {
    this.props.onHide(false);
  };

  render() {
    return (
      <FormModal
        show={this.props.show}
        onHide={this.props.onHide}
        title={modalTitles.modules.edit}
        body={<ModuleEditForm
          module={this.props.module}
          edit={this.edit}
          cancel={this.closeModal}
        />}
      />
    );
  }
}

export default ModuleEditModal;
