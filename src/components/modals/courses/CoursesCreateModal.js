import FormModal from '../index';
import React from 'react';
import {
  modalTitles,
  validateFormFields
} from '../../../_helpers/exportConsts';
import {CoursesCreateForm} from '../../forms/courses';


class CoursesCreateModal extends React.Component {
  create = (e) => {
    e.preventDefault();

    const formData = e.target.form;
    const data = {};

    data.name = formData.name.value;
    data.description = formData.description.value;
    this.props.create(validateFormFields(data));
    this.closeModal();
  };

  closeModal = () => {
    this.props.onHide(false);
  };

  render() {
    return (
      <FormModal
        show={this.props.show}
        onHide={this.props.onHide}
        title={modalTitles.courses.create}
        body={<CoursesCreateForm
          create={this.create}
          cancel={this.closeModal}
        />}
      />
    );
  }
}

export default CoursesCreateModal;
