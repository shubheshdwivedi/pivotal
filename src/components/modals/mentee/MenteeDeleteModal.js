import FormModal from '../index';
import React from 'react';
import {modalTitles} from '../../../_helpers/exportConsts';
import {MenteeDeleteForm} from '../../forms/mentee/';

class MenteeDeleteModal extends React.Component {
  del = () => {
    this.closeModal();
    this.props.del(this.props.mentee.id);
  };

  closeModal = () => {
    this.props.onHide(false);
  };

  render() {
    return (
      <FormModal
        show={this.props.show}
        onHide={this.props.onHide}
        title={modalTitles.mentees.delete}
        body={<MenteeDeleteForm del={this.del} cancel={this.closeModal}/>}
      />
    );
  }
}

export default MenteeDeleteModal;
