import FormModal from '../index';
import React from 'react';
import {
  modalTitles,
  validateFormFields
} from '../../../_helpers/exportConsts';
import {MenteeEditForm} from '../../forms/mentee/';

class MenteeEditModal extends React.Component {
  edit = (e, id) => {
    e.preventDefault();

    const formData = e.target.form;
    const data = {};
    data.studentId = id;
    data.employeeId = formData.mentor.value;

    this.props.edit(validateFormFields(data));
    this.closeModal();
  };

  closeModal = () => {
    this.props.onHide(false);
  };

  render() {
    return (
      <FormModal
        show={this.props.show}
        onHide={this.props.onHide}
        title={modalTitles.mentees.edit}
        body={<MenteeEditForm
          mentee={this.props.mentee}
          edit={this.edit}
          cancel={this.closeModal}
        />}
      />
    );
  }
}

export default MenteeEditModal;
