import FormModal from '../index';
import React from 'react';
import {
  modalTitles,
  validateFormFields
} from '../../../_helpers/exportConsts';
import {MenteeCreateForm} from '../../forms/mentee/';

class MenteeCreateModal extends React.Component {
  create = (e) => {
    e.preventDefault();

    const formData = e.target.form;
    const data = {};
    data.studentId = formData.student.value;
    data.employeeId = formData.mentor.value;
    this.props.create(validateFormFields(data));
    this.closeModal();
  };

  closeModal = () => {
    this.props.onHide(false);
  };

  render() {
    return (
      <FormModal
        show={this.props.show}
        onHide={this.props.onHide}
        title={modalTitles.mentees.create}
        body={<MenteeCreateForm
          create={this.create}
          cancel={this.closeModal}
        />}
      />
    );
  }
}

export default MenteeCreateModal;
