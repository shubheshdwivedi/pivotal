import FormModal from '../index';
import React from 'react';
import {
  modalTitles,
  eRolesArray,
  validateFormFields
} from '../../../_helpers/exportConsts';
import {EmployeeEditForm} from '../../forms/employee/';

class EmployeeEditModal extends React.Component {
  edit = (e, id) => {
    e.preventDefault();

    const formData = e.target.form;
    const data = {};
    data.employeeId = id;
    data.roles = [];
    eRolesArray.forEach((role) => {
      if (formData[role].checked) data.roles.push(role);
    });

    data.name = formData.name.value;
    data.password = formData.password.value;
    data.email = formData.email.value;
    data.enable = formData.enable.checked;

    this.props.edit(validateFormFields(data));
    this.closeModal();
  };

  closeModal = () => {
    this.props.onHide(false);
  };

  render() {
    return (
      <FormModal
        show={this.props.show}
        onHide={this.props.onHide}
        title={modalTitles.employee.edit}
        body={<EmployeeEditForm
          emp={this.props.emp}
          edit={this.edit}
          cancel={this.closeModal}
        />}
      />
    );
  }
}

export default EmployeeEditModal;
