import FormModal from '../index';
import React from 'react';
import {
  modalTitles,
  eRolesArray,
  validateFormFields
} from '../../../_helpers/exportConsts';
import {EmployeeCreateForm} from '../../forms/employee/';

class EmployeeCreateModal extends React.Component {
  create = (e) => {
    e.preventDefault();

    const formData = e.target.form;
    const data = {};
    data.roles = [];
    eRolesArray.forEach((role) => {
      if (formData[role].checked) data.roles.push(role);
    });

    data.name = formData.name.value;
    data.password = formData.password.value;
    data.email = formData.email.value;
    data.enable = formData.enable.checked;
    this.props.create(validateFormFields(data));
    this.closeModal();
  };

  closeModal = () => {
    this.props.onHide(false);
  };

  render() {
    return (
      <FormModal
        show={this.props.show}
        onHide={this.props.onHide}
        title={modalTitles.employee.create}
        body={<EmployeeCreateForm
          create={this.create}
          cancel={this.closeModal}
        />}
      />
    );
  }
}

export default EmployeeCreateModal;
