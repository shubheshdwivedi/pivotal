import FormModal from '../index';
import React from 'react';
import {EmployeeDeleteForm} from '../../forms/employee/';
import {modalTitles} from '../../../_helpers/exportConsts';

class EmployeeDeleteModal extends React.Component {
  del = () => {
    this.closeModal();
    this.props.del(this.props.emp.id);
  };

  closeModal = () => {
    this.props.onHide(false);
  };

  render() {
    return (
      <FormModal
        show={this.props.show}
        onHide={this.props.onHide}
        title={modalTitles.employee.delete}
        body={<EmployeeDeleteForm del={this.del} cancel={this.closeModal}/>}
      />
    );
  }
}

export default EmployeeDeleteModal;
