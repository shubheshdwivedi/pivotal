import FormModal from '../index';
import React from 'react';
import {modalTitles} from '../../../_helpers/exportConsts';
import {PaperDeleteForm} from '../../forms/papers';

class PaperDeleteModal extends React.Component {
  del = () => {
    this.closeModal();
    this.props.del(this.props.paper.paperId);
  };

  closeModal = () => {
    this.props.onHide(false);
  };

  render() {
    return (
      <FormModal
        show={this.props.show}
        onHide={this.props.onHide}
        title={modalTitles.papers.delete}
        body={<PaperDeleteForm del={this.del} cancel={this.closeModal}/>}
      />
    );
  }
}

export default PaperDeleteModal;
