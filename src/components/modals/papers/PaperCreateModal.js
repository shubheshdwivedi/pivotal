import FormModal from '../index';
import React from 'react';
import {modalTitles, validateFormFields} from '../../../_helpers/exportConsts';
import PaperCreateForm from '../../forms/papers/PaperCreateForm';

class PaperCreateModal extends React.Component {
  create = (e) => {
    e.preventDefault();
    const formData = e.target.form;
    const data = {};
    data.name = formData.name.value;
    data.details = formData.details.value;
    this.props.create(validateFormFields(data));
    this.closeModal();
  };

  closeModal = () => {
    this.props.onHide(false);
  };

  render() {
    return (
      <FormModal
        show={this.props.show}
        onHide={this.props.onHide}
        title={modalTitles.papers.create}
        body={<PaperCreateForm create={this.create} cancel={this.closeModal}/>}
      />
    );
  }
}

export default PaperCreateModal;
