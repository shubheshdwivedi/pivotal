import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faPowerOff} from '@fortawesome/free-solid-svg-icons';
import './header.scss';
import {APP_NAME} from '../../_helpers/exportConsts';
import {Nav, Navbar} from 'react-bootstrap';

// TODO: fix nav collapse @shubhesh

const Header = (props) => {
  return (
    <Navbar expand="lg" id="myHeader">
      <Navbar.Brand href="/" id="brand">
        {APP_NAME.toUpperCase()}</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          {/* Adding nav links here as per requirement */}
        </Nav>
        <div className="collapse navbar-collapse justify-content-end"
          id="navbarSupportedContent">
          <ul className="navbar-nav">
            <li className="nav-item mr-3">
              <span className="navbar-text font-weight-bold">{props.name}</span>
            </li>
            <li className="nav-item">
              <div className="nav-link navbar-text" onClick={props.logout}>
                <FontAwesomeIcon icon={faPowerOff} />
              </div>
            </li>
          </ul>
        </div>
      </Navbar.Collapse>
    </Navbar>
  );
};

export default Header;
