import React from 'react';
import {Container, Form} from 'react-bootstrap';
import {getSearchDebounced} from '../../_helpers/exportConsts';
import Mentor from '../../models/Mentor';
import styleFixes from '../../_helpers/bootstrapStyleHelpers';
import './SearchContainer.scss';
import {SearchMentorsRequest} from '../../requests/employees/';

const searchAPIDebounced = getSearchDebounced(new SearchMentorsRequest());

class MentorSearch extends React.Component {
  state = {
    text   : '',
    mentors: null
  };

  createState = (res) => {
    const mentors = [];
    res.forEach((mentor) => mentors.push(new Mentor(mentor)));
    this.setState({...this.state, mentors});
  };

  handleTextChange = async (e) => {
    const text = e.target.value;
    this.setState({text, mentors: null});
    await searchAPIDebounced(text)
      .then((res) => this.createState(res.data.data));
  };

  selectThis = (mentor) => {
    const selected = document.getElementById('selectedMentor');
    const mentorInput = document.getElementById('mentorInput');
    const listContainer = document.querySelector('#list-container');

    selected.innerText = mentor.name;
    mentorInput.value = mentor.id;
    listContainer.innerHTML = '';
  };

  getResults = () => {
    const divs = [];
    const mentors = this.state.mentors;
    if (mentors instanceof Array) {
      mentors.forEach((mentor) => {
        divs.push(
          <li key={mentor.id}>
            <label
              className="select-box__option"
              htmlFor="0"
              aria-hidden={true}
              style={{borderTop: 'none'}}
              onClick={() => this.selectThis(mentor)}>
              {mentor.name}
            </label>
          </li>
        );
      });
    }
    return divs;
  };

  componentWillUnmount() {
    this.setState = () => {};
  }

  render() {
    return (
      <Container className={'search-container'}>
        <input type="number" name="mentor" id="mentorInput" hidden/>
        <div id="selectedMentor" className={'selected mb-3'}>
          No Mentor Selected</div>
        <h5>Search for a Mentor</h5>
        <Form.Group controlId="formBasicMentorName" style={{marginBottom: 0}}>
          <Form.Control
            type="text"
            placeholder="Enter Mentor name"
            onChange={this.handleTextChange.bind(this)}
            style={styleFixes.formInputs}
            autoComplete="off"/>
        </Form.Group>
        <ul className="select-box__list" id={'list-container'}>
          {this.getResults()}
        </ul>
      </Container>
    );
  }
}

export default MentorSearch;
