import React from 'react';
import {SearchStudentRequest} from '../../requests/students';
import Student from '../../models/Student';
import {Container, Form} from 'react-bootstrap';
import styleFixes from '../../_helpers/bootstrapStyleHelpers';
import './SearchContainer.scss';
import {getSearchDebounced} from '../../_helpers/exportConsts';

const searchAPIDebounced = getSearchDebounced(new SearchStudentRequest());

class StudentSearch extends React.Component {
  state = {
    text    : '',
    students: null
  };

  createState = (res) => {
    const students = [];
    res.forEach((student) => students.push(new Student(student)));
    this.setState({...this.state, students});
  };

  handleTextChange = async (e) => {
    const text = e.target.value;
    this.setState({text, students: null});
    await searchAPIDebounced(text)
      .then((res) => this.createState(res.data.data));
  };

  selectThis = (student) => {
    const selected = document.getElementById('selectedStudent');
    const studentInput = document.getElementById('studentInput');
    const listContainer = document.querySelector('#list-container');

    selected.innerText = student.name;
    studentInput.value = student.id;
    listContainer.innerHTML = '';
  };

  getResults = () => {
    const divs = [];
    const students = this.state.students;
    if (students instanceof Array) {
      students.forEach((student) => {
        divs.push(
          <li key={student.id}>
            <label
              className="select-box__option"
              htmlFor="0"
              aria-hidden={true}
              style={{borderTop: 'none'}}
              onClick={() => this.selectThis(student)}>
              {student.name}
            </label>
          </li>
        );
      });
    }
    return divs;
  };

  componentWillUnmount() {
    this.setState = () => {};
  }

  render() {
    return (
      <Container className={'search-container'}>
        <input type="number" name="student" id="studentInput" hidden/>
        <div id="selectedStudent" className={'selected mb-3'}>
          No Student Selected</div>
        <h5>Search for a student</h5>
        <Form.Group controlId="formBasicStudentName" style={{marginBottom: 0}}>
          <Form.Control
            type="text"
            placeholder="Enter student name"
            onChange={this.handleTextChange.bind(this)}
            style={styleFixes.formInputs}
            autoComplete="off"/>
        </Form.Group>
        <ul className="select-box__list" id={'list-container'}>
          {this.getResults()}
        </ul>
      </Container>
    );
  }
}

export default StudentSearch;
