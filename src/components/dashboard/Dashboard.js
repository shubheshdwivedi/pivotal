import React from 'react';
import Header from '../header/Header';
import {EmployeeRouter, TenantRouter} from '../../router/';
import {uRoles} from '../../_helpers/exportConsts';

const Dashboard = (props) => {
  const user = props.user;
  const userType = user.userType;

  return (
    <div>
      <Header name={props.user.name} logout={props.logout}/>
      {userType === uRoles.tenant && <TenantRouter/>}
      {userType === uRoles.employee && <EmployeeRouter/>}
    </div>
  );
};

export default Dashboard;
