import React from 'react';
import {Button, ButtonToolbar, Form, Row} from 'react-bootstrap';
import styleFixes from '../../../_helpers/bootstrapStyleHelpers';

class CoursesEditForm extends React.Component {
  render() {
    const course = this.props.course;
    return (
      <Form style={{fontSize: '1.2em'}}>
        <Form.Group controlId="formBasicName">
          <Form.Control
            type="text"
            name="name"
            placeholder="Name"
            defaultValue={course.name}
            style={styleFixes.formInputs}/>
        </Form.Group>
        <Form.Group controlId="formBasicName">
          <Form.Control
            type="textarea"
            name="description"
            defaultValue={course.description}
            placeholder="Course Description"
            style={styleFixes.formInputs}/>
        </Form.Group>
        <Row className={'justify-content-center'}>
          <ButtonToolbar className={' float-right'}>
            <Button
              onClick={this.props.cancel}
              variant={'secondary'}
              className={'action-btn mr-3'}>
              Cancel
            </Button>
            <Button
              onClick={(e) => this.props.edit(e, course.id)}
              className={'action-btn add-bg '}
              type={'submit'}>
              Submit
            </Button>
          </ButtonToolbar>
        </Row>
        <br/>
      </Form>
    );
  }
}

export default CoursesEditForm;
