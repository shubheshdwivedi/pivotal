import CoursesDeleteForm from './CoursesDeleteForm';
import CoursesCreateForm from './CoursesCreateForm';
import CoursesEditForm from './CoursesEditForm';

export {
  CoursesDeleteForm,
  CoursesCreateForm,
  CoursesEditForm
};
