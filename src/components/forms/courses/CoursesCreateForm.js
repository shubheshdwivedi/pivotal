import React from 'react';
import {Button, ButtonToolbar, Col, Form, Nav, Row, Tab} from 'react-bootstrap';
import styleFixes from '../../../_helpers/bootstrapStyleHelpers';

class CoursesCreateForm extends React.Component {
  render() {
    return (
      <Form style={{fontSize: '1.2em'}}>
        <Tab.Container id="left-tabs-example" defaultActiveKey="first">
          <Row>
            <Row className={'justify-content-center'} style={{width: '100%'}}>
              <Nav variant="pills" >
                <Nav.Item>
                  <Nav.Link eventKey="first">Individual</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="second">Bulk</Nav.Link>
                </Nav.Item>
              </Nav>
            </Row>
            <Col sm={9} style={{margin: '2rem auto'}}>
              <Tab.Content>
                <Tab.Pane eventKey="first">
                  <Form.Group controlId="formBasicName">
                    <Form.Control
                      type="text"
                      name="name"
                      placeholder="Course Name"
                      style={styleFixes.formInputs}/>
                  </Form.Group>
                  <Form.Group controlId="formBasicName">
                    <Form.Control
                      type="textarea"
                      name="description"
                      placeholder="Course Description"
                      style={styleFixes.formInputs}/>
                  </Form.Group>
                  <Row className={'justify-content-center'}>
                    <ButtonToolbar className={' float-right'}>
                      <Button
                        onClick={this.props.cancel}
                        variant={'secondary'}
                        className={'action-btn mr-3'}>
                        Cancel
                      </Button>
                      <Button
                        onClick={this.props.create}
                        className={'action-btn add-bg '}
                        type={'submit'}>
                        Submit
                      </Button>
                    </ButtonToolbar>
                  </Row>
                  <br/>
                </Tab.Pane>
                <Tab.Pane eventKey="second">
                  <Row className={'grid-card add-shadow mb-4 px-4 py-4'}>
                    <h4>Download Format</h4>
                    <small className={'text-muted'}>
                      Download the format to upload courses in bulk
                    </small>
                  </Row>
                  <Row className={'grid-card add-shadow mb-4 px-4 py-4'}>
                    <h4>Upload Bulk Courses</h4>
                    <small className={'text-muted'}>
                      Upload courses in bulk, in the above prescribed format
                    </small>
                  </Row>
                </Tab.Pane>
              </Tab.Content>
            </Col>
          </Row>
        </Tab.Container>
      </Form>
    );
  }
}

export default CoursesCreateForm;
