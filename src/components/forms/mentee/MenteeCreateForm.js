import React from 'react';
import {Button, ButtonToolbar, Form, Row} from 'react-bootstrap';
import StudentSearch from '../../searchInputResult/StudentSearch';
import MentorSearch from '../../searchInputResult/MentorSearch';

class MenteeCreateForm extends React.Component {
  render() {
    return (
      <Form style={{fontSize: '1.2em'}}>
        <Row className={'justify-content-center'}>
          <MentorSearch/>
        </Row>

        <Row className={'justify-content-center'}>
          <StudentSearch/>
        </Row>

        <Row className={'justify-content-center'}>
          <ButtonToolbar className={' float-right'}>
            <Button
              onClick={this.props.cancel}
              variant={'secondary'}
              className={'action-btn mr-3'}>
              Cancel
            </Button>
            <Button
              onClick={this.props.create}
              className={'action-btn add-bg '}
              type={'submit'}>
              Submit
            </Button>
          </ButtonToolbar>
        </Row>
      </Form>
    );
  }
}

export default MenteeCreateForm;
