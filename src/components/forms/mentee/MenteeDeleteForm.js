import React from 'react';
import {Button, ButtonToolbar, Row} from 'react-bootstrap';
import styleFixes from '../../../_helpers/bootstrapStyleHelpers';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faCheck, faTimes} from '@fortawesome/free-solid-svg-icons';

const MenteeDeleteForm = (props) => {
  return (
    <Row style={{...styleFixes.customPadding(1, 2, 1, 2),
      justifyContent: 'space-between'}}>
      <h4>Are you sure?</h4>
      <br/>
      <ButtonToolbar>
        <Button variant={'secondary'} className={'mr-2'} onClick={props.del}>
          <FontAwesomeIcon icon={faCheck}/>
          <span className={'ml-3'}>Yes</span>
        </Button>
        <Button className={'action-btn add-bg'}
          style={styleFixes.customPadding(0.7, 1, 0.7, 1)}
          onClick={props.cancel}>
          <FontAwesomeIcon icon={faTimes}/>
          <span className={'ml-3'}>No</span>
        </Button>
      </ButtonToolbar>
    </Row>
  );
};

export default MenteeDeleteForm;
