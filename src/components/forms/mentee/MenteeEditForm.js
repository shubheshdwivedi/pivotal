import React from 'react';
import {Button, ButtonToolbar, Form, Row} from 'react-bootstrap';
import MentorSearch from '../../searchInputResult/MentorSearch';

class MenteeEditForm extends React.Component {
  getOptions = (objects) => {
    const options = [];
    Object.entries(objects).forEach(([k, object]) => {
      options.push(
        <option key={object.id} value={object.id}>{object.name}</option>
      );
    });

    return options;
  };

  render() {
    const mentee = this.props.mentee;

    return (
      <Form style={{fontSize: '1.2em'}}>
        <Row>
          <MentorSearch/>
        </Row>
        <Row className={'justify-content-center'}>
          <ButtonToolbar className={' float-right'}>
            <Button
              onClick={this.props.cancel}
              variant={'secondary'}
              className={'action-btn mr-3'}>
              Cancel
            </Button>
            <Button
              onClick={(e) => this.props.edit(e, mentee.id)}
              className={'action-btn add-bg '}
              type={'submit'}>
              Submit
            </Button>
          </ButtonToolbar>
        </Row>
        <br/>
      </Form>
    );
  }
}

export default MenteeEditForm;
