import React from 'react';
import {Button, ButtonToolbar, Form, Row} from 'react-bootstrap';
import styleFixes from '../../../_helpers/bootstrapStyleHelpers';

class TopicCreateForm extends React.Component {
  render() {
    return (
      <Form style={{fontSize: '1.2em'}}>
        <Form.Group controlId="formBasicName">
          <Form.Control
            type="text"
            name="name"
            placeholder="Name"
            style={styleFixes.formInputs}/>
        </Form.Group>
        <Form.Group controlId="formBasicName">
          <Form.Control
            type="textarea"
            name="description"
            placeholder="Topic Description"
            style={styleFixes.formInputs}/>
        </Form.Group>
        <Row className={'justify-content-center'}>
          <ButtonToolbar className={' float-right'}>
            <Button
              onClick={this.props.cancel}
              variant={'secondary'}
              className={'action-btn mr-3'}>
              Cancel
            </Button>
            <Button
              onClick={this.props.create}
              className={'action-btn add-bg '}
              type={'submit'}>
              Submit
            </Button>
          </ButtonToolbar>
        </Row>
        <br/>
      </Form>
    );
  }
}

export default TopicCreateForm;
