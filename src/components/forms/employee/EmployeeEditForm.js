import React from 'react';
import {eRoles} from '../../../_helpers/exportConsts';
import {Button, ButtonToolbar, Container, Form, Row} from 'react-bootstrap';
import styleFixes from '../../../_helpers/bootstrapStyleHelpers';

class EmployeeEditForm extends React.Component {
  checked(args) {
    // Add check
  }

  render() {
    const emp = this.props.emp;
    const roleCheckboxes = Object.keys(eRoles).map((k, i) => {
      return (
        <Form.Group key={i} controlId="formBasicCheckbox">
          <div className="pretty p-curve p-default">
            <input name={eRoles[k]} type="checkbox" value={eRoles[k]}/>
            <div className="state p-primary">
              <label>{eRoles[k]}</label>
            </div>
          </div>
        </Form.Group>
      );
    });

    return (
      <Form style={{fontSize: '1.2em'}}>
        <Form.Group controlId="formBasicName">
          <Form.Control
            type="text"
            name="name"
            placeholder="Name"
            defaultValue={emp.name}
            style={styleFixes.formInputs}/>
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
          <Form.Control
            type="password"
            name="password"
            placeholder={'Password'}
            style={styleFixes.formInputs}/>
        </Form.Group>

        <Form.Group controlId="formBasicEmail">
          <Form.Control
            type="email"
            name="email"
            placeholder="Email"
            defaultValue={emp.email}
            style={styleFixes.formInputs}/>
        </Form.Group>

        <Container className={'py-4 px-5'}
          style={{borderRadius: '8px', background: '#efefef'}}>
          <h5><strong>Roles</strong></h5>
          <hr/>
          <Form.Group controlId="formBasicCheckbox">
            {roleCheckboxes}
          </Form.Group>
        </Container>
        <br/>
        <Row className={'px-4 py-2'}>
          <Form.Group controlId="formBasicCheckbox">
            <div className="pretty p-switch p-fill">
              <input type="checkbox" name={'enable'} />
              <div className="state p-success">
                <label>Enable employee</label>
              </div>
            </div>
          </Form.Group>
        </Row>
        <Row className={'justify-content-center'}>
          <ButtonToolbar className={' float-right'}>
            <Button
              onClick={this.props.cancel}
              variant={'secondary'}
              className={'action-btn mr-3'}>
              Cancel
            </Button>
            <Button
              onClick={(e) => this.props.edit(e, emp.id)}
              className={'action-btn add-bg '}
              type={'submit'}>
              Submit
            </Button>
          </ButtonToolbar>
        </Row>
        <br/>
      </Form>
    );
  }
}

export default EmployeeEditForm;
