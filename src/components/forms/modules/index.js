import ModuleCreateForm from './ModuleCreateForm';
import ModuleEditForm from './ModuleEditForm';
import ModuleDeleteForm from './ModuleDeleteForm';

export {
  ModuleCreateForm,
  ModuleDeleteForm,
  ModuleEditForm
};
