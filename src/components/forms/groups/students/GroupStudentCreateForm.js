import React from 'react';
import {Button, ButtonToolbar, Form, Row} from 'react-bootstrap';
import StudentSearch from '../../../searchInputResult/StudentSearch';

class GroupStudentCreateForm extends React.Component {
  render() {
    return (
      <Form style={{fontSize: '1.2em'}}>
        <Row>
          <StudentSearch/>
        </Row>

        <Row className={'justify-content-center'}>
          <ButtonToolbar className={' float-right'}>
            <Button
              onClick={this.props.cancel}
              variant={'secondary'}
              className={'action-btn mr-3'}>
              Cancel
            </Button>
            <Button
              onClick={(e) => this.props.create(e)}
              className={'action-btn add-bg '}
              type={'submit'}>
              Submit
            </Button>
          </ButtonToolbar>
        </Row>
        <br/>
      </Form>
    );
  }
}

export default GroupStudentCreateForm;
