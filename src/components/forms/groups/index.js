import GroupCreateForm from './GroupCreateForm';
import GroupEditForm from './GroupEditForm';
import GroupDeleteForm from './GroupDeleteForm';
import GroupStudentDeleteForm from './students/GroupStudentDeleteForm';
import GroupStudentCreateForm from './students/GroupStudentCreateForm';

export {
  GroupCreateForm,
  GroupEditForm,
  GroupDeleteForm,
  GroupStudentDeleteForm,
  GroupStudentCreateForm
};
