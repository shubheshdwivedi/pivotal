import React from 'react';
import {Button, ButtonToolbar, Form, Row} from 'react-bootstrap';
import styleFixes from '../../../_helpers/bootstrapStyleHelpers';

class GroupEditForm extends React.Component {
  render() {
    const gro = this.props.gro;
    return (
      <Form style={{fontSize: '1.2em'}}>
        <Form.Group controlId="formBasicName">
          <Form.Control
            type="text"
            name="name"
            placeholder="Name"
            defaultValue={gro.name}
            style={styleFixes.formInputs}/>
        </Form.Group>
        <Form.Group controlId="formBasicName">
          <Form.Control
            type="textarea"
            name="details"
            defaultValue={gro.description}
            placeholder="Group Description"
            style={styleFixes.formInputs}/>
        </Form.Group>
        <Row className={'justify-content-center'}>
          <ButtonToolbar className={' float-right'}>
            <Button
              onClick={this.props.cancel}
              variant={'secondary'}
              className={'action-btn mr-3'}>
              Cancel
            </Button>
            <Button
              onClick={(e) => this.props.edit(e, gro.id)}
              className={'action-btn add-bg '}
              type={'submit'}>
              Submit
            </Button>
          </ButtonToolbar>
        </Row>
        <br/>
      </Form>
    );
  }
}

export default GroupEditForm;
