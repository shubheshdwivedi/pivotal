import StudentEditForm from './StudentEditForm';
import StudentDeleteForm from './StudentDeleteForm';
import StudentCreateForm from './StudentCreateForm';

export {
  StudentDeleteForm,
  StudentEditForm,
  StudentCreateForm
};
