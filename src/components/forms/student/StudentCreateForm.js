import React from 'react';
import {Button, ButtonToolbar, Form, Row} from 'react-bootstrap';
import styleFixes from '../../../_helpers/bootstrapStyleHelpers';

class StudentCreateForm extends React.Component {
  render() {
    return (
      <Form style={{fontSize: '1.2em'}}>
        <Form.Group controlId="formBasicName">
          <Form.Control
            type="text"
            name="name"
            placeholder="Name"
            style={styleFixes.formInputs}/>
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
          <Form.Control
            type="password"
            name="password"
            placeholder={'Password'}
            style={styleFixes.formInputs}/>
        </Form.Group>

        <Form.Group controlId="formBasicEmail">
          <Form.Control
            type="email"
            name="email"
            placeholder="Email"
            style={styleFixes.formInputs}/>
        </Form.Group>

        <br/>
        <Row className={'px-4 py-2'}>
          <Form.Group controlId="formBasicCheckbox">
            <div className="pretty p-switch p-fill">
              <input type="checkbox" name={'enable'} />
              <div className="state p-success">
                <label>Enable Student</label>
              </div>
            </div>
          </Form.Group>
        </Row>
        <Row className={'justify-content-center'}>
          <ButtonToolbar className={' float-right'}>
            <Button
              onClick={this.props.cancel}
              variant={'secondary'}
              className={'action-btn mr-3'}>
              Cancel
            </Button>
            <Button
              onClick={(e) => this.props.create(e)}
              className={'action-btn add-bg '}
              type={'submit'}>
              Submit
            </Button>
          </ButtonToolbar>
        </Row>
        <br/>
      </Form>
    );
  }
}

export default StudentCreateForm;
