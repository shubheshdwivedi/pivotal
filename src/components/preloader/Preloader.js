import React from 'react';
import './preoloader.scss';

const Preloader = () => <div className={'cover-all'}>
  <div className="loader">Loading...</div>
</div>;

export default Preloader;
