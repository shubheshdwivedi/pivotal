import React from 'react';
import {paginatorLength} from '../../_helpers/exportConsts';
import {Pagination} from 'react-bootstrap';

class Paginator extends React.Component {
    getCycle = () => {
      const {currentPage, totalPages} = this.props;
      if (currentPage % paginatorLength === 0) {
        return [currentPage - paginatorLength + 1, currentPage];
      }
      const cycle = Math.floor(currentPage / paginatorLength);
      const first = paginatorLength * cycle + 1;
      const last = Math.min(totalPages, paginatorLength * (cycle + 1));
      return [first, last];
    };

    checkPrev = () => this.props.currentPage === 1;

    checkNext = () => this.props.currentPage === this.props.totalPages;

    renderItems = () => {
      const [first, last] = this.getCycle();
      const items = [];
      for (let i = first; i <= last; i ++) {
        items.push(
          <Pagination.Item
            key={i}
            active={i === this.props.currentPage}
            onClick={() => this.props.fetch(i)}
          >
            {i}
          </Pagination.Item>
        );
      }
      return items;
    };

    render() {
      const {fetch, currentPage, totalPages} = this.props;
      return (
        <>
          {this.props.totalPages > 0 && (
            <div>
              <Pagination>
                <Pagination.First onClick={() => fetch(1)}/>
                <Pagination.Prev disabled={this.checkPrev()}
                  onClick={() => fetch(currentPage - 1)}/>
                {this.renderItems()}
                <Pagination.Next disabled={this.checkNext()}
                  onClick={() => fetch(currentPage + 1)}/>
                <Pagination.Last onClick={() => fetch(totalPages)}/>
              </Pagination>
            </div>
          )}
        </>
      );
    }
}

export default Paginator;
