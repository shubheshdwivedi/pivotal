import React from 'react';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/es/integration/react';
import {store, persistor} from '../store/store';
import Router from '../router';
import './App.scss';

function App() {
  return (
    <div id="app" className="container-fluid p-0">
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <Router/>
        </PersistGate>
      </Provider>
    </div>
  );
}

export default App;
