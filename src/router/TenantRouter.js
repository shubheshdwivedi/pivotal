import React from 'react';
import {Switch, Route} from 'react-router-dom';
import routes from '../_helpers/routeHelpers';
import TenantDashboard from '../containers/tenant-dashboard';
import Employee from '../containers/employee';
import Student from '../containers/student/';
import Mentee from '../containers/mentee/';
import Group from '../containers/group/';
import GroupStudent from '../containers/groupStudent';
import Course from '../containers/course';
import Module from '../containers/module';
import Topic from '../containers/topic/';

const TenantRouter = () => {
  return (
    <Switch>
      <Route path={routes.employeeList} component={Employee}/>
      <Route path={routes.studentList} component={Student}/>
      <Route path={routes.menteeList} component={Mentee}/>
      <Route exact path={routes.groupList} component={Group}/>
      <Route path={routes.groupStudentList} component={GroupStudent}/>
      <Route exact path={routes.courseList} component={Course}/>
      <Route exact path={routes.courseModuleList} component={Module}/>
      <Route exact path={routes.moduleTopicList} component={Topic}/>
      <Route component={TenantDashboard}/>
    </Switch>
  );
};
export default TenantRouter;
