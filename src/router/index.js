import Router from './Router';
import TenantRouter from './TenantRouter';
import EmployeeRouter from './EmployeeRouter';

export {
  TenantRouter,
  EmployeeRouter
};
export default Router;
