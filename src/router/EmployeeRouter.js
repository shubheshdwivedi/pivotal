import React from 'react';
import {Switch, Route} from 'react-router-dom';
import EmployeeDashboard from '../containers/employee-dashboard/';
import routes from '../_helpers/routeHelpers';
import Paper from '../containers/paper/';
import PaperCraft from '../containers/paperCraft/PaperCraft';

const EmployeeRouter = () => {
  return (
    <div>
      <Switch>
        <Route path={routes.paperList} component={Paper}/>
        <Route path={routes.paperCrafting} component={PaperCraft}/>
        <Route component={EmployeeDashboard}/>
      </Switch>
    </div>
  );
};

export default EmployeeRouter;
