import React, {Component} from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import routes from '../_helpers/routeHelpers';
import Auth from '../containers/auth/Auth';
import NotFound from '../components/not-found';
import ProtectedRoute from '../containers/protected-route/ProtectedRoute';
import Dashboard from '../components/dashboard/Dashboard';

class Router extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path={routes.auth} component={Auth}/>
          <Route
            path={routes.dashboard}
            render={(props) => <ProtectedRoute {...props} next={Dashboard}/>}
          />
          <Route component={NotFound}/>
        </Switch>
      </BrowserRouter>
    );
  }
}

export default Router;
