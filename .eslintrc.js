module.exports = {
  "env": {
    "browser": true,
    "es6": true
  },
  "extends": [
    "eslint:recommended",
    "google",
    "plugin:react/recommended"
  ],
  "globals": {
    "Atomics": "readonly",
    "SharedArrayBuffer": "readonly"
  },
  "parser": "babel-eslint",
  "parserOptions": {
    "ecmaFeatures": {
      "jsx": true
    },
    "ecmaVersion": 2018,
    "sourceType": "module"
  },
  "plugins": [
    "react",
    "babel"
  ],
  "settings": {
    "react": {
      "pragma": "React",
      "version": "detect",
      "flowVersion": "0.53"
    }
  },
  "rules": {
    "indent": [
      "error",
      2,
      {
        "SwitchCase": 1
      }
    ],
    "linebreak-style": [
      "error",
      "windows"
    ],
    "quotes": [
      "error",
      "single"
    ],
    "semi": [
      "error",
      "always"
    ],
    "require-jsdoc": 0,
    "comma-dangle": ["error", {
      "arrays": "never",
      "objects": "never",
      "imports": "never",
      "exports": "never",
      "functions": "never"
    }],
    "key-spacing": [
      "error",
      {
        "beforeColon": false,
        "afterColon": true,
        "align": "colon"
      }
    ],
    "space-infix-ops": [
      "error",
      {
        "int32Hint": false
      }
    ],
    "arrow-spacing": 2,
    "no-console": 2,
    "no-invalid-this": 0,
    "babel/no-invalid-this": 2,
    "react/prop-types": 0,
    "no-import-assign": 2,
    "comma-spacing": 2,
    "comma-style": [
      "error",
      "last"
    ],
    "brace-style": 2,
    "computed-property-spacing": 2,
    "eol-last": 1,
    "func-call-spacing": 2,
    "implicit-arrow-linebreak": 2,
    "jsx-quotes": 2,
    "keyword-spacing": 2,
    "lines-between-class-members": 2,
    "multiline-comment-style": 2,
    "new-parens": 2,
    'no-case-declarations': 0,
    'new-cap': 0,
    "newline-per-chained-call": [
      "error",
      {
        "ignoreChainWithDepth": 3
      }
    ],
    "no-lonely-if": 2
  },
};